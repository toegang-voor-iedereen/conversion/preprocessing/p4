FROM elixir:1.16-otp-24

ENV APP_ENV=prod
ENV MIX_ENV=prod

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y git && apt-get clean

RUN mix local.hex --force
RUN mix local.rebar --force

COPY mix.exs mix.lock ./
RUN mix deps.get
RUN mix deps.compile

COPY . .

RUN mix compile
RUN mix release

CMD ["_build/prod/rel/p4/bin/p4", "start"]
