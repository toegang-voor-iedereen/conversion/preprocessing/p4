## [0.8.10](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.9...0.8.10) (2024-07-26)


### Bug Fixes

* **logger:** add error reason to an error's message ([5b6632b](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/5b6632bc49d07153c173c3d148b5889ba410bc2b))
* **logger:** print app config on startup for better debuggability ([6fd9dba](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/6fd9dba4d02792e58e20dcdecc7e3701a1ce83b4))

## [0.8.9](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.8...0.8.9) (2024-07-01)


### Bug Fixes

* fix Helm chart such that extraEnvVars actually supplies the env vars to the deployment ([742a671](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/742a671787ee1d9411bf70821bfac6fa0d5dd4e7))

## [0.8.8](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.7...0.8.8) (2024-06-25)


### Bug Fixes

* NLDOC-1875 default styles.xml in DocxReader ([4ceca6e](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/4ceca6eb13a41a2e1c3d693f5a7e397fbdd724fb))

## [0.8.7](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.6...0.8.7) (2024-06-21)


### Bug Fixes

* rename P4 image so it correctly reflects CI_REGISTRY_IMAGE ([db167e3](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/db167e381279019c30e6bd9e6578646fa146d135))

## [0.8.6](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.5...0.8.6) (2024-06-21)


### Bug Fixes

* fix Helm chart version bumping and Helm chart tests ([1b2610d](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/1b2610d4c4dbb328f0a75dd2445e4079782409e7))

## [0.8.5](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.4...0.8.5) (2024-06-20)


### Bug Fixes

* NLDOC-1779 helm releasing ([5a3ecc4](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/5a3ecc4a1e47d5c05411386fd56b1863ef537dde))

## [0.8.4](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.3...0.8.4) (2024-06-20)


### Bug Fixes

* add id to P4.Docx.Document and P4.Html.Document, taken from conversion message ([4b3ed55](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/4b3ed55ed4a1b796b6662a51fe64495554024532))
* all file converters now log with data.converter set to the converter's name ([a2518af](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/a2518af82f6116d7fd8c7400dac76ba511847852))
* also annotate w:pict and make it possible to retrieve element UUIDs from document for logging. Add autofix-removed-toc log ([c5d11e9](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/c5d11e9d17528748bc112c46a4b8a4ee92c47d8d))
* **logger:** fix caller and stacktrace so they actually return the callers of the P4.Logger methods ([f546df4](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/f546df43e46252245c5c9c05b5ba19c33be930a3))
* **logger:** remove P4.IO.Debug and fix logging so everything uses P4.Logger, fix logs in tests ([29fe3be](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/29fe3beb8c2c4a0f6a2acfaf56cf780ee88c68af))
* NLDOC-1779 helm chart ([570791e](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/570791e137679a0fa4072feec8ab5dbe26454c11))
* replace all IO.puts with calls to P4.Logger ([f89df33](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/f89df3352bb15991e79bafd0a23e6f67e43837f1))
* replace IO.puts and P4.IO.puts with calls to P4.Logger ([584ad6a](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/584ad6a9af8f8f10894c3fc2c3899a26c9ef1bc0))

## [0.8.3](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.2...0.8.3) (2024-06-19)


### Bug Fixes

* fix bump-versions.sh and fix helm chart tests again ([36ec254](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/36ec2548ebfb1b3428b3f474ee66b14c2410f19c))
* helm chart tests ([b75084f](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/b75084fecb2aedd1af206a20c149b6818301b111))

## [0.8.2](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.1...0.8.2) (2024-06-19)


### Bug Fixes

* remove -y again because wtf yq... ([de0ebda](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/de0ebdac4a41f3c2fcb5a507d4c4d8a676280a31))
* revert values.yaml changes made by semantic-release ([366b8b0](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/366b8b0704e362535bbc722b0d2fb5240794eda3))

## [0.8.1](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.8.0...0.8.1) (2024-06-19)


### Bug Fixes

* use yq -i -y in bump-versions because apparently that's necessary ([5702677](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/5702677108d914129fe62f841627bde00197b1a9))

# [0.8.0](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.7.3...0.8.0) (2024-06-19)


### Features

* add P4 Helm chart and include a CI job for checking and publishing it ([d268d75](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/d268d756c72277dee5c23f7ed2deacd44fe595f7))
* enable publishing Helm chart on release ([8372a46](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/8372a465d1a24de4272b4a0da7019fae70aedc99))

## [0.7.3](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.7.2...0.7.3) (2024-06-17)


### Bug Fixes

* NLDOC-1823 handle multiline titles by removing p tags from title elements ([84c40fc](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/84c40fcce9e4d392deb7454c14199f2640bc6302))
* remove formatting from multiline titles ([cedfd77](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/cedfd779df31c8f1bf5a0f844814ae16256e1bd3))

## [0.7.2](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.7.1...0.7.2) (2024-06-11)


### Bug Fixes

* NLDOC-1751 remove blockquotes from th, td and li in HTML postprocessing ([16c3bb8](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/16c3bb84473d616cbad305684fb715b47e4f6b57))

## [0.7.1](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.7.0...0.7.1) (2024-06-07)


### Bug Fixes

* also remove mini-images if they are w:pict (for v:imagedata), in addition to w:drawing ([90921b9](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/90921b97a47d63af58266ebf9e47650e95ba6394))
* ensure no crashes happen when strange edge cases appear in style properties in Word CSS ([20d7808](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/20d78086bd46b9869f8c77d058d7d476321061e8))

# [0.7.0](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.6.2...0.7.0) (2024-06-06)


### Bug Fixes

* NLDOC-1825 introduce postprocessing ([79ab474](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/79ab474d8937e06cc44ec6e2ee53140b529799cf))


### Features

* **docx:** NLDOC-1825: remove mini images such as chevrons and dividers from Word documents in preprocessing ([5726d20](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/5726d20dc49160e69601f6999603a51316f2a9d9))

## [0.6.2](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.6.1...0.6.2) (2024-06-05)


### Bug Fixes

* NLDOC-1823 file extension differs when post-processing ([01607aa](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/01607aa81f1fe9bbb9ae6ef00bf2514af174b5b3))

## [0.6.1](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.6.0...0.6.1) (2024-06-04)


### Bug Fixes

* **html:** NLDOC-1339: transform UUID comment on <title> to data-uuid attribute, remove all spans from <title>, remove all anchor spans from document ([23814c2](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/23814c2054a0b6a3baf6db0459de3d001688fcc9))

# [0.6.0](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.5.0...0.6.0) (2024-05-28)


### Features

* **html:** NLDOC-1777 remove mini images such as chevrons and dividers ([5f30ef2](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/5f30ef25ffd60e377a10591efe355177f1f971b8))

# [0.5.0](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/compare/0.4.3...0.5.0) (2024-05-24)


### Features

* configure P4 so all logs are printed in Elastic's ECS logging format ([006253b](https://gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4/commit/006253bb0f4f609b58eceaa975e007588ca07350))
