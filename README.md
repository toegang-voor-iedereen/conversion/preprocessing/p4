# P4 - Pandoc Pre- and Post-Processor

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `p4` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:p4, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/p4>.

## Installation for Contributing

### P4

1. Install Elixir if needed: `brew install elixir`
2. Install Git LFS if needed: `brew install git-lfs && git lfs install`
3. Clone this repository.
4. Install dependencies: `mix deps.get`

Now you can run commands like `mix compile` and `mix test`.

If `mix test` fails because of `Saxy.ParseError`s containing something along the lines of `binary: "version https://git-lfs.github.com/`, then run `git lfs pull` and try again.

### Pre-Commit

1. Install [pre-commit](https://pre-commit.com/#install)
2. Run `pre-commit install && pre-commit install --hook-type commit-msg` to ensure pre-commit runs before every commit.
