defmodule P4Test do
  use ExUnit.Case, async: true
  doctest P4

  describe "get_printable_config" do
    test "returns a printable config" do
      config = P4.get_printable_config()

      assert config == %{
               "p4" => %{
                 "env" => "test",
                 "name" => Application.get_env(:p4, :name),
                 "version" => Application.get_env(:p4, :version)
               },
               # these AMQP settings don't get set in the test environment because that makes AMQP start auto-connecting.
               "amqp" => %{
                 "host" => nil,
                 "password" => nil,
                 "port" => nil,
                 "username" => nil,
                 "vhost" => nil
               },
               "s3" => %{
                 "access_key_id" => nil,
                 "bucket" => Application.get_env(:p4, :s3_bucket),
                 "host" => Application.get_env(:ex_aws, :s3)[:host],
                 "port" => Application.get_env(:ex_aws, :s3)[:port],
                 "region" => Application.get_env(:ex_aws, :region),
                 "scheme" => Application.get_env(:ex_aws, :s3)[:scheme],
                 "secret_access_key" => nil
               }
             }
    end
  end
end
