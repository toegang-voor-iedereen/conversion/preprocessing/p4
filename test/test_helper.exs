:meck.new(:zip, [:unstick, :passthrough])

Mimic.copy(:zip)
Mimic.copy(Application)
Mimic.copy(AMQP.Application)
Mimic.copy(AMQP.Basic)
Mimic.copy(AMQP.Exchange)
Mimic.copy(AMQP.Queue)
Mimic.copy(Ecto.UUID)
Mimic.copy(ExAws)
Mimic.copy(ExAws.S3)
Mimic.copy(ExAws.S3.Upload)
Mimic.copy(File)
Mimic.copy(Kernel)
Mimic.copy(P4.Amqp.ApplicationTopology)
Mimic.copy(P4.Amqp.Consumer)
Mimic.copy(P4.Amqp.Dto.ConversionMessage)
Mimic.copy(P4.Amqp.MessageRouter)
Mimic.copy(P4.Amqp.Publisher)
Mimic.copy(P4.Amqp.Serialization.Serializer)
Mimic.copy(P4.Amqp.Storage.S3)
Mimic.copy(P4.Amqp.Topology)
Mimic.copy(P4.Docx.PreProcessor)
Mimic.copy(P4.Docx.Reader)
Mimic.copy(P4.Docx.Writer)
Mimic.copy(P4.IO.File)
Mimic.copy(P4.Logger)
Mimic.copy(P4.Misc.KeyGenerator)
Mimic.copy(Process)
Mimic.copy(Sentry)
Mimic.copy(Temp)

for transformer <- P4.Html.PostProcessor.transformers() do
  Mimic.copy(transformer)
end

ExUnit.start(cover: true)

defmodule TestHelper do
  @prolog %Saxy.Prolog{encoding: "UTF-8", standalone: true, version: "1.0"}

  def load_xml(path) do
    {:ok, content} = File.read(path)
    {:ok, xml} = Saxy.SimpleForm.parse_string(content, expand_entity: :never)
    xml
  end

  def test_xml_map_snapshot(value, path) when is_map(value) and is_binary(path) do
    value
    |> Map.to_list()
    |> Enum.reduce(
      "",
      fn
        {key, value}, acc ->
          acc <>
            "\n<!--BEGIN #{key}-->\n" <> Saxy.encode!(value, @prolog) <> "\n<!--END #{key}-->\n"
      end
    )
    |> test_snapshot(path, "xml")
  end

  def test_xml_snapshot(value, path) when is_binary(path) do
    value
    |> Saxy.encode!(@prolog)
    |> test_snapshot(path, "xml")
  end

  def test_snapshot(value, path, ext)
      when is_binary(value) and is_binary(path) and is_binary(ext) do
    File.mkdir_p!(path)

    path_expected = Path.join(path, "expected.#{ext}")
    path_actual = Path.join(path, "actual.#{ext}")

    File.write!(path_actual, value)

    expected_missing_message = ~s"""
    Please provide expected snapshot (expected.#{ext}) in #{path}.

    Run the following command to update the snapshot:

      $ cp \\
          "#{path_actual}" \\
          "#{path_expected}"
    """

    path_expected
    |> File.exists?()
    |> ExUnit.Assertions.assert(expected_missing_message)

    mismatch_message = ~s"""
    Snapshot mismatch for #{path} - expected.#{ext} differs from actual.#{ext}.

    Run the following command to diff the snapshot (or use your editor):

      $ diff \\
          "#{path_actual}" \\
          "#{path_expected}"

    Run the following command to update the snapshot:

      $ cp \\
          "#{path_actual}" \\
          "#{path_expected}"
    """

    path_expected
    |> File.read!()
    |> String.equivalent?(value)
    |> ExUnit.Assertions.assert(mismatch_message)
  end
end
