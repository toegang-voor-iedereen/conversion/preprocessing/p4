defmodule P4.StdTest do
  use ExUnit.Case, async: true

  describe "default/2" do
    test "returns the first argument if it is not nil" do
      assert P4.Std.default(1, 2) == 1
    end

    test "returns the second argument if the first argument is nil" do
      assert P4.Std.default(nil, 2) == 2
    end
  end

  describe "is_not_nil/1" do
    test "returns true if the value is not nil" do
      assert P4.Std.is_not_nil(1)
    end

    test "returns false if the value is nil" do
      refute P4.Std.is_not_nil(nil)
    end
  end
end
