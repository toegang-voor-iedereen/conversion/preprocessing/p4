defmodule P4.Style.StyleMatcherTest do
  use ExUnit.Case, async: true
  doctest P4.Style.StyleMatcher

  @style_map %{
    "Normal" =>
      {"w:style", [{"w:type", "paragraph"}, {"w:default", "1"}, {"w:styleId", "Normal"}],
       [
         {"w:rPr", [],
          [
            {"w:lang", [{"w:bidi", "ar-SA"}, {"w:eastAsia", "en-US"}, {"w:val", "en-US"}], []},
            {"w:rFonts",
             [
               {"w:asciiTheme", "minorHAnsi"},
               {"w:eastAsiaTheme", "minorHAnsi"},
               {"w:hAnsiTheme", "minorHAnsi"},
               {"w:cstheme", "minorBidi"}
             ], []},
            {"w:kern", [{"w:val", "2"}], []},
            {"w:sz", [{"w:val", "24"}], []},
            {"w:szCs", [{"w:val", "24"}], []},
            {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []}
          ]}
       ]},
    "Heading1" =>
      {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Heading1"}],
       [
         {"w:rPr", [],
          [
            {"w:szCs", [{"w:val", "32"}], []},
            {"w:color",
             [
               {"w:val", "2F5496"},
               {"w:themeColor", "accent1"},
               {"w:themeShade", "BF"}
             ], []},
            {"w:lang", [{"w:bidi", "ar-SA"}, {"w:eastAsia", "en-US"}, {"w:val", "en-US"}], []},
            {"w:rFonts",
             [
               {"w:eastAsiaTheme", "majorEastAsia"},
               {"w:hAnsiTheme", "majorHAnsi"},
               {"w:asciiTheme", "majorHAnsi"},
               {"w:cstheme", "majorBidi"}
             ], []},
            {"w:sz", [{"w:val", "32"}], []}
          ]},
         {"w:pPr", [],
          [
            {"w:outlineLvl", [{"w:val", "0"}], []},
            {"w:keepLines", [], []},
            {"w:keepNext", [], []},
            {"w:spacing", [{"w:before", "240"}], []}
          ]}
       ]},
    "Heading2" =>
      {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Heading2"}],
       [
         {"w:rPr", [],
          [
            {"w:szCs", [{"w:val", "26"}], []},
            {"w:color",
             [
               {"w:val", "2F5496"},
               {"w:themeColor", "accent1"},
               {"w:themeShade", "BF"}
             ], []},
            {"w:rFonts",
             [
               {"w:eastAsiaTheme", "majorEastAsia"},
               {"w:hAnsiTheme", "majorHAnsi"},
               {"w:asciiTheme", "majorHAnsi"},
               {"w:cstheme", "majorBidi"}
             ], []},
            {"w:sz", [{"w:val", "26"}], []}
          ]},
         {"w:pPr", [],
          [
            {"w:outlineLvl", [{"w:val", "1"}], []},
            {"w:keepLines", [], []},
            {"w:keepNext", [], []},
            {"w:spacing", [{"w:before", "40"}], []}
          ]}
       ]},
    "Heading3" =>
      {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Heading3"}],
       [
         {"w:rPr", [],
          [
            {"w:color",
             [
               {"w:val", "1F3763"},
               {"w:themeColor", "accent1"},
               {"w:themeShade", "7F"}
             ], []},
            {"w:kern", [{"w:val", "2"}], []},
            {"w:sz", [{"w:val", "24"}], []},
            {"w:szCs", [{"w:val", "24"}], []},
            {"w:rFonts",
             [
               {"w:eastAsiaTheme", "majorEastAsia"},
               {"w:hAnsiTheme", "majorHAnsi"},
               {"w:asciiTheme", "majorHAnsi"},
               {"w:cstheme", "majorBidi"}
             ], []}
          ]},
         {"w:pPr", [],
          [
            {"w:outlineLvl", [{"w:val", "2"}], []},
            {"w:keepLines", [], []},
            {"w:keepNext", [], []},
            {"w:spacing", [{"w:before", "40"}], []}
          ]}
       ]}
  }

  @context %P4.Docx.DocumentContext{
    style_map: @style_map,
    specific_style_ids: %P4.Docx.SpecificStyleIdsState{
      table_of_contents: [],
      normal: "Normal",
      heading: ["Heading1", "Heading2", "Heading3"]
    }
  }

  describe "merge_paragraph_run_properties_with_style/2" do
    test "merges successfully" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "minorHAnsi"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading2"}], []}, run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 2"]}]}
         ]}

      assert P4.Style.StyleMatcher.merge_paragraph_run_properties_with_style(input, @context) ===
               {
                 "w:rPr",
                 [],
                 [
                   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                   {"w:rFonts",
                    [
                      {"w:eastAsiaTheme", "majorEastAsia"},
                      {"w:hAnsiTheme", "minorHAnsi"},
                      {"w:asciiTheme", "minorHAnsi"},
                      {"w:cstheme", "minorHAnsi"}
                    ], []},
                   {"w:sz", [{"w:val", "24"}], []},
                   {"w:szCs", [{"w:val", "24"}], []}
                 ]
               }
    end
  end

  describe "&run_properties_matches?/2" do
    test "when differs in rFonts" do
      left =
        {"w:rPr", [],
         [
           {"w:lang", [{"w:bidi", "ar-SA"}, {"w:eastAsia", "en-US"}, {"w:val", "en-US"}], []},
           {"w:kern", [{"w:val", "2"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []}
         ]}

      right =
        {"w:rPr", [],
         [
           {"w:szCs", [{"w:val", "24"}], []},
           {"w:rFonts",
            [
              {"w:eastAsiaTheme", "majorEastAsia"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:asciiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
            []},
           {"w:sz", [{"w:val", "24"}], []}
         ]}

      refute P4.Style.StyleMatcher.run_properties_matches?(left, right)
      refute P4.Style.StyleMatcher.run_properties_matches?(right, left)
    end

    test "when differs in size" do
      left =
        {"w:rPr", [],
         [
           {"w:lang", [{"w:bidi", "ar-SA"}, {"w:eastAsia", "en-US"}, {"w:val", "en-US"}], []},
           {"w:rFonts",
            [
              {"w:eastAsiaTheme", "majorEastAsia"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:asciiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:kern", [{"w:val", "2"}], []},
           {"w:sz", [{"w:val", "26"}], []},
           {"w:szCs", [{"w:val", "26"}], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []}
         ]}

      right =
        {"w:rPr", [],
         [
           {"w:szCs", [{"w:val", "24"}], []},
           {"w:rFonts",
            [
              {"w:eastAsiaTheme", "majorEastAsia"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:asciiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
            []},
           {"w:sz", [{"w:val", "24"}], []}
         ]}

      refute P4.Style.StyleMatcher.run_properties_matches?(left, right)
      refute P4.Style.StyleMatcher.run_properties_matches?(right, left)
    end

    test "when does not differ" do
      left =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "minorHAnsi"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      right =
        {"w:rPr", [],
         [
           {"w:szCs", [{"w:val", "24"}], []},
           {"w:rFonts",
            [
              {"w:eastAsiaTheme", "majorEastAsia"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:asciiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []}
         ]}

      assert P4.Style.StyleMatcher.run_properties_matches?(left, right)
      assert P4.Style.StyleMatcher.run_properties_matches?(right, left)
    end
  end

  describe "&modify_style_properties/2" do
    test "it should modify Normal marked up as h1" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "majorHAnsi"},
              {"w:hAnsiTheme", "majorHAnsi"},
              {"w:cstheme", "majorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
            []},
           {"w:sz", [{"w:val", "32"}], []},
           {"w:szCs", [{"w:val", "32"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 1"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) ===
               {"w:p", [],
                [
                  {"w:pPr", [], [run_properties, {"w:pStyle", [{"w:val", "Heading1"}], []}]},
                  {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 1"]}]}
                ]}
    end

    test "it should modify Normal marked up as h2" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "majorHAnsi"},
              {"w:hAnsiTheme", "majorHAnsi"},
              {"w:cstheme", "majorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
            []},
           {"w:sz", [{"w:val", "26"}], []},
           {"w:szCs", [{"w:val", "26"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 2"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) ===
               {"w:p", [],
                [
                  {"w:pPr", [], [run_properties, {"w:pStyle", [{"w:val", "Heading2"}], []}]},
                  {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 2"]}]}
                ]}
    end

    test "it should modify Normal marked up as h3" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "majorHAnsi"},
              {"w:hAnsiTheme", "majorHAnsi"},
              {"w:cstheme", "majorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "1F3763"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 3"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) ===
               {"w:p", [],
                [
                  {"w:pPr", [], [run_properties, {"w:pStyle", [{"w:val", "Heading3"}], []}]},
                  {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 3"]}]}
                ]}
    end

    test "it should modify h2 marked up as Normal" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "minorHAnsi"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading2"}], []}, run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 2"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) ==
               {
                 "w:p",
                 [],
                 [
                   {
                     "w:pPr",
                     [],
                     [
                       {"w:pStyle", [{"w:val", "Normal"}], []},
                       run_properties
                     ]
                   },
                   {
                     "w:r",
                     [],
                     [
                       run_properties,
                       {"w:t", [], ["Normal as Heading 2"]}
                     ]
                   }
                 ]
               }
    end

    test "it should modify h3 marked up as Normal" do
      run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "minorHAnsi"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading3"}], []}, run_properties]},
           {"w:r", [], [run_properties, {"w:t", [], ["Normal as Heading 3"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) ==
               {
                 "w:p",
                 [],
                 [
                   {
                     "w:pPr",
                     [],
                     [
                       {"w:pStyle", [{"w:val", "Normal"}], []},
                       run_properties
                     ]
                   },
                   {
                     "w:r",
                     [],
                     [
                       run_properties,
                       {"w:t", [], ["Normal as Heading 3"]}
                     ]
                   }
                 ]
               }
    end

    test "it should not modify Normal if marked up properly" do
      input = {"w:p", [], [{"w:r", [], [{"w:t", [], ["Normal"]}]}]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) === input
    end

    test "it should not modify h1 if marked up properly" do
      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading1"}], []}]},
           {"w:r", [], [{"w:t", [], ["Heading 1"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) === input
    end

    test "it should not modify h2 if marked up properly" do
      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading2"}], []}]},
           {"w:r", [], [{"w:t", [], ["Heading 2"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) === input
    end

    test "it should not modify h3 if marked up properly" do
      input =
        {"w:p", [],
         [
           {"w:pPr", [], [{"w:pStyle", [{"w:val", "Heading3"}], []}]},
           {"w:r", [], [{"w:t", [], ["Heading 3"]}]}
         ]}

      assert P4.Style.StyleMatcher.modify_style_properties(input, @context) === input
    end

    test "it should not modify any of the headings in headings.docx" do
      context = %P4.Docx.DocumentContext{
        specific_style_ids: %P4.Docx.SpecificStyleIdsState{
          table_of_contents: [],
          normal: "Standaard",
          heading: ["Kop6", "Kop5", "Kop4", "Kop3", "Kop2", "Kop1"]
        },
        style_map: %{
          "Geenlijst" =>
            {"w:style", [{"w:type", "numbering"}, {"w:default", "1"}, {"w:styleId", "Geenlijst"}],
             [
               {"w:name", [{"w:val", "No List"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:asciiTheme", "minorHAnsi"},
                     {"w:eastAsiaTheme", "minorHAnsi"},
                     {"w:hAnsiTheme", "minorHAnsi"},
                     {"w:cstheme", "minorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "99"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop1" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop1"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop1Char"}], []},
               {"w:name", [{"w:val", "heading 1"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "0"}], []},
                  {"w:spacing", [{"w:before", "240"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "32"}], []},
                  {"w:szCs", [{"w:val", "32"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []}
             ]},
          "Kop1Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop1Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop1"}], []},
               {"w:name", [{"w:val", "Kop 1 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "32"}], []},
                  {"w:szCs", [{"w:val", "32"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop2" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop2"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop2Char"}], []},
               {"w:name", [{"w:val", "heading 2"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "1"}], []},
                  {"w:spacing", [{"w:before", "40"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "26"}], []},
                  {"w:szCs", [{"w:val", "26"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop2Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop2Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop2"}], []},
               {"w:name", [{"w:val", "Kop 2 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "26"}], []},
                  {"w:szCs", [{"w:val", "26"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop3" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop3"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop3Char"}], []},
               {"w:name", [{"w:val", "heading 3"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "2"}], []},
                  {"w:spacing", [{"w:before", "40"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "1F3763"}, {"w:themeColor", "accent1"}, {"w:themeShade", "7F"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop3Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop3Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop3"}], []},
               {"w:name", [{"w:val", "Kop 3 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "1F3763"}, {"w:themeColor", "accent1"}, {"w:themeShade", "7F"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop4" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop4"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop4Char"}], []},
               {"w:name", [{"w:val", "heading 4"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "3"}], []},
                  {"w:spacing", [{"w:before", "40"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:i", [], []},
                  {"w:iCs", [], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop4Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop4Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop4"}], []},
               {"w:name", [{"w:val", "Kop 4 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:i", [], []},
                  {"w:iCs", [], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop5" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop5"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop5Char"}], []},
               {"w:name", [{"w:val", "heading 5"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "4"}], []},
                  {"w:spacing", [{"w:before", "40"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop5Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop5Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop5"}], []},
               {"w:name", [{"w:val", "Kop 5 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop6" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:styleId", "Kop6"}],
             [
               {"w:basedOn", [{"w:val", "Standaard"}], []},
               {"w:link", [{"w:val", "Kop6Char"}], []},
               {"w:name", [{"w:val", "heading 6"}], []},
               {"w:next", [{"w:val", "Standaard"}], []},
               {"w:pPr", [],
                [
                  {"w:keepLines", [], []},
                  {"w:keepNext", [], []},
                  {"w:outlineLvl", [{"w:val", "5"}], []},
                  {"w:spacing", [{"w:before", "40"}], []}
                ]},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "1F3763"}, {"w:themeColor", "accent1"}, {"w:themeShade", "7F"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Kop6Char" =>
            {"w:style",
             [{"w:type", "character"}, {"w:customStyle", "1"}, {"w:styleId", "Kop6Char"}],
             [
               {"w:basedOn", [{"w:val", "Standaardalinea-lettertype"}], []},
               {"w:link", [{"w:val", "Kop6"}], []},
               {"w:name", [{"w:val", "Kop 6 Char"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color",
                   [{"w:val", "1F3763"}, {"w:themeColor", "accent1"}, {"w:themeShade", "7F"}],
                   []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:eastAsiaTheme", "majorEastAsia"},
                     {"w:hAnsiTheme", "majorHAnsi"},
                     {"w:asciiTheme", "majorHAnsi"},
                     {"w:cstheme", "majorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:rsid", [{"w:val", "00CE262B"}], []},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "9"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Standaard" =>
            {"w:style", [{"w:type", "paragraph"}, {"w:default", "1"}, {"w:styleId", "Standaard"}],
             [
               {"w:name", [{"w:val", "Normal"}], []},
               {"w:pPr", [], []},
               {"w:qFormat", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:asciiTheme", "minorHAnsi"},
                     {"w:eastAsiaTheme", "minorHAnsi"},
                     {"w:hAnsiTheme", "minorHAnsi"},
                     {"w:cstheme", "minorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]}
             ]},
          "Standaardalinea-lettertype" =>
            {"w:style",
             [
               {"w:type", "character"},
               {"w:default", "1"},
               {"w:styleId", "Standaardalinea-lettertype"}
             ],
             [
               {"w:name", [{"w:val", "Default Paragraph Font"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:asciiTheme", "minorHAnsi"},
                     {"w:eastAsiaTheme", "minorHAnsi"},
                     {"w:hAnsiTheme", "minorHAnsi"},
                     {"w:cstheme", "minorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:semiHidden", [], []},
               {"w:uiPriority", [{"w:val", "1"}], []},
               {"w:unhideWhenUsed", [], []}
             ]},
          "Standaardtabel" =>
            {"w:style",
             [{"w:type", "table"}, {"w:default", "1"}, {"w:styleId", "Standaardtabel"}],
             [
               {"w:name", [{"w:val", "Normal Table"}], []},
               {"w:pPr", [], []},
               {"w:rPr", [],
                [
                  {"w14:ligatures", [{"w14:val", "standardContextual"}], []},
                  {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                  {"w:kern", [{"w:val", "2"}], []},
                  {"w:lang", [{"w:val", "nl-NL"}, {"w:eastAsia", "en-US"}, {"w:bidi", "ar-SA"}],
                   []},
                  {"w:rFonts",
                   [
                     {"w:asciiTheme", "minorHAnsi"},
                     {"w:eastAsiaTheme", "minorHAnsi"},
                     {"w:hAnsiTheme", "minorHAnsi"},
                     {"w:cstheme", "minorBidi"}
                   ], []},
                  {"w:sz", [{"w:val", "24"}], []},
                  {"w:szCs", [{"w:val", "24"}], []}
                ]},
               {"w:semiHidden", [], []},
               {"w:tblPr", [],
                [
                  {"w:tblInd", [{"w:w", "0"}, {"w:type", "dxa"}], []},
                  {"w:tblCellMar", [],
                   [
                     {"w:top", [{"w:w", "0"}, {"w:type", "dxa"}], []},
                     {"w:left", [{"w:w", "108"}, {"w:type", "dxa"}], []},
                     {"w:bottom", [{"w:w", "0"}, {"w:type", "dxa"}], []},
                     {"w:right", [{"w:w", "108"}, {"w:type", "dxa"}], []}
                   ]}
                ]},
               {"w:uiPriority", [{"w:val", "99"}], []},
               {"w:unhideWhenUsed", [], []}
             ]}
        }
      }

      for level <- 1..6 do
        input =
          {"w:p",
           [{"w:rsidR", "00CE262B"}, {"w:rsidRDefault", "00CE262B"}, {"w:rsidP", "00CE262B"}],
           [
             {"w:pPr", [],
              [
                {"w:pStyle", [{"w:val", "Kop#{level}"}], []},
                {"w:rPr", [], [{"w:lang", [{"w:val", "en-US"}], []}]}
              ]},
             {"w:r", [],
              [
                {"w:rPr", [], [{"w:lang", [{"w:val", "en-US"}], []}]},
                {"w:t", [], ["Heading #{level}"]}
              ]}
           ]}

        assert input === P4.Style.StyleMatcher.modify_style_properties(input, context)
      end
    end
  end
end
