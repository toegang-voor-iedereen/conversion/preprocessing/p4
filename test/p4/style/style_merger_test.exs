defmodule P4.Style.StyleMergerTest do
  use ExUnit.Case, async: true
  doctest P4.Style.StyleMerger

  describe "&merge/2" do
    test "Merges correctly" do
      heading_run_properties =
        {"w:rPr", [],
         [
           {"w:szCs", [{"w:val", "26"}], []},
           {"w:color", [{"w:val", "2F5496"}, {"w:themeColor", "accent1"}, {"w:themeShade", "BF"}],
            []},
           {"w:rFonts",
            [
              {"w:eastAsiaTheme", "majorEastAsia"},
              {"w:hAnsiTheme", "majorHAnsi"},
              {"w:asciiTheme", "majorHAnsi"},
              {"w:cstheme", "majorBidi"}
            ], []},
           {"w:sz", [{"w:val", "26"}], []}
         ]}

      paragraph_run_properties =
        {"w:rPr", [],
         [
           {"w:rFonts",
            [
              {"w:asciiTheme", "minorHAnsi"},
              {"w:hAnsiTheme", "minorHAnsi"},
              {"w:cstheme", "minorHAnsi"}
            ], []},
           {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
           {"w:sz", [{"w:val", "24"}], []},
           {"w:szCs", [{"w:val", "24"}], []}
         ]}

      assert P4.Style.StyleMerger.merge(heading_run_properties, paragraph_run_properties) ===
               {
                 "w:rPr",
                 [],
                 [
                   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
                   {"w:rFonts",
                    [
                      {"w:eastAsiaTheme", "majorEastAsia"},
                      {"w:hAnsiTheme", "minorHAnsi"},
                      {"w:asciiTheme", "minorHAnsi"},
                      {"w:cstheme", "minorHAnsi"}
                    ], []},
                   {"w:sz", [{"w:val", "24"}], []},
                   {"w:szCs", [{"w:val", "24"}], []}
                 ]
               }
    end
  end
end
