defmodule P4.Style.StyleMutatorTest do
  use ExUnit.Case, async: true

  doctest P4.Style.StyleMutator

  test "Merge styles from styles/cats-in-space.xml" do
    "test/__fixtures__/styles/cats-in-space.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/cats-in-space")
  end

  test "Merge styles from styles/MsWord.xml" do
    "test/__fixtures__/styles/MsWord.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/MsWord")
  end

  test "Merge styles from styles/cats-in-space-no-defaults.xml" do
    "test/__fixtures__/styles/cats-in-space-no-defaults.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/cats-in-space-no-defaults")
  end

  test "Merge styles from styles/styles-with-missing-based-on.xml" do
    "test/__fixtures__/styles/styles-with-missing-based-on.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/styles-with-missing-based-on")
  end

  test "Merges styles from styles/heading-normal-text-mixed-up-styling.xml" do
    "test/__fixtures__/styles/heading-normal-text-mixed-up-styling.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/heading-normal-text-mixed-up-styling")
  end

  test "Merges styles from styles/MsWord-Normal-Heading1-GeneralHeading.xml" do
    "test/__fixtures__/styles/MsWord-Normal-Heading1-GeneralHeading.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/MsWord-Normal-Heading1-GeneralHeading")
  end

  test "Merges styles from styles/headings_without_outline_level.xml" do
    "test/__fixtures__/styles/headings_without_outline_level.xml"
    |> TestHelper.load_xml()
    |> P4.Style.StyleMutator.merge_document_styles()
    |> TestHelper.test_xml_map_snapshot("test/__snapshots__/P4.Style.StyleMutator/merge_document_styles/headings_without_outline_level")
  end
end
