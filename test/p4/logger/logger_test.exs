defmodule P4.TestLogModule do
  def log() do
    P4.Logger.info("Test")
    :ok
  end
end

defmodule P4.LoggerTest do
  use ExUnit.Case, async: true
  use Mimic

  setup_all do
    # Since we cannot use logger config files in tests, we need to manually load the config and set the formatter at runtime for this test.
    logging_config = Config.Reader.read!("config/config_logger.exs")
    formatter = logging_config[:logger][:default_handler][:formatter]
    :logger.update_handler_config(:default, :formatter, formatter)
  end

  describe "get_stacktrace" do
    test "returns the stacktrace" do
      assert List.starts_with?(P4.Logger.get_stacktrace(), [
               {P4.LoggerTest, :"test get_stacktrace returns the stacktrace", 1,
                [file: ~c"test/p4/logger/logger_test.exs", line: 21]}
             ])
    end
  end

  describe "log levels" do
    test "Logger.info logs a message" do
      log_message =
        capture_log(:info, fn ->
          P4.Logger.info("test")
        end)

      assert {:ok, log} = Jason.decode(log_message)
      assert log["message"] == "test"
    end

    test "Logger.error transforms error keyword into error fields (crash_reason with stacktrace)" do
      exception = %P4.Error.IO.CreateTempFile{}

      log_message =
        capture_log(:error, fn ->
          P4.Logger.error("test", error: exception)
        end)

      assert {:ok, log} = Jason.decode(log_message)
      assert log["message"] == "test"
      assert log["error.message"] == "Failure creating temporary path"
      assert log["error.type"] == "Elixir.P4.Error.IO.CreateTempFile"

      stack_trace = log["error.stack_trace"] |> String.split("\n")

      assert Enum.at(stack_trace, 0) ==
               "** (P4.Error.IO.CreateTempFile) Failure creating temporary path"

      assert Enum.at(stack_trace, 1) =~
               ~r"    test/p4/logger/logger_test.exs:\d+: anonymous fn/1 in P4.LoggerTest.capture_log/2"

      assert Enum.at(stack_trace, 2) =~
               ~r"    \(ex_unit \d.\d+.\d+\) lib/ex_unit/capture_io.ex:\d+: ExUnit.CaptureIO.do_with_io/3"

      assert Enum.at(stack_trace, 3) =~
               ~r"    \(ex_unit \d.\d+.\d\) lib/ex_unit/capture_io.ex:\d+: ExUnit.CaptureIO.capture_io/2"

      assert Enum.at(stack_trace, 4) =~
               ~r"    test/p4/logger/logger_test.exs:\d+: P4.LoggerTest.capture_log/2"

      assert Enum.at(stack_trace, 5) =~
               ~r"    test/p4/logger/logger_test.exs:\d+: P4.LoggerTest.\"test log levels Logger.error transforms error keyword into error fields \(crash_reason with stacktrace\)\"/1"
    end
  end

  describe "log.logger and log.origin fields" do
    test "are set to the module that calls P4.Logger method" do
      log_message =
        capture_log(:info, fn ->
          P4.TestLogModule.log()
        end)

      assert {:ok, log} = Jason.decode(log_message)
      assert log["message"] == "Test"
      assert log["log.logger"] == "Elixir.P4.TestLogModule"

      assert log["log.origin"] == %{
               "function" => "log/0",
               "file.line" => 3,
               "file.name" => "test/p4/logger/logger_test.exs"
             }
    end
  end

  describe "logger also prints error reasons" do
    test "Logger.error prints a string error reason" do
      reason = "because I think it's funny"

      {:error, error} =
        P4.Error.S3.Download.wrap({:error, reason}, %{
          bucket: "test-bucket",
          key: "test-key",
          local_path: "test_path"
        })

      log_message =
        capture_log(:error, fn ->
          P4.Logger.error("Oh no there was a problem", error: error)
        end)

      assert {:ok, log} = Jason.decode(log_message)
      assert log["message"] == "Oh no there was a problem"

      assert log["error.message"] ==
               "Failure downloading file from S3: because I think it's funny"
    end

    test "Logger.error prints an error reason from nested exceptions" do
      {:error, reason} =
        P4.Error.IO.CreateTempFile.wrap({:error, "because I think it's funny"}, %{})

      {:error, error} =
        P4.Error.S3.Download.wrap({:error, reason}, %{
          bucket: "test-bucket",
          key: "test-key",
          local_path: "test_path"
        })

      log_message =
        capture_log(:error, fn ->
          P4.Logger.error("Oh no there was a problem", error: error)
        end)

      assert {:ok, log} = Jason.decode(log_message)
      assert log["message"] == "Oh no there was a problem"

      assert log["error.message"] ==
               "Failure downloading file from S3: Failure creating temporary path: because I think it's funny"
    end
  end

  defp capture_log(level, fun) do
    Logger.configure(level: level)

    ExUnit.CaptureIO.capture_io(:user, fn ->
      fun.()
      Logger.flush()
    end)
  after
    Logger.configure(level: :debug)
  end
end
