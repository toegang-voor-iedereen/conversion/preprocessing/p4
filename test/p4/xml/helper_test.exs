defmodule P4.Xml.HelperTest do
  use ExUnit.Case, async: true
  doctest P4.Xml.Helper

  describe "find_element_recursively/2" do
    test "finds the first element for which matcher returns true" do
      document =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"section", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"p", [], ["Fourth paragraph"]},
                 {"div", [],
                  [
                    {"p", [], ["Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      element =
        P4.Xml.Helper.find_element_recursively(document, fn
          {"p", [], ["Second paragraph"]} -> true
          _ -> false
        end)

      assert element === {"p", [], ["Second paragraph"]}

      element =
        P4.Xml.Helper.find_element_recursively(document, fn
          {"p", _, _} -> true
          _ -> false
        end)

      assert element === {"p", [], ["First paragraph"]}
    end

    test "returns nil when matcher never returns true" do
      document =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"section", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"p", [], ["Fourth paragraph"]},
                 {"div", [],
                  [
                    {"p", [], ["Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      element =
        P4.Xml.Helper.find_element_recursively(document, fn
          _ -> false
        end)

      assert element === nil
    end
  end

  describe "find_element_recursively_by_name/2" do
    test "finds the first element with the given name" do
      document =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"section", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"p", [], ["Fourth paragraph"]},
                 {"div", [],
                  [
                    {"p", [], ["Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      element = P4.Xml.Helper.find_element_recursively_by_name(document, "p")

      assert element === {"p", [], ["First paragraph"]}

      element = P4.Xml.Helper.find_element_recursively_by_name(document, "div")

      assert element === {"div", [], [{"p", [], ["Fifth paragraph"]}]}
    end
  end

  describe "find_elements_recursively_by_name/2" do
    test "finds all elements with the given name" do
      document =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"div", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"p", [], ["Fourth paragraph"]},
                 {"div", [],
                  [
                    {"p", [], ["Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      elements = P4.Xml.Helper.find_elements_recursively_by_name(document, "p")

      assert length(elements) == 5

      assert elements === [
               {"p", [], ["First paragraph"]},
               {"p", [], ["Second paragraph"]},
               {"p", [], ["Third paragraph"]},
               {"p", [], ["Fourth paragraph"]},
               {"p", [], ["Fifth paragraph"]}
             ]
    end
  end

  describe "transform_child_recursively/3" do
    test "transforms a basic element" do
      input_element = {"p", [], ["Some text"]}

      matcher = fn
        {"p", _, _} -> true
        _ -> false
      end

      transformer = fn
        {"p", _, children} -> {"div", [], ["New text" | children]}
      end

      transformed_element =
        P4.Xml.Helper.transform_recursive_child(input_element, matcher, transformer)

      expected_element = {"div", [], ["New text", "Some text"]}

      assert transformed_element === expected_element
    end

    test "transforms deeply nested elements" do
      input_element =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"div", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"p", [], ["Fourth paragraph"]},
                 {"div", [],
                  [
                    {"p", [], ["Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      matcher = fn
        {"p", _, ["Fourth paragraph"]} -> true
        {"p", _, ["Fifth paragraph"]} -> true
        _ -> false
      end

      transformer = fn
        {"p", _, children} -> {"div", [], ["New text" | children]}
      end

      transformed_element =
        P4.Xml.Helper.transform_recursive_child(input_element, matcher, transformer)

      expected_element =
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["First paragraph"]},
              {"p", [], ["Second paragraph"]},
              {"div", [],
               [
                 {"p", [], ["Third paragraph"]},
                 {"div", [], ["New text", "Fourth paragraph"]},
                 {"div", [],
                  [
                    {"div", [], ["New text", "Fifth paragraph"]}
                  ]}
               ]}
            ]}
         ]}

      assert transformed_element === expected_element
    end
  end
end
