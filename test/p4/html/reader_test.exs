defmodule P4.Html.ReaderTest do
  use ExUnit.Case, async: true
  doctest P4.Html.Reader

  @id "test"

  describe "read_from_path/1" do
    test "reads a valid HTML file with no contents" do
      expected_document = %P4.Html.Document{
        id: @id,
        document:
          {"html", [{"lang", "en"}],
           [
             "\n\n",
             {"head", [], ["\n"]},
             "\n\n",
             {"body", [], ["\n"]},
             "\n\n"
           ]}
      }

      assert {:ok, actual_document} =
               P4.Html.Reader.read_from_path("test/__fixtures__/html/no-contents.html", @id)

      assert expected_document === actual_document
    end

    test "reads a valid HTML file with basic contents" do
      expected_document = %P4.Html.Document{
        id: @id,
        document:
          {"html", [{"lang", "en"}],
           [
             "\n\n",
             {"head", [], ["\n  ", {"title", [], ["Test"]}, "\n"]},
             "\n\n",
             {"body", [],
              [
                "\n  ",
                {"h1", [{"uuid", "1234"}], ["Hello, World!"]},
                "\n  ",
                {"p", [], ["This is a test"]},
                "\n"
              ]},
             "\n\n"
           ]}
      }

      assert {:ok, actual_document} =
               P4.Html.Reader.read_from_path("test/__fixtures__/html/basic.html", @id)

      assert expected_document === actual_document
    end

    test "returns an error when reading a non-existant file" do
      expected_error = %P4.Error.Html.Reader.FromPath{
        message: "Failure reading HTML file from path: :enoent",
        context: %{path: "test/__fixtures__/html/non-existant.html"},
        reason: :enoent
      }

      assert {:error, actual_error} =
               P4.Html.Reader.read_from_path("test/__fixtures__/html/non-existant.html", @id)

      assert expected_error === actual_error
    end

    test "returns an error when reading an empty file" do
      expected_error = %P4.Error.Html.Reader.FromPath{
        message:
          "Failure reading HTML file from path: unexpected end of input, expected token: :lt",
        context: %{path: "test/__fixtures__/html/empty.html"},
        reason: %Saxy.ParseError{
          reason: {:token, :lt},
          binary: "",
          position: 0
        }
      }

      assert {:error, actual_error} =
               P4.Html.Reader.read_from_path("test/__fixtures__/html/empty.html", @id)

      assert expected_error === actual_error
    end
  end
end
