defmodule P4.Html.WriterTest do
  use ExUnit.Case, async: true
  use Mimic
  doctest P4.Html.Writer

  describe "write/1" do
    test "writes a valid HTML document with no contents" do
      document = %P4.Html.Document{
        document:
          {"html", [{"lang", "en"}],
           [
             {"head", [], []},
             {"body", [], []}
           ]}
      }

      assert {:ok, path_to} = P4.Html.Writer.write(document)

      path_contents = File.read!(path_to)
      assert path_contents === "<html lang=\"en\"><head/><body/></html>"
    end

    test "writes a valid HTML document with basic contents" do
      document = %P4.Html.Document{
        document:
          {"html", [{"lang", "en"}],
           [
             {"head", [], [{"title", [], ["Test"]}]},
             {"body", [],
              [
                {"h1", [{"uuid", "1234"}], ["Hello, World!"]},
                {"p", [], ["This is a test"]}
              ]}
           ]}
      }

      assert {:ok, path_to} = P4.Html.Writer.write(document)

      path_contents = File.read!(path_to)

      assert path_contents ===
               "<html lang=\"en\"><head><title>Test</title></head><body><h1 uuid=\"1234\">Hello, World!</h1><p>This is a test</p></body></html>"
    end

    test "returns an error when creating temp file fails" do
      expect(P4.IO.File, :create_temp, fn -> {:error, :some_reason} end)
      assert {:error, reason} = P4.Html.Writer.write(%P4.Html.Document{document: nil})

      assert reason === %P4.Error.Html.Writer{
               message: "Failure writing HTML file",
               reason: :some_reason,
               context: %{document: nil}
             }
    end

    test "returns an error when writing to the file fails" do
      expect(P4.IO.File, :create_temp, fn -> {:ok, "/tmp/some_path"} end)
      expect(File, :write, fn _path, _content -> {:error, :some_reason} end)

      document =
        {"html", [{"lang", "en"}],
         [
           {"head", [], []},
           {"body", [], []}
         ]}

      assert {:error, reason} =
               P4.Html.Writer.write(%P4.Html.Document{
                 document: document
               })

      assert reason === %P4.Error.Html.Writer{
               message: "Failure writing HTML file",
               reason: :some_reason,
               context: %{path: "/tmp/some_path", document: document}
             }
    end
  end
end
