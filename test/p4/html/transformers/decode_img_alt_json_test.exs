defmodule P4.Html.Transformer.DecodeImgAltJsonTest do
  use ExUnit.Case, async: true
  use Mimic

  test "decodes the JSON-encoded alt tag into an alt text and data-uuid property" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "{\"message\":\"An image\",\"uuid\":\"1234567890\"}" |> Base.encode64()}
               ], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"alt", "An image"}, {"data-uuid", "1234567890"}, {"src", "image.jpg"}],
               []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.DecodeImgAltJson.transform(input_document)

    assert expected_document === output_document
  end

  test "logs when the alt tag is not a Base64-encoded string" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"}
               ], []}
            ]}
         ]}
    }

    expect(P4.Logger, :error, 1, fn message, meta ->
      assert message === "failed to decode alt tag"

      assert meta[:error] === %ArgumentError{
               message: "non-alphabet character found: \" \" (byte 32)"
             }
    end)

    assert input_document == P4.Html.Transformer.DecodeImgAltJson.transform(input_document)
  end

  test "logs when the alt tag is not a Base64-encoded JSON string" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image" |> Base.encode64()}
               ], []}
            ]}
         ]}
    }

    expect(P4.Logger, :error, 1, fn message, meta ->
      assert message === "failed to decode alt tag"
      assert meta[:error] === %Jason.DecodeError{position: 0, token: nil, data: "An image"}
    end)

    assert input_document == P4.Html.Transformer.DecodeImgAltJson.transform(input_document)
  end

  test "does not set an alt text if the message is missing in the encoded alt" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "{\"uuid\":\"1234567890\"}" |> Base.encode64()}
               ], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"data-uuid", "1234567890"}, {"src", "image.jpg"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.DecodeImgAltJson.transform(input_document)

    assert expected_document === output_document
  end

  test "does not set a data-uuid if the uuid is missing in the encoded alt" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "{\"message\":\"An image\"}" |> Base.encode64()}
               ], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"alt", "An image"}, {"src", "image.jpg"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.DecodeImgAltJson.transform(input_document)

    assert expected_document === output_document
  end

  test "does not alter the element if the alt tag is missing" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"}
               ], []}
            ]}
         ]}
    }

    reject(P4.Logger, :error, 2)

    assert input_document == P4.Html.Transformer.DecodeImgAltJson.transform(input_document)
  end
end
