defmodule P4.Html.Transformer.StripBlockQuotesTest do
  use ExUnit.Case, async: true

  test "P4.Html.Transformer.StripBlockQuotes removes blockquotes as only child of th" do
    input_document = make_input_document("th")
    expected_document = make_output_document("th")

    output_document = P4.Html.Transformer.StripBlockQuotes.transform(input_document)
    assert expected_document === output_document
  end

  test "P4.Html.Transformer.StripBlockQuotes removes blockquotes as only child of td" do
    input_document = make_input_document("td")
    expected_document = make_output_document("td")

    output_document = P4.Html.Transformer.StripBlockQuotes.transform(input_document)
    assert expected_document === output_document
  end

  test "P4.Html.Transformer.StripBlockQuotes removes blockquotes as only child of li" do
    input_document = make_input_document("li")
    expected_document = make_output_document("li")

    output_document = P4.Html.Transformer.StripBlockQuotes.transform(input_document)
    assert expected_document === output_document
  end

  defp make_input_document(container_element) when is_binary(container_element) do
    %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {container_element, [],
               [
                 {"blockquote", [],
                  [
                    {"p", [], ["Test"]}
                  ]}
               ]}
            ]}
         ]}
    }
  end

  defp make_output_document(container_element) when is_binary(container_element) do
    %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {container_element, [],
               [
                 {"p", [], ["Test"]}
               ]}
            ]}
         ]}
    }
  end
end
