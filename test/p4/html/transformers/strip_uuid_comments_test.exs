defmodule P4.Html.Transformer.StripUUIDCommentsTest do
  use ExUnit.Case, async: true

  test "leaves a simple document without UUID comments intact" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"h1", [{"class", "title"}], ["Title"]},
              {"p", [{"class", "main"}, {"lang"}], ["en"]},
              {"h2", [], ["Subtitle"]},
              {"p", [{"draggable", "true"}], ["world"]}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert input_document === output_document
  end

  test "moves UUID comments in h1 headers to data attribute on h1 headers" do
    input_document = make_input_document("h1")
    expected_document = make_output_document("h1")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in h2 headers to data attribute on h2 headers" do
    input_document = make_input_document("h2")
    expected_document = make_output_document("h2")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in h3 headers to data attribute on h3 headers" do
    input_document = make_input_document("h3")
    expected_document = make_output_document("h3")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in h4 headers to data attribute on h4 headers" do
    input_document = make_input_document("h4")
    expected_document = make_output_document("h4")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in h5 headers to data attribute on h5 headers" do
    input_document = make_input_document("h5")
    expected_document = make_output_document("h5")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in h6 headers to data attribute on h6 headers" do
    input_document = make_input_document("h6")
    expected_document = make_output_document("h6")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in paragraphs to data attribute on paragraphs" do
    input_document = make_input_document("p")
    expected_document = make_output_document("p")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in table data to data attribute on paragraphs" do
    input_document = make_input_document("td")
    expected_document = make_output_document("td")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in table headers to data attribute on paragraphs" do
    input_document = make_input_document("th")
    expected_document = make_output_document("th")

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "handles UUID comments on paragraphs with drop caps" do
    input_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"body", [],
           [
             {"p", [],
              [
                {"span", [{"class", "comment-start"}, {"id", "421"}],
                 [
                   {"span", [{"class", "comment-start"}, {"id", "420"}],
                    ["decade00-0000-4000-a000-000000000000"]},
                   "E",
                   {"span", [{"class", "comment-end"}, {"id", "420"}], []},
                   "facade00-0000-4000-a000-000000000000"
                 ]},
                "lement Type p",
                {"span", [{"class", "comment-end"}, {"id", "421"}], []}
              ]}
           ]}
        ]
      }
    }

    expected_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"body", [],
           [
             {"p", [{"data-uuid", "facade00-0000-4000-a000-000000000000"}],
              ["E", "lement Type p"]}
           ]}
        ]
      }
    }

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  test "moves UUID comments in title tag to data attribute on title tag" do
    input_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [],
           [
             {"title", [],
              [
                {"span", [{"class", "comment-start"}], ["1234567890"]},
                "Document Title",
                {"span", [{"class", "comment-end"}], []}
              ]}
           ]}
        ]
      }
    }

    expected_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [],
           [
             {"title", [{"data-uuid", "1234567890"}], ["Document Title"]}
           ]}
        ]
      }
    }

    output_document = P4.Html.Transformer.StripUUIDComments.transform(input_document)

    assert expected_document === output_document
  end

  defp make_input_document(element_type) when is_binary(element_type) do
    %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"body", [],
           [
             {element_type, [],
              [
                {"span", [{"class", "comment-start"}], ["1234567890"]},
                "Element Type #{element_type}",
                {"span", [{"class", "comment-end"}], []}
              ]}
           ]}
        ]
      }
    }
  end

  defp make_output_document(element_type) when is_binary(element_type) do
    %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"body", [],
           [
             {element_type, [{"data-uuid", "1234567890"}], ["Element Type #{element_type}"]}
           ]}
        ]
      }
    }
  end
end
