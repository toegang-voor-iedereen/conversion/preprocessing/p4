defmodule P4.Html.Transformer.CombineMultilineTitleTest do
  use ExUnit.Case, async: true

  test "P4.Html.Transformer.CombineMultilineTitle combines multiline titles" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {"title", [],
               [
                 {"p", [], ["Line 1 "]},
                 {"p", [], ["Line 2 "]},
                 {"p", [], ["Line 3 "]}
               ]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {
                "title",
                [],
                [
                  "Line 1 ",
                  "Line 2 ",
                  "Line 3 "
                ]
              }
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.CombineMultilineTitle.transform(input_document)

    assert expected_document === output_document
  end

  test "P4.Html.Transformer.CombineMultilineTitle combines multiline titles even if they contain formatted text" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {"title", [],
               [
                 {"p", [], [{"u", [], ["Line 1 "]}]},
                 {"p", [], [{"strong", [], ["Line 2 "]}]},
                 {"p", [], [{"em", [], ["Line 3 "]}]},
                 {"p", [], [{"em", [], [{"u", [], ["Line 4 "]}]}]},
                 {"p", [], [{"em", [], [{"strong", [], ["Line 5 "]}]}]},
                 {"p", [], [{"strong", [], [{"u", [], ["Line 6 "]}]}]},
                 {"p", [], [{"em", [], [{"u", [], [{"strong", [], ["Line 7 "]}]}]}]}
               ]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {
                "title",
                [],
                [
                  "Line 1 ",
                  "Line 2 ",
                  "Line 3 ",
                  "Line 4 ",
                  "Line 5 ",
                  "Line 6 ",
                  "Line 7 "
                ]
              }
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.CombineMultilineTitle.transform(input_document)

    assert expected_document === output_document
  end

  test "P4.Html.Transformer.CombineMultilineTitle leaves singleline titles intact" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {"title", [], ["Single Line Title"]}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.CombineMultilineTitle.transform(input_document)

    assert input_document === output_document
  end

  test "P4.Html.Transformer.CombineMultilineTitle removes formatting from singleline titles" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {"title", [], [{"u", [], ["Single Line Title"]}]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"head", [],
            [
              {"title", [], ["Single Line Title"]}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.CombineMultilineTitle.transform(input_document)

    assert expected_document === output_document
  end
end
