defmodule P4.Html.Transformer.StripSpansTest do
  use ExUnit.Case, async: true
  use Mimic

  setup do
    stub(P4.Logger, :warning, fn _ -> :ok end)
    :ok
  end

  test "strips remaining comment spans" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [],
               [
                 {"span", [{"class", "comment-start"}], ["1234567890"]},
                 "Some Text",
                 {"span", [{"class", "comment-end"}], []}
               ]},
              {"td", [],
               [
                 {"span", [{"class", "comment-start"}], ["1234567890"]},
                 "Some Text",
                 {"span", [{"class", "comment-end"}], []}
               ]},
              {"th", [],
               [
                 {"span", [{"class", "comment-start"}], ["1234567890"]},
                 "Some Text",
                 {"span", [{"class", "comment-end"}], []}
               ]},
              {"div", [],
               [
                 "Wow",
                 {"span", [{"class", "comment-start"}], ["1234567890"]},
                 "Some Text",
                 {"span", [{"class", "comment-end"}], []}
               ]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["Some Text"]},
              {"td", [], ["Some Text"]},
              {"th", [], ["Some Text"]},
              {"div", [], ["Wow", "Some Text"]}
            ]}
         ]}
    }

    expect(P4.Logger, :warning, 4, fn message ->
      assert message ===
               "Removing a comment span that should have been removed by the StripUUIDComments transformer"
    end)

    output_document = P4.Html.Transformer.StripSpans.transform(input_document)

    assert expected_document === output_document
  end

  test "strips anchor spans from document" do
    input_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [],
           [
             {"title", [], ["Test"]},
             {"span", [{"class", "anchor"}], ["1234"]}
           ]},
          {"body", [],
           [
             {"span", [{"class", "anchor"}], ["1234"]},
             {"p", [], ["Some Text"]},
             {"span", [{"class", "anchor"}], ["5678"]},
             {"td", [], ["Some Text"]},
             {"span", [{"class", "anchor"}], ["9012"]},
             {"th", [], ["Some Text"]},
             {"span", [{"class", "anchor"}], ["3456"]},
             {"div", [], ["Wow", "Some Text"]},
             {"span", [{"class", "anchor"}], ["7890"]}
           ]}
        ]
      }
    }

    expected_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [],
           [
             {"title", [], ["Test"]}
           ]},
          {"body", [],
           [
             {"p", [], ["Some Text"]},
             {"td", [], ["Some Text"]},
             {"th", [], ["Some Text"]},
             {"div", [], ["Wow", "Some Text"]}
           ]}
        ]
      }
    }

    output_document = P4.Html.Transformer.StripSpans.transform(input_document)

    assert expected_document === output_document
  end

  test "stips all spans from title" do
    input_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [],
           [
             {"title", [],
              [
                {"span", [{"class", "anchor"}], ["1234"]},
                "Test",
                {"span", [], ["1234"]},
                {"span", [{"class", "other"}], ["1234"]},
                {"span", [{"class", "test"}], ["1234"]},
                {"span", [{"class", "anchor"}], ["1234"]},
                {"span", [{"class", "comment-start"}], ["1234"]},
                {"span", [{"class", "comment-end"}], []}
              ]}
           ]}
        ]
      }
    }

    expected_document = %P4.Html.Document{
      document: {
        "html",
        [],
        [
          {"head", [], [{"title", [], ["Test"]}]}
        ]
      }
    }

    reject(P4.Logger, :warning, 1)

    output_document = P4.Html.Transformer.StripSpans.transform(input_document)

    assert expected_document === output_document
  end
end
