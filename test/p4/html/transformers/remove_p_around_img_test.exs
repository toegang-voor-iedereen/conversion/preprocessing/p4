defmodule P4.Html.Transformer.RemovePAroundImgTest do
  use ExUnit.Case, async: true

  test "removes <p> tags tightly wrapping <img> element" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], [{"img", [{"src", "image.jpg"}, {"alt", "An image"}], []}]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemovePAroundImg.transform(input_document)

    assert expected_document === output_document
  end

  test "splits a paragraph when the <img> tag is not the only child" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [],
               [
                 "Some text before image",
                 {"img", [{"src", "image.jpg"}, {"alt", "An image"}], []},
                 "Some text after image",
                 "Some more text after the image"
               ]}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], ["Some text before image"]},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}], []},
              {"p", [], ["Some text after image"]},
              {"p", [], ["Some more text after the image"]}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemovePAroundImg.transform(input_document)

    assert expected_document === output_document
  end

  test "strips an <img> from all attributes except for src, alt and data-uuid" do
    input_element =
      {"img",
       [
         {"src", "test.jpeg"},
         {"alt", "A test image"},
         {"data-uuid", "some-id"},
         {"id", "some-id"},
         {"class", "some-class"},
         {"style", "{}"}
       ], []}

    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [], [input_element]}
            ]}
         ]}
    }

    expected_element =
      {"img",
       [
         {"src", "test.jpeg"},
         {"alt", "A test image"},
         {"data-uuid", "some-id"}
       ], []}

    expected_document = %P4.Html.Document{
      document: {"html", [], [{"body", [], [expected_element]}]}
    }

    output_element = P4.Html.Transformer.RemovePAroundImg.transform(input_element)
    output_document = P4.Html.Transformer.RemovePAroundImg.transform(input_document)

    assert expected_element === output_element
    assert expected_document === output_document
  end

  test "leaves a simple document without <p> tags around <img> tags intact" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"h1", [{"class", "title"}], ["Title"]},
              {"p", [{"class", "main"}, {"lang"}], ["en"]},
              {"h2", [], ["Subtitle"]},
              {"p", [{"draggable", "true"}], ["world"]},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemovePAroundImg.transform(input_document)

    assert input_document === output_document
  end

  test "does not split a paragraph with multiple child elements but no <img> element" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"p", [],
               [
                 "To see the DOCX conversion in action, simply add this file to calibre using the ",
                 {"strong", [], ["\"Add Books\""]},
                 " button and then click ",
                 {"strong", [], ["\"Convert\"."]},
                 " Set the output format in the top right corner of the conversion dialog to EPUB or AZW3 and click ",
                 {"strong", [], ["\"OK\"."]}
               ]}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemovePAroundImg.transform(input_document)

    assert input_document === output_document
  end
end
