defmodule P4.Html.Transformer.RemoveMiniImagesTest do
  use ExUnit.Case, async: true
  use Mimic

  test "removes mini images with height less than 0.5 inch" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0.49in"}],
               []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "height:0.50in"}],
               []},
              {"img", [{"src", "keep.jpg"}, {"alt", "An image"}, {"style", "height:0.51in"}], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "keep.jpg"}, {"alt", "An image"}, {"style", "height:0.51in"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemoveMiniImages.transform(input_document)

    assert expected_document === output_document
  end

  test "removes mini images with width less than 0.5 inch" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.0in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.4in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.49in"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.50in"}], []},
              {"img", [{"src", "keep.jpg"}, {"alt", "An image"}, {"style", "width:0.51in"}], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "keep.jpg"}, {"alt", "An image"}, {"style", "width:0.51in"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemoveMiniImages.transform(input_document)

    assert expected_document === output_document
  end

  test "removes mini images with width or height less than 0.5 inch" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:0.0in;height:10.0in"}
               ], []},
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:10.0in;height:0.0in"}
               ], []},
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:0.5in;height:10.0in"}
               ], []},
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:10.0in;height:0.5in"}
               ], []},
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:0.25in;height:0.25in"}
               ], []},
              {"img",
               [
                 {"src", "image.jpg"},
                 {"alt", "An image"},
                 {"style", "width:-0.4in;height:-0.4in"}
               ], []},
              {"img",
               [
                 {"src", "keep.jpg"},
                 {"alt", "An image"},
                 {"style", "width:0.51in;height:0.51in"}
               ], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img",
               [
                 {"src", "keep.jpg"},
                 {"alt", "An image"},
                 {"style", "width:0.51in;height:0.51in"}
               ], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemoveMiniImages.transform(input_document)

    assert expected_document === output_document
  end

  test "keeps images with unknown size unit" do
    input_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5em"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5rem"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5vh"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5vw"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5%"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5pt"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5pc"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5mm"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5"}], []}
            ]}
         ]}
    }

    expected_document = %P4.Html.Document{
      document:
        {"html", [],
         [
           {"body", [],
            [
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5em"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5rem"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5vh"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5vw"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5%"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5pt"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5pc"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5mm"}], []},
              {"img", [{"src", "image.jpg"}, {"alt", "An image"}, {"style", "width:0.5"}], []}
            ]}
         ]}
    }

    output_document = P4.Html.Transformer.RemoveMiniImages.transform(input_document)

    assert expected_document === output_document
  end
end
