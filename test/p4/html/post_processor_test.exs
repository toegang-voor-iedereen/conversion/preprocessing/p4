defmodule P4.Html.PostProcessorTest do
  use ExUnit.Case, async: true
  use Mimic

  test "process/1 calls all transformers on the document" do
    document = %P4.Html.Document{}

    for transformer <- P4.Html.PostProcessor.transformers() do
      expect(transformer, :transform, 1, &identity/1)
    end

    assert {:ok, _document} = P4.Html.PostProcessor.process(document)
  end

  def identity(x), do: x
end
