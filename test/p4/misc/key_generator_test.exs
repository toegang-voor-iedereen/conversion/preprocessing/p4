defmodule P4.Misc.KeyGeneratorTest do
  use ExUnit.Case, async: true

  test "all keys are random" do
    unique_count =
      1..100
      |> Enum.map(fn _ -> P4.Misc.KeyGenerator.generate(24) end)
      |> Enum.uniq()
      |> length()

    assert unique_count === 100
  end

  test "accepts prefix but suffix not required" do
    key = P4.Misc.KeyGenerator.generate(24, prefix: "prefix.")

    assert Regex.match?(~r/^prefix\.[a-zA-Z0-9]{24}$/, key)

    assert String.starts_with?(key, "prefix.")
    assert String.length(key) === 24 + String.length("prefix.")
  end

  test "accepts suffix but prefix not required" do
    key = P4.Misc.KeyGenerator.generate(12, suffix: ".suffix")

    assert Regex.match?(~r/^[a-zA-Z0-9]{12}\.suffix$/, key)

    assert String.ends_with?(key, ".suffix")
    assert String.length(key) === 12 + String.length(".suffix")
  end

  test "accepts prefix and suffix" do
    key = P4.Misc.KeyGenerator.generate(6, prefix: "prefix.", suffix: ".suffix")

    assert Regex.match?(~r/^prefix\.[a-zA-Z0-9]{6}\.suffix$/, key)

    assert String.starts_with?(key, "prefix.")
    assert String.ends_with?(key, ".suffix")
    assert String.length(key) === 6 + String.length("prefix.") + String.length(".suffix")
  end
end
