defmodule P4.Document.AlternateContentMutatorTest do
  use ExUnit.Case, async: true
  doctest P4.Document.AlternateContentMutator

  test "process_alternate_content/1 on particles/alternate_content_element.xml" do
    "test/__fixtures__/particles/alternate_content_element.xml"
    |> TestHelper.load_xml()
    |> P4.Document.AlternateContentMutator.process_alternate_content()
    |> P4.Xml.Helper.wrap_elements("root")
    |> TestHelper.test_xml_snapshot("test/__snapshots__/P4.Document.AlternateContentMutator/process_alternate_content/alternate_content_element")
  end
end
