defmodule P4.Docx.DocumentContextTest do
  use ExUnit.Case, async: true
  doctest P4.Docx.DocumentContext

  @heading1_style {"w:style", [{"w:styleId", "heading1"}], []}
  @heading2_style {"w:style", [{"w:styleId", "heading2"}], []}
  @normal_style {"w:style", [{"w:styleId", "normal"}], []}
  @toc_style {"w:style", [{"w:styleId", "toc"}], []}

  @context %P4.Docx.DocumentContext{
    style_map: %{
      "heading 1" => @heading1_style,
      "heading 2" => @heading2_style,
      "normal" => @normal_style,
      "toc" => @toc_style
    },
    specific_style_ids: %P4.Docx.SpecificStyleIdsState{
      heading: ["heading 1", "heading 2"],
      normal: "normal",
      table_of_contents: ["toc"]
    }
  }

  describe "get_style" do
    test "returns the style with the given id" do
      assert P4.Docx.DocumentContext.get_style(@context, "heading 1") === @heading1_style
      assert P4.Docx.DocumentContext.get_style(@context, "heading 2") === @heading2_style
      assert P4.Docx.DocumentContext.get_style(@context, "normal") === @normal_style
      assert P4.Docx.DocumentContext.get_style(@context, "toc") === @toc_style
    end

    test "returns the default value if the style is not found" do
      assert P4.Docx.DocumentContext.get_style(@context, "unknown", "default") === "default"
      assert P4.Docx.DocumentContext.get_style(@context, "unknown") === nil
    end
  end

  describe "is_toc_style_id?" do
    test "returns true if the style id is a table of contents style id" do
      assert P4.Docx.DocumentContext.is_toc_style_id?("toc", @context)
    end

    test "returns false if the style id is not a table of contents style id" do
      refute P4.Docx.DocumentContext.is_toc_style_id?("heading 1", @context)
      refute P4.Docx.DocumentContext.is_toc_style_id?("normal", @context)
    end
  end

  describe "is_heading_style_id?" do
    test "returns true if the style id is a heading style id" do
      assert P4.Docx.DocumentContext.is_heading_style_id?("heading 1", @context)
      assert P4.Docx.DocumentContext.is_heading_style_id?("heading 2", @context)
    end

    test "returns false if the style id is not a heading style id" do
      refute P4.Docx.DocumentContext.is_heading_style_id?("normal", @context)
      refute P4.Docx.DocumentContext.is_heading_style_id?("toc", @context)
    end
  end

  describe "is_normal_style_id?" do
    test "returns true if the style id is a normal style id" do
      assert P4.Docx.DocumentContext.is_normal_style_id?("normal", @context)
    end

    test "returns false if the style id is not a normal style id" do
      refute P4.Docx.DocumentContext.is_normal_style_id?("heading 1", @context)
      refute P4.Docx.DocumentContext.is_normal_style_id?("toc", @context)
    end
  end
end
