defmodule P4.Docx.AnnotatorTest do
  use ExUnit.Case, async: true
  use ExUnit.Parameterized
  use Mimic

  @moduletag timeout: :infinity

  alias P4.Docx.Annotator

  @spec assert_valid_uuid_comment(Saxy.XML.element()) :: :ok
  def assert_valid_uuid_comment(comment = {name, _, _}) do
    assert name === "w:comment"

    # assert that each comment has a valid ID
    id = P4.Xml.Helper.get_attribute_value(comment, "w:id")
    assert String.match?(id, ~r/^[0-9]+$/)

    # assert that each comment is a valid UUID
    {_, _, [comment_text]} =
      comment
      |> P4.Xml.Helper.get_child_by_name("w:p")
      |> P4.Xml.Helper.get_child_by_name("w:r")
      |> P4.Xml.Helper.get_child_by_name("w:t")

    assert {:ok, _} = Ecto.UUID.cast(comment_text)
  end

  def assert_valid_uuid_comment(comment) do
    flunk(
      "Invalid UUID comment structure, expecting a tuple with 3 elements, but got: #{inspect(comment)}"
    )
  end

  @spec assert_document_has_valid_uuid_comments(P4.Docx.Document.t(), integer()) :: :ok
  def assert_document_has_valid_uuid_comments(
        %P4.Docx.Document{comments: comments_root},
        expected_num_comments
      ) do
    {_, _, comments} = comments_root
    assert length(comments) == expected_num_comments

    Enum.each(comments, &assert_valid_uuid_comment/1)

    # assert that all comment IDs are unique.
    comment_ids =
      Enum.map(comments, fn comment ->
        P4.Xml.Helper.get_attribute_value(comment, "w:id")
      end)

    assert Enum.uniq(comment_ids) == comment_ids
  end

  @spec assert_element_has_comment_range(Saxy.XML.element()) :: :ok
  def assert_element_has_comment_range({_, _, children}) do
    assert Enum.any?(children, fn
             {"w:commentRangeStart", _, _} -> true
             _ -> false
           end)

    assert Enum.any?(children, fn
             {"w:commentRangeEnd", _, _} -> true
             _ -> false
           end)
  end

  @spec assert_elements_have_comment_ranges(P4.Docx.Document.t(), String.t()) :: :ok
  def assert_elements_have_comment_ranges(
        %P4.Docx.Document{document: root},
        element_type
      ) do
    P4.Xml.Helper.find_elements_recursively_by_name(root, element_type)
    |> Enum.each(&assert_element_has_comment_range/1)
  end

  describe "annotate/1" do
    test_with_params "annotates all paragraphs with comments containing a UUID", fn path ->
      # Given a document with paragraphs
      assert {:ok, docx_document} = P4.Docx.Reader.read_from_path(path, "")

      # When the document is annotated
      assert {:ok, result} = Annotator.annotate(docx_document)

      # Then the document contains valid UUID comments for each 36 paragraphs (there are also a bunch of empty paragraphs in there)
      assert_document_has_valid_uuid_comments(result, 36)

      assert_elements_have_comment_ranges(result, "w:p")
    end do
      [
        {"test/__fixtures__/docx/annotator_test.docx"},
        {"test/__fixtures__/docx/annotator_test.without_styles.docx"}
      ]
    end

    test_with_params "annotates a table of contents (ToC) and table of figures (ToF) with a UUID comment",
                     fn path ->
                       # Given a document with paragraphs
                       assert {:ok, docx_document} =
                                P4.Docx.Reader.read_from_path(path, "")

                       # When the document is annotated
                       assert {:ok, result} = Annotator.annotate(docx_document)

                       # Then the document contains valid UUID comments for all paragraphs and the ToC
                       assert_document_has_valid_uuid_comments(result, 42)

                       assert_elements_have_comment_ranges(result, "w:sdt")
                       assert_elements_have_comment_ranges(result, "w:p")
                       # note: the table of figures is a w:p
                     end do
      [
        {"test/__fixtures__/docx/annotator_toc_test.docx"},
        {"test/__fixtures__/docx/annotator_toc_test.without_styles.docx"}
      ]
    end

    test_with_params "annotates images with a UUID comment by description in JSON", fn path ->
      # Given a document with paragraphs and some images
      assert {:ok, docx_document} = P4.Docx.Reader.read_from_path(path, "")

      # When the document is annotated
      assert {:ok, result} = Annotator.annotate(docx_document)

      # Then all images must contain a UUID comment in the description attribute
      img_elements = P4.Xml.Helper.find_elements_recursively_by_name(result.document, "w:drawing")

      Enum.each(img_elements, fn img_element ->
        description_encoded =
          img_element
          |> P4.Xml.Helper.find_element_recursively_by_name("wp:docPr")
          |> P4.Xml.Helper.get_attribute_value("descr")

        refute is_nil(description_encoded)
        description = description_encoded |> Base.decode64!() |> Jason.decode!()

        assert is_binary(description["message"]) or is_nil(description["message"])
        assert {:ok, _} = Ecto.UUID.cast(description["uuid"])
      end)
    end do
      [
        {"test/__fixtures__/docx/annotator_images_test.docx"},
        {"test/__fixtures__/docx/annotator_images_test.without_styles.docx"}
      ]
    end

    test_with_params "annotates all table cell paragraphs with comments containing a UUID",
                     fn path ->
                       # Given a document with tables. Each table cell `w:tc` contains a paragraph `w:p`
                       assert {:ok, docx_document} = P4.Docx.Reader.read_from_path(path, "")

                       # When the document is annotated
                       assert {:ok, result} = Annotator.annotate(docx_document)

                       # Then the document contains valid UUID comments for each 35 paragraphs
                       assert_document_has_valid_uuid_comments(result, 35)

                       assert_elements_have_comment_ranges(result, "w:p")
                     end do
      [
        {"test/__fixtures__/docx/annotator_tables_test.docx"},
        {"test/__fixtures__/docx/annotator_tables_test.without_styles.docx"}
      ]
    end
  end

  describe "snapshot tests" do
    test_with_params "annotate/1 on input documents", fn num_uuid_calls,
                                                         input_docx_path,
                                                         document_snapshot_path,
                                                         comments_snapshot_path ->
      # there are 23 elements to be annotated in this document
      mock_uuid_generate(num_uuid_calls)

      assert {:ok, docx_document} = P4.Docx.Reader.read_from_path(input_docx_path, "")

      assert {:ok, result} = Annotator.annotate(docx_document)

      TestHelper.test_xml_snapshot(result.document, document_snapshot_path)
      TestHelper.test_xml_snapshot(result.comments, comments_snapshot_path)
    end do
      [
        {
          23,
          "test/__fixtures__/docx/annotator_images_test.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_images_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_images_test/comments"
        },
        {
          23,
          "test/__fixtures__/docx/annotator_images_test.without_styles.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_images_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_images_test/comments"
        },
        {
          36,
          "test/__fixtures__/docx/annotator_tables_test.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_tables_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_tables_test/comments"
        },
        {
          36,
          "test/__fixtures__/docx/annotator_tables_test.without_styles.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_tables_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_tables_test/comments"
        },
        {
          36,
          "test/__fixtures__/docx/annotator_test.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_test/comments"
        },
        {
          36,
          "test/__fixtures__/docx/annotator_test.without_styles.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_test/comments"
        },
        {
          43,
          "test/__fixtures__/docx/annotator_toc_test.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_toc_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_toc_test/comments"
        },
        {
          43,
          "test/__fixtures__/docx/annotator_toc_test.without_styles.docx",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_toc_test/document",
          "test/__snapshots__/P4.Docx.Annotator/annotate/annotator_toc_test/comments"
        }
      ]
    end
  end

  @doc """
  Mocks Ecto.UUID.generate to return predictable UUIDs for a set number of calls
  Inspiration taken from: https://elixirforum.com/t/implementing-a-counter-aka-how-to-keep-state-in-a-closure/17056/2
  """
  @spec mock_uuid_generate(integer) :: :ok
  def mock_uuid_generate(num_calls) when is_integer(num_calls) do
    {:ok, pid} = Agent.start_link(fn -> 0 end)
    get_and_increment = fn -> Agent.get_and_update(pid, fn i -> {i, i + 1} end) end

    expect(Ecto.UUID, :generate, num_calls, fn ->
      count = get_and_increment.()
      "00000000-1234-5678-0000-00#{count}"
    end)

    :ok
  end
end
