defmodule P4.Docx.WriterTest do
  use ExUnit.Case, async: true
  use Mimic

  doctest P4.Docx.Writer

  @sample_document %P4.Docx.Document{
    file_list: ["word/document.xml", "word/styles.xml", "word/theme/theme1.xml"],
    extraction_path: "/some/extraction/path",
    document: {"w:document", [], [{"w:body", [], []}]},
    styles: {"w:styles", [], [{"w:style", [], []}]},
    comments: {"w:comments", [], []}
  }

  test "cannot write word/document.xml" do
    {:error, reason} =
      mock_file_write(
        :error,
        "/some/extraction/path/word/document.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    assert {:error,
            %P4.Error.P4.Docx.Writer.WritingFile{
              message: "Failure writing file during writing outcome: :reason",
              reason: reason,
              context: %{
                extraction_path: "/some/extraction/path",
                relative_file_path: "word/document.xml"
              }
            }} ==
             P4.Docx.Writer.write(@sample_document)
  end

  test "cannot write word/comments.xml" do
    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/document.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    {:error, reason} =
      mock_file_write(
        :error,
        "/some/extraction/path/word/comments.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:comments/>"
      )

    assert {:error,
            %P4.Error.P4.Docx.Writer.WritingFile{
              message: "Failure writing file during writing outcome: :reason",
              reason: reason,
              context: %{
                extraction_path: "/some/extraction/path",
                relative_file_path: "word/comments.xml"
              }
            }} ==
             P4.Docx.Writer.write(@sample_document)
  end

  test "cannot write word/styles.xml" do
    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/document.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/comments.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:comments/>"
      )

    {:error, reason} =
      mock_file_write(
        :error,
        "/some/extraction/path/word/styles.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:styles><w:style/></w:styles>"
      )

    assert {:error,
            %P4.Error.P4.Docx.Writer.WritingFile{
              message: "Failure writing file during writing outcome: :reason",
              reason: reason,
              context: %{
                extraction_path: "/some/extraction/path",
                relative_file_path: "word/styles.xml"
              }
            }} ==
             P4.Docx.Writer.write(@sample_document)
  end

  test "cannot create temp path" do
    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/document.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/comments.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:comments/>"
      )

    :ok =
      mock_file_write(
        :ok,
        "/some/extraction/path/word/styles.xml",
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:styles><w:style/></w:styles>"
      )

    {:error, reason} = mock_create_temp_file(:error)

    assert {:error, reason} === P4.Docx.Writer.write(@sample_document)
  end

  test "cannot zip from directory" do
    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/document.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/comments.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:comments/>"
      )

    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/styles.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:styles><w:style/></w:styles>"
      )

    {:ok, path} = mock_create_temp_file(:ok)

    {:error, reason} =
      mock_zip_create_from_directory(
        :error,
        @sample_document.extraction_path,
        @sample_document.file_list,
        path
      )

    assert {:error,
            %P4.Error.Zip.CreatingFromDirectory{
              message: "Failure creating zip from directory: :reason",
              reason: reason,
              context: %{
                file_list: @sample_document.file_list,
                directory_path: "/some/extraction/path",
                output_zip_path: path
              }
            }} === P4.Docx.Writer.write(@sample_document)
  end

  test "successful writing" do
    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/document.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:document><w:body/></w:document>"
      )

    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/comments.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:comments/>"
      )

    :ok =
      mock_file_write(
        :ok,
        Path.join(@sample_document.extraction_path, "word/styles.xml"),
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><w:styles><w:style/></w:styles>"
      )

    {:ok, path} = mock_create_temp_file(:ok)

    {:ok, path} =
      mock_zip_create_from_directory(
        :ok,
        @sample_document.extraction_path,
        @sample_document.file_list,
        path
      )

    assert {:ok, path} === P4.Docx.Writer.write(@sample_document)
  end

  def mock_zip_create_from_directory(:ok, directory_path, file_list, output_zip_path) do
    output_zip_path_charlist = String.to_charlist(output_zip_path)
    file_list_charlists = Enum.map(file_list, &String.to_charlist/1)

    expect(
      :zip,
      :create,
      fn ^output_zip_path_charlist, ^file_list_charlists, [cwd: ^directory_path] ->
        {:ok, output_zip_path}
      end
    )

    {:ok, output_zip_path}
  end

  def mock_zip_create_from_directory(:error, directory_path, file_list, output_zip_path) do
    output_zip_path_charlist = String.to_charlist(output_zip_path)
    file_list_charlists = Enum.map(file_list, &String.to_charlist/1)

    expect(
      :zip,
      :create,
      fn ^output_zip_path_charlist, ^file_list_charlists, [cwd: ^directory_path] ->
        {:error, :reason}
      end
    )

    {:error, :reason}
  end

  defp mock_create_temp_file(:ok) do
    expect(P4.IO.File, :create_temp, fn -> {:ok, "/any/path"} end)

    {:ok, "/any/path"}
  end

  defp mock_create_temp_file(:error) do
    expect(P4.IO.File, :create_temp, fn -> {:error, :reason} end)

    {:error, :reason}
  end

  defp mock_file_write(:ok, path, content) do
    expect(File, :write, fn ^path, ^content -> :ok end)

    :ok
  end

  defp mock_file_write(:error, path, content) do
    expect(File, :write, fn ^path, ^content -> {:error, :reason} end)

    {:error, :reason}
  end
end
