defmodule P4.Docx.ReaderTest do
  use ExUnit.Case, async: true
  use Mimic

  doctest P4.Docx.Reader

  @id "test"

  describe "read_from_path/1 real file tests" do
    test "reads .docx file with UTF-16 BOM character" do
      # Note: this test file has a UTF-16 BOM character at the beginning,
      # which would cause the parser to fail if it doesn't trim BOMs characters.
      assert {:ok, document} =
               P4.Docx.Reader.read_from_path("test/__fixtures__/docx/utf-16-bom.docx", @id)

      assert is_struct(document, P4.Docx.Document)
    end
  end

  describe "read_from_path/1 unit tests" do
    test "cannot create tmp dir" do
      {:error, reason} = mock_temp_mkdir(:error)

      assert {:error,
              %P4.Error.P4.Docx.Reader.CreateTempDir{
                message: "Failure creating temporary directory: :reason",
                reason: reason,
                context: %{}
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "cannot extract" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:error, reason} = mock_zip_extract(:error, "document.docx", extraction_path)

      assert {:error,
              %P4.Error.Zip.Extracting{
                message: "Failure extracting zip to directory: :reason",
                reason: reason,
                context: %{archive_path: "document.docx", extraction_path: extraction_path}
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "cannot read word/document.xml" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)
      document_path = Path.join([extraction_path, "word/document.xml"])
      {:error, reason} = mock_file_read(:error, document_path)

      assert {:error, %File.Error{action: "stream", reason: reason, path: document_path}} ==
               P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "cannot read word/styles.xml" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)
      {:ok, _content} = mock_file_read(:ok, Path.join([extraction_path, "word/document.xml"]))
      styles_path = Path.join([extraction_path, "word/styles.xml"])
      {:error, reason} = mock_file_read(:error, styles_path)

      assert {:error, %File.Error{action: "stream", reason: reason, path: styles_path}} ==
               P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "cannot parse word/document.xml" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)

      {:ok, _content} =
        mock_file_read(:ok, Path.join([extraction_path, "word/document.xml"]), "Invalid XML")

      {:ok, _content} = mock_file_read(:ok, Path.join([extraction_path, "word/styles.xml"]))

      assert {:error,
              %P4.Error.P4.Docx.Reader.ParsingXml{
                context: "word/document.xml",
                message: "Failure parsing XML: unexpected byte \"I\", expected token: :lt",
                reason: %Saxy.ParseError{
                  reason: {:token, :lt},
                  binary: "Invalid XML",
                  position: 0
                }
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "cannot parse word/styles.xml" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)

      {:ok, _content} =
        mock_file_read(:ok, Path.join([extraction_path, "word/document.xml"]))

      {:ok, _content} =
        mock_file_read(:ok, Path.join([extraction_path, "word/styles.xml"]), "Invalid XML")

      assert {:error,
              %P4.Error.P4.Docx.Reader.ParsingXml{
                context: "word/styles.xml",
                message: "Failure parsing XML: unexpected byte \"I\", expected token: :lt",
                reason: %Saxy.ParseError{
                  reason: {:token, :lt},
                  binary: "Invalid XML",
                  position: 0
                }
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "successful reading" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)

      {:ok, _content} =
        mock_file_read(:ok, Path.join([extraction_path, "word/document.xml"]))

      {:ok, _content} = mock_file_read(:ok, Path.join([extraction_path, "word/styles.xml"]))

      {:ok, _content} = mock_file_read(:ok, Path.join([extraction_path, "word/comments.xml"]))

      assert {:ok,
              %P4.Docx.Document{
                id: @id,
                document: {"xml", [], []},
                extraction_path: extraction_path,
                file_list: [
                  "word/comments.xml",
                  "word/document.xml",
                  "word/styles.xml",
                  "word/theme/theme1.xml"
                ],
                comments: {"xml", [], []},
                styles: {"xml", [], []}
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end

    test "successful reading without comments.xml" do
      {:ok, extraction_path} = mock_temp_mkdir(:ok)
      {:ok, _file_list} = mock_zip_extract(:ok, "document.docx", extraction_path)

      {:ok, _content} =
        mock_file_read(:ok, Path.join([extraction_path, "word/document.xml"]))

      {:ok, _content} = mock_file_read(:ok, Path.join([extraction_path, "word/styles.xml"]))

      assert {:ok,
              %P4.Docx.Document{
                id: @id,
                document: {"xml", [], []},
                extraction_path: extraction_path,
                file_list: [
                  "word/comments.xml",
                  "word/document.xml",
                  "word/styles.xml",
                  "word/theme/theme1.xml"
                ],
                styles: {"xml", [], []},
                comments:
                  {"w:comments",
                   [
                     {"xmlns:wpc",
                      "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"},
                     {"xmlns:cx", "http://schemas.microsoft.com/office/drawing/2014/chartex"},
                     {"xmlns:cx1",
                      "http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"},
                     {"xmlns:cx2",
                      "http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"},
                     {"xmlns:cx3",
                      "http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"},
                     {"xmlns:cx4",
                      "http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"},
                     {"xmlns:cx5",
                      "http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"},
                     {"xmlns:cx6",
                      "http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"},
                     {"xmlns:cx7",
                      "http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"},
                     {"xmlns:cx8",
                      "http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"},
                     {"xmlns:mc", "http://schemas.openxmlformats.org/markup-compatibility/2006"},
                     {"xmlns:aink", "http://schemas.microsoft.com/office/drawing/2016/ink"},
                     {"xmlns:am3d", "http://schemas.microsoft.com/office/drawing/2017/model3d"},
                     {"xmlns:o", "urn:schemas-microsoft-com:office:office"},
                     {"xmlns:oel", "http://schemas.microsoft.com/office/2019/extlst"},
                     {"xmlns:r",
                      "http://schemas.openxmlformats.org/officeDocument/2006/relationships"},
                     {"xmlns:m", "http://schemas.openxmlformats.org/officeDocument/2006/math"},
                     {"xmlns:v", "urn:schemas-microsoft-com:vml"},
                     {"xmlns:wp14",
                      "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"},
                     {"xmlns:wp",
                      "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"},
                     {"xmlns:w10", "urn:schemas-microsoft-com:office:word"},
                     {"xmlns:w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main"},
                     {"xmlns:w14", "http://schemas.microsoft.com/office/word/2010/wordml"},
                     {"xmlns:w15", "http://schemas.microsoft.com/office/word/2012/wordml"},
                     {"xmlns:w16cex", "http://schemas.microsoft.com/office/word/2018/wordml/cex"},
                     {"xmlns:w16cid", "http://schemas.microsoft.com/office/word/2016/wordml/cid"},
                     {"xmlns:w16", "http://schemas.microsoft.com/office/word/2018/wordml"},
                     {"xmlns:w16sdtdh",
                      "http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"},
                     {"xmlns:w16se",
                      "http://schemas.microsoft.com/office/word/2015/wordml/symex"},
                     {"xmlns:wpg",
                      "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"},
                     {"xmlns:wpi",
                      "http://schemas.microsoft.com/office/word/2010/wordprocessingInk"},
                     {"xmlns:wne", "http://schemas.microsoft.com/office/word/2006/wordml"},
                     {"xmlns:wps",
                      "http://schemas.microsoft.com/office/word/2010/wordprocessingShape"},
                     {"mc:Ignorable", "w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14"}
                   ], []}
              }} == P4.Docx.Reader.read_from_path("document.docx", @id)
    end
  end

  defp mock_file_read(result, path, content \\ "<xml/>")

  defp mock_file_read(:ok, path, content) do
    expect(File, :stream!, 1, fn ^path, options ->
      assert options === [:trim_bom, encoding: :utf8]
      [content]
    end)

    {:ok, content}
  end

  defp mock_file_read(:error, path, _content) do
    expect(File, :stream!, 1, fn ^path, _options ->
      raise %File.Error{reason: :reason, path: path, action: "stream"}
    end)

    {:error, :reason}
  end

  defp mock_zip_extract(:ok, path, extraction_path) do
    path_as_charlist = String.to_charlist(path)

    result =
      {:ok,
       [
         Path.join([extraction_path, "word/document.xml"]),
         Path.join([extraction_path, "word/styles.xml"]),
         Path.join([extraction_path, "word/theme/theme1.xml"])
       ]}

    expect(:zip, :extract, fn ^path_as_charlist, [cwd: ^extraction_path] -> result end)

    result
  end

  defp mock_zip_extract(:error, path, extraction_path) do
    path_as_charlist = String.to_charlist(path)

    expect(:zip, :extract, fn ^path_as_charlist, [cwd: ^extraction_path] -> {:error, :reason} end)

    {:error, :reason}
  end

  defp mock_temp_mkdir(:ok) do
    expect(Temp, :mkdir, fn -> {:ok, "some-dir"} end)

    {:ok, "some-dir"}
  end

  defp mock_temp_mkdir(:error) do
    expect(Temp, :mkdir, fn -> {:error, :reason} end)

    {:error, :reason}
  end
end
