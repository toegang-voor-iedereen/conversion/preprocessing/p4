defmodule P4.Docx.Parser.StyleTest do
  use ExUnit.Case, async: true
  doctest P4.Docx.Parser.Style

  @heading_style_1 {"w:style", [{"w:type", "paragraph"}],
                    [{"w:name", [{"w:val", "heading 1"}], []}]}
  @heading_style_2 {"w:style", [{"w:type", "paragraph"}],
                    [{"w:name", [{"w:val", "Heading 1"}], []}]}
  @heading_style_3 {"w:style", [{"w:type", "paragraph"}],
                    [{"w:pPr", [], [{"w:outlineLvl", [{"w:val", "0"}], []}]}]}

  @normal_style {"w:style", [{"w:default", "1"}, {"w:type", "paragraph"}],
                 [{"w:name", [{"w:val", "Normal"}], []}]}

  @toc_style {"w:style", [{"w:styleId", "toc"}], []}
  @tof_style {"w:style", [{"w:styleId", "tableoffigures"}], []}

  describe "is_heading?" do
    test "returns true for a heading style defined by name" do
      assert P4.Docx.Parser.Style.is_heading?(@heading_style_1)
    end

    test "returns true for a heading style defined by capitalised name" do
      assert P4.Docx.Parser.Style.is_heading?(@heading_style_2)
    end

    test "returns false for a heading style defined solely by outline level" do
      refute P4.Docx.Parser.Style.is_heading?(@heading_style_3)
    end

    test "returns false for non-heading styles" do
      refute P4.Docx.Parser.Style.is_heading?(@normal_style)
    end
  end

  describe "has_heading_name?" do
    test "returns true for a style with a heading name" do
      assert P4.Docx.Parser.Style.has_heading_name?(@heading_style_1)
    end

    test "returns true for a style with a capitalized heading name" do
      assert P4.Docx.Parser.Style.has_heading_name?(@heading_style_2)
    end

    test "returns false for a style without a heading name" do
      refute P4.Docx.Parser.Style.has_heading_name?(@heading_style_3)
      refute P4.Docx.Parser.Style.has_heading_name?(@normal_style)
    end
  end

  describe "is_toc?" do
    test "returns true for a table of contents and table of figures style" do
      assert P4.Docx.Parser.Style.is_toc?(@toc_style)
      assert P4.Docx.Parser.Style.is_toc?(@tof_style)
    end

    test "returns false for all other styles" do
      refute P4.Docx.Parser.Style.is_toc?(@heading_style_1)
      refute P4.Docx.Parser.Style.is_toc?(@heading_style_2)
      refute P4.Docx.Parser.Style.is_toc?(@heading_style_3)
      refute P4.Docx.Parser.Style.is_toc?(@normal_style)
    end
  end

  describe "is_of_type?" do
    test "returns true for a style of the given type" do
      assert P4.Docx.Parser.Style.is_of_type?(@heading_style_1, "paragraph")
    end

    test "returns false for a style of a different type" do
      refute P4.Docx.Parser.Style.is_of_type?(@heading_style_1, "character")
    end
  end

  describe "is_default?" do
    test "returns true for a default style" do
      assert P4.Docx.Parser.Style.is_default?(@normal_style)
    end

    test "returns false for a non-default style" do
      refute P4.Docx.Parser.Style.is_default?(@heading_style_1)
    end
  end

  describe "is_top_level?" do
    test "returns true for a top level style" do
      assert P4.Docx.Parser.Style.is_top_level?(@normal_style)
    end

    test "returns false for a non-top level style" do
      style =
        {"w:style", [],
         [{"w:name", [{"w:val", "heading 2"}], []}, {"w:basedOn", [{"w:val", "heading 1"}], []}]}

      refute P4.Docx.Parser.Style.is_top_level?(style)
    end
  end
end
