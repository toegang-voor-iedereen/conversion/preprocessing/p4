defmodule P4.Docx.Parser.StylesTest do
  use ExUnit.Case, async: true
  doctest P4.Docx.Parser.Styles

  test "&get_toc_styles/1 for styles/cats-in-space.xml" do
    sheet = TestHelper.load_xml("test/__fixtures__/styles/cats-in-space.xml")

    assert P4.Docx.Parser.Styles.get_specific_style_ids(sheet) === %P4.Docx.SpecificStyleIdsState{
             table_of_contents: [
               "TOC9",
               "TOC8",
               "TOC7",
               "TOC6",
               "TOC5",
               "TOC4",
               "TOC3",
               "TOC2",
               "TOC1",
               "TOCHeading"
             ],
             normal: "Normal",
             heading: ["Heading3", "Heading2", "Heading1"]
           }
  end

  test "&get_toc_styles/1 for styles/cats-in-space-no-defaults.xml" do
    sheet = TestHelper.load_xml("test/__fixtures__/styles/cats-in-space-no-defaults.xml")

    assert P4.Docx.Parser.Styles.get_specific_style_ids(sheet) === %P4.Docx.SpecificStyleIdsState{
             table_of_contents: [
               "TOC9",
               "TOC8",
               "TOC7",
               "TOC6",
               "TOC5",
               "TOC4",
               "TOC3",
               "TOC2",
               "TOC1",
               "TOCHeading"
             ],
             normal: "Normal",
             heading: ["Heading3", "Heading2", "Heading1"]
           }
  end

  test "&get_toc_styles/1 for styles/MsWord.xml" do
    sheet = TestHelper.load_xml("test/__fixtures__/styles/MsWord.xml")

    assert P4.Docx.Parser.Styles.get_specific_style_ids(sheet) === %P4.Docx.SpecificStyleIdsState{
             table_of_contents: ["TOC8", "TOC2", "TOC7", "TableofFigures", "TOC1"],
             normal: "Normal",
             heading: [
               "Heading9",
               "Heading8",
               "Heading7",
               "Heading6",
               "Heading5",
               "Heading4",
               "Heading3",
               "Heading2",
               "Heading1"
             ]
           }
  end

  test "&get_toc_styles/1 for styles/headings_without_outline_level.xml" do
    sheet = TestHelper.load_xml("test/__fixtures__/styles/headings_without_outline_level.xml")

    assert P4.Docx.Parser.Styles.get_specific_style_ids(sheet) === %P4.Docx.SpecificStyleIdsState{
             heading: ["Heading6", "Heading5", "Heading4", "Heading3", "Heading2", "Heading1"],
             normal: "Normal",
             table_of_contents: []
           }
  end

  test "&get_toc_styles/1 for styles/styles-with-missing-based-on.xml" do
    sheet = TestHelper.load_xml("test/__fixtures__/styles/styles-with-missing-based-on.xml")

    assert P4.Docx.Parser.Styles.get_specific_style_ids(sheet) === %P4.Docx.SpecificStyleIdsState{
             table_of_contents: [],
             normal: "Normal",
             heading: ["Heading2", "Heading1", "Heading3"]
           }
  end
end
