defmodule P4.Docx.Transformer.RemoveMiniImagesTest do
  use ExUnit.Case, async: true

  test "P4.Docx.Transformer.RemoveMiniImages removes dividers and chevrons" do
    %P4.Docx.Document{
      styles: TestHelper.load_xml("test/__fixtures__/styles/mini-images.xml"),
      document: TestHelper.load_xml("test/__fixtures__/document/mini-images.xml")
    }
    |> P4.Docx.PreProcessor.process()
    |> get_document()
    |> TestHelper.test_xml_snapshot("test/__snapshots__/P4.Docx.PreProcessor/process/mini-images")
  end

  defp get_document({:ok, %P4.Docx.Document{document: document}}), do: document

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style attribute contains gibberish text" do
    input_document = make_nocrash_input("asdasdasfasda")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style attribute width property contains gibberish text" do
    input_document = make_nocrash_input("width:asdfasdf")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style attribute contains only semi-colons" do
    input_document = make_nocrash_input(";;;;;")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style attribute contains empty CSS lines" do
    input_document = make_nocrash_input("width:100cm;;;height:100cm;;")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style property value contains semi-colon" do
    input_document = make_nocrash_input("width:100cm;list-type-format:';';height:100cm")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  test "P4.Docx.Transformer.RemoveMiniImages does not crash when style property value contains colon" do
    input_document = make_nocrash_input("width:100cm;list-type-format:':';height:100cm")
    {:ok, output_document} = P4.Docx.PreProcessor.process(input_document)
    assert input_document == output_document
  end

  defp make_nocrash_input(style) when is_binary(style) do
    %P4.Docx.Document{
      styles: {"w:styles", [], []},
      document:
        {"w:document", [],
         [
           {"w:pict", [],
            [
              {"v:shape", [{"style", style}], []}
            ]}
         ]}
    }
  end
end
