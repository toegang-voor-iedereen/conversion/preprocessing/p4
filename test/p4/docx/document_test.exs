defmodule P4.Docx.DocumentTest do
  use ExUnit.Case, async: true
  use Mimic

  alias P4.Docx.Annotator.State

  # describe "get_context/1" do
  #   document = %P4.Docx.Document{
  #     id: "test",
  #     styles: []# TODO.
  #   }

  #   assert P4.Docx.Document.get_context(document) === %P4.Docx.DocumentContext{
  #     id: "test",
  #     specific_style_ids: [], # TODO.
  #     style_map: [], # TODO.
  #   }
  # end

  describe "get_element_uuid/1" do
    test "gets the UUID of a paragraph element" do
      ctx = %P4.Docx.DocumentContext{}
      element = {"w:p", [], [{"w:r", [], [{"w:t", [], ["Draai een keer in het rond"]}]}]}

      assert P4.Docx.Document.get_element_uuid(element) === nil

      expect(Ecto.UUID, :generate, 1, fn -> "fake-uuid" end)
      {annotated, _} = P4.Docx.Annotator.annotate_element({element, %State{}}, ctx)

      assert P4.Docx.Document.get_element_uuid(annotated) === "fake-uuid"
    end

    test "gets the UUID of an image element" do
      ctx = %P4.Docx.DocumentContext{}

      element =
        {"w:drawing", [],
         [
           {"wp:inline", [],
            [
              {"wp:extent", [{"cx", "5731510"}, {"cy", "3278505"}], []},
              {"wp:effectExtent", [{"l", "0"}, {"t", "0"}, {"r", "0"}, {"b", "0"}], []},
              {"wp:docPr",
               [
                 {"id", "936319329"},
                 {"name", "Picture 1"}
               ], []},
              {"a:graphic", [],
               [
                 {"a:graphicData", [],
                  [
                    {"pic:pic", [],
                     [
                       {"pic:nvPicPr", [],
                        [
                          {"pic:cNvPr",
                           [
                             {"id", "936319329"},
                             {"name", "Picture 1"},
                             {"descr",
                              "A colorful cat with bubbles\n\nDescription automatically generated"}
                           ], []}
                        ]}
                     ]}
                  ]}
               ]}
            ]}
         ]}

      assert P4.Docx.Document.get_element_uuid(element) === nil

      expect(Ecto.UUID, :generate, 1, fn -> "fake-uuid" end)
      {annotated, _} = P4.Docx.Annotator.annotate_element({element, %State{}}, ctx)

      assert P4.Docx.Document.get_element_uuid(annotated) === "fake-uuid"
    end
  end
end
