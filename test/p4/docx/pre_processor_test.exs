defmodule P4.Docx.PreProcessorTest do
  use ExUnit.Case, async: true
  use Mimic

  setup do
    stub(P4.Logger, :info, fn _, _ -> :ok end)
    :ok
  end

  test "P4.Docx.PreProcessor.process on cats-in-space.xml" do
    %P4.Docx.Document{
      styles: TestHelper.load_xml("test/__fixtures__/styles/cats-in-space.xml"),
      document: TestHelper.load_xml("test/__fixtures__/document/cats-in-space.xml")
    }
    |> P4.Docx.PreProcessor.process()
    |> get_document()
    |> TestHelper.test_xml_snapshot(
      "test/__snapshots__/P4.Docx.PreProcessor/process/cats-in-space"
    )
  end

  test "P4.Docx.PreProcessor.process on MsWord.xml" do
    %P4.Docx.Document{
      styles: TestHelper.load_xml("test/__fixtures__/styles/MsWord.xml"),
      document: TestHelper.load_xml("test/__fixtures__/document/MsWord.xml")
    }
    |> P4.Docx.PreProcessor.process()
    |> get_document()
    |> TestHelper.test_xml_snapshot("test/__snapshots__/P4.Docx.PreProcessor/process/MsWord")
  end

  test "P4.Docx.PreProcessor.process on heading-normal-text-mixed-up-styling.xml" do
    %P4.Docx.Document{
      styles:
        TestHelper.load_xml("test/__fixtures__/styles/heading-normal-text-mixed-up-styling.xml"),
      document:
        TestHelper.load_xml("test/__fixtures__/document/heading-normal-text-mixed-up-styling.xml")
    }
    |> P4.Docx.PreProcessor.process()
    |> get_document()
    |> TestHelper.test_xml_snapshot(
      "test/__snapshots__/P4.Docx.PreProcessor/process/heading-normal-text-mixed-up-styling"
    )
  end

  test "P4.Docx.PreProcessor.process on mini-images.xml" do
    %P4.Docx.Document{
      styles: TestHelper.load_xml("test/__fixtures__/styles/mini-images.xml"),
      document: TestHelper.load_xml("test/__fixtures__/document/mini-images.xml")
    }
    |> P4.Docx.PreProcessor.process()
    |> get_document()
    |> TestHelper.test_xml_snapshot("test/__snapshots__/P4.Docx.PreProcessor/process/mini-images")
  end

  defp get_document({:ok, %P4.Docx.Document{document: document}}), do: document

  test "P4.Docx.PreProcessor.process on headings.docx" do
    assert {:ok, docx_document} =
             P4.Docx.Reader.read_from_path("test/__fixtures__/docx/headings.docx", "")

    assert {:ok, result} = P4.Docx.PreProcessor.process(docx_document)

    result.document
    |> TestHelper.test_xml_snapshot(
      "test/__snapshots__/P4.Docx.PreProcessor/process/headings/document"
    )
  end
end
