defmodule P4.Amqp.PublisherTest do
  use ExUnit.Case, async: true
  use Mimic

  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Serialization.Serializer
  alias P4.Amqp.MessageRouter

  test "publishing a message but serialization fails" do
    message = %ConversionMessage{identifier: "xyz"}

    exception = %P4.Error.Amqp.InvalidPayload{}

    expect(Serializer, :serialize, 1, fn ^message -> {:error, exception} end)

    reject(&AMQP.Basic.publish/4)

    assert {:error, exception} === P4.Amqp.Publisher.publish(message, a_fake_channel())
  end

  test "publishing a message but basic publishing fails" do
    message = %ConversionMessage{identifier: "xyz"}
    channel = a_fake_channel()

    expect(Serializer, :serialize, 1, fn ^message -> {:ok, "<serialized>"} end)
    expect(MessageRouter, :route, 1, fn ^message -> {:outbound, "<routing key>"} end)

    expect(
      AMQP.Basic,
      :publish,
      1,
      fn ^channel, "exchange.conversion.outbound", "<routing key>", "<serialized>" ->
        {:error, :reason}
      end
    )

    assert {:error,
            %P4.Error.Amqp.Publish{
              message: "Failure publishing: :reason",
              context: %{
                payload: "<serialized>",
                exchange_name: "exchange.conversion.outbound",
                routing_key: "<routing key>"
              },
              reason: :reason
            }} === P4.Amqp.Publisher.publish(message, channel)
  end

  test "publishing a message with success" do
    message = %ConversionMessage{identifier: "xyz"}
    channel = a_fake_channel()

    expect(Serializer, :serialize, 1, fn ^message -> {:ok, "<serialized>"} end)
    expect(MessageRouter, :route, 1, fn ^message -> {:conversion, "<routing key>"} end)

    expect(
      AMQP.Basic,
      :publish,
      1,
      fn ^channel, "exchange.conversion", "<routing key>", "<serialized>" ->
        :ok
      end
    )

    assert {:ok, :done} === P4.Amqp.Publisher.publish(message, channel)
  end

  defp a_fake_channel() do
    %AMQP.Channel{pid: 1234}
  end
end
