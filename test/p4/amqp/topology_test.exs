defmodule P4.Amqp.TopologyTest do
  use ExUnit.Case, async: true
  use Mimic

  @error {:error, "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}

  test "setting up topology structure without errors" do
    channel = %AMQP.Channel{pid: 1234}

    expect(AMQP.Queue, :declare, 2, fn ^channel, queue, _options ->
      {:ok, %{queue: queue, consumer_count: 0, message_count: 0}}
    end)

    expect(AMQP.Exchange, :declare, 2, fn ^channel, _exchange, _type, _options -> :ok end)
    expect(AMQP.Queue, :bind, 2, fn ^channel, _queue, _exchange, _options -> :ok end)

    assert P4.Amqp.Topology.construct(topology(), channel) === {:ok, :done}
  end

  test "setting up topology structure with error when defining bindings" do
    channel = %AMQP.Channel{pid: 1234}

    expect(AMQP.Queue, :declare, 2, fn ^channel, _queue, _options -> :ok end)
    expect(AMQP.Exchange, :declare, 2, fn ^channel, _exchange, _type, _options -> :ok end)

    expect(AMQP.Queue, :bind, 1, fn ^channel, _queue, _exchange, _options ->
      {:error, "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}
    end)

    assert P4.Amqp.Topology.construct(topology(), channel) ===
             {:error, "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}
  end

  test "setting up topology structure with error when defining direct exchange" do
    channel = %AMQP.Channel{pid: 1234}

    expect(AMQP.Exchange, :declare, 1, fn ^channel, _exchange, _type, _options -> @error end)

    reject(&AMQP.Queue.declare/3)
    reject(&AMQP.Queue.bind/4)

    assert P4.Amqp.Topology.construct(topology(), channel) ===
             {:error, "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}
  end

  test "setting up topology structure with error when defining queue" do
    channel = %AMQP.Channel{pid: 1234}

    expect(AMQP.Queue, :declare, 1, fn ^channel, _queue, _options -> @error end)
    expect(AMQP.Exchange, :declare, 2, fn ^channel, _exchange, _type, _options -> :ok end)
    reject(&AMQP.Queue.bind/4)

    assert P4.Amqp.Topology.construct(topology(), channel) ===
             {:error, "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}
  end

  defp topology do
    %P4.Amqp.Topology{
      exchanges: [
        %P4.Amqp.Topology.Exchange{name: "x.dlx", durable: true, type: :fanout},
        %P4.Amqp.Topology.Exchange{name: "x.conversion", durable: true, type: :direct}
      ],
      queues: [
        %P4.Amqp.Topology.Queue{name: "q.dlx", durable: true},
        %P4.Amqp.Topology.Queue{
          name: "q.p4",
          durable: true,
          arguments: [
            {"x-dead-letter-exchange", :longstr, "x.dlx"}
          ]
        }
      ],
      bindings: [
        %P4.Amqp.Topology.Binding{queue: "q.dlx", exchange: "x.dlx"},
        %P4.Amqp.Topology.Binding{
          queue: "q.p4",
          exchange: "x.conversion",
          routing_key: "routing key"
        }
      ]
    }
  end
end
