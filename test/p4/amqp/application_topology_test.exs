defmodule P4.Amqp.ApplicationTopologyTest do
  use ExUnit.Case, async: true
  use Mimic

  test "setting up topology structure based on settings" do
    channel = %AMQP.Channel{pid: 1234}

    settings = %P4.Settings.Amqp{
      exchange: %P4.Settings.Amqp.Exchange{
        outbound: "x.outbound",
        conversion: "x.conversion",
        dlx: "x.dlx"
      },
      queue: %P4.Settings.Amqp.Queue{
        dlx: "q.dlx",
        p4_docx: "q.p4_docx",
        p4_docx_annotator: "q.p4_docx_annotator",
        p4_html_postprocessing: "q.p4_html_postprocessing",
        outbound: "q.outbound"
      }
    }

    expected_topology = %P4.Amqp.Topology{
      exchanges: [
        %P4.Amqp.Topology.Exchange{name: "x.dlx", durable: true, type: :fanout},
        %P4.Amqp.Topology.Exchange{
          name: "x.outbound",
          durable: true,
          type: :fanout
        },
        %P4.Amqp.Topology.Exchange{
          name: "x.conversion",
          durable: true,
          type: :direct,
          arguments: [{"alternate-exchange", :longstr, "exchange.unrouted"}]
        }
      ],
      queues: [
        %P4.Amqp.Topology.Queue{name: "q.dlx", durable: true},
        %P4.Amqp.Topology.Queue{
          name: "q.p4_docx",
          durable: true,
          arguments: [{"x-dead-letter-exchange", :longstr, "x.dlx"}]
        },
        %P4.Amqp.Topology.Queue{
          name: "q.p4_docx_annotator",
          durable: true,
          arguments: [{"x-dead-letter-exchange", :longstr, "x.dlx"}]
        },
        %P4.Amqp.Topology.Queue{
          name: "q.p4_html_postprocessing",
          durable: true,
          arguments: [{"x-dead-letter-exchange", :longstr, "x.dlx"}]
        },
        %P4.Amqp.Topology.Queue{
          name: "q.outbound",
          durable: true,
          arguments: [{"x-dead-letter-exchange", :longstr, "x.dlx"}]
        }
      ],
      bindings: [
        %P4.Amqp.Topology.Binding{
          queue: "q.dlx",
          exchange: "x.dlx"
        },
        %P4.Amqp.Topology.Binding{
          queue: "q.outbound",
          exchange: "x.outbound"
        },
        %P4.Amqp.Topology.Binding{
          queue: "q.p4_docx",
          exchange: "x.conversion",
          routing_key:
            "ZnJvbTphcHBsaWNhdGlvbi92bmQtbmxkb2MuYW5ub3RhdGVkK29wZW54bWxmb3JtYXRzLW9mZmljZWRvY3VtZW50LndvcmRwcm9jZXNzaW5nbWwuZG9jdW1lbnQ7dG86YXBwbGljYXRpb24vdm5kLW5sZG9jLnByZXByb2Nlc3NlZCtvcGVueG1sZm9ybWF0cy1vZmZpY2Vkb2N1bWVudC53b3JkcHJvY2Vzc2luZ21sLmRvY3VtZW50"
        },
        %P4.Amqp.Topology.Binding{
          queue: "q.p4_docx_annotator",
          exchange: "x.conversion",
          routing_key:
            "ZnJvbTphcHBsaWNhdGlvbi92bmQub3BlbnhtbGZvcm1hdHMtb2ZmaWNlZG9j" <>
              "dW1lbnQud29yZHByb2Nlc3NpbmdtbC5kb2N1bWVudDt0bzphcHBsaWNhdGlv" <>
              "bi92bmQtbmxkb2MuYW5ub3RhdGVkK29wZW54bWxmb3JtYXRzLW9mZmlj" <>
              "ZWRvY3VtZW50LndvcmRwcm9jZXNzaW5nbWwuZG9jdW1lbnQ="
        },
        %P4.Amqp.Topology.Binding{
          queue: "q.p4_html_postprocessing",
          exchange: "x.conversion",
          routing_key: "ZnJvbTp0ZXh0L2h0bWw7dG86dGV4dC92bmQtbmxkb2MucG9zdHByb2Nlc3NlZCtodG1s" #gitleaks:allow
        }
      ]
    }

    expect(P4.Amqp.Topology, :construct, 1, fn actual_topology, actual_channel ->
      assert actual_topology === expected_topology
      assert actual_channel === channel
      {:ok, :done}
    end)

    P4.Amqp.ApplicationTopology.construct(channel, settings)
  end
end
