defmodule P4.ConfigTest do
  use ExUnit.Case
  require Logger
  require Config

  setup_all do
    # Since we cannot use logger config files in tests, we need to manually load the config and set the formatter at runtime for this test.
    logging_config = Config.Reader.read!("config/config_logger.exs")
    formatter = logging_config[:logger][:default_handler][:formatter]
    :logger.update_handler_config(:default, :formatter, formatter)
  end

  test "app version is correctly set in config" do
    version = Application.get_env(:p4, :version)
    assert version == Mix.Project.config()[:version]
  end

  test "logging is configured so we log in ECS format" do
    log_message =
      capture_log(:info, fn ->
        Logger.info("test")
      end)

    assert {:ok, log} = Jason.decode(log_message)
    assert {:ok, _, _} = DateTime.from_iso8601(log["@timestamp"])
    assert log["ecs.version"] == "8.11.0"
    assert log["log.level"] == "info"
    assert log["message"] == "test"
  end

  defp capture_log(level, fun) do
    Logger.configure(level: level)

    ExUnit.CaptureIO.capture_io(:user, fn ->
      fun.()
      Logger.flush()
    end)
  after
    Logger.configure(level: :debug)
  end
end
