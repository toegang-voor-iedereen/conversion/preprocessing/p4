defmodule WordPreprocessingConverterTest do
  use ExUnit.Case, async: true
  use Mimic

  alias P4.Amqp.FileConverter.WordPreprocessingConverter
  alias P4.Amqp.Dto.MessageWithFile

  setup do
    stub(P4.Logger, :debug, fn _ -> :ok end)
    stub(P4.Logger, :debug, fn _, _ -> :ok end)
    :ok
  end

  test "converts from annotated Word files to preprocessed Word files" do
    assert WordPreprocessingConverter.from() ===
             "application/vnd-nldoc.annotated+openxmlformats-officedocument.wordprocessingml.document"

    assert WordPreprocessingConverter.to() ===
             "application/vnd-nldoc.preprocessed+openxmlformats-officedocument.wordprocessingml.document"

    assert WordPreprocessingConverter.file_infix() === "preprocessed"
    assert WordPreprocessingConverter.file_extension() === "docx"
  end

  test "convert fails when reading input file fails" do
    msg = %MessageWithFile{local_file_path: "/some/path.docx"}
    {:error, reason} = mock_docx_reading(:error, msg.local_file_path)

    assert {:error, reason} === WordPreprocessingConverter.convert(msg)
  end

  test "convert fails when processing fails" do
    msg = %MessageWithFile{local_file_path: "/some/path.docx"}
    {:ok, document} = mock_docx_reading(:ok, msg.local_file_path)
    {:error, reason} = mock_docx_processing(:error, document)

    assert {:error, reason} === WordPreprocessingConverter.convert(msg)
  end

  test "convert fails when writing output file fails" do
    msg = %MessageWithFile{local_file_path: "/some/path.docx"}
    {:ok, document} = mock_docx_reading(:ok, msg.local_file_path)
    {:ok, document} = mock_docx_processing(:ok, document)
    {:error, reason} = mock_docx_writing(:error, document)

    assert {:error, reason} === WordPreprocessingConverter.convert(msg)
  end

  test "convert fails when removing input file fails" do
    msg = %MessageWithFile{local_file_path: "/some/path.docx"}
    {:ok, document} = mock_docx_reading(:ok, msg.local_file_path)
    {:ok, document} = mock_docx_processing(:ok, document)
    {:ok, _} = mock_docx_writing(:ok, document)
    {:error, reason} = mock_file_rm(:error, msg.local_file_path, 2)

    expected_error = %P4.Error.IO.RemoveFile{
      message: "Error removing file: :reason",
      reason: reason,
      context: %{path: msg.local_file_path}
    }

    assert {:error, expected_error} === WordPreprocessingConverter.convert(msg)
  end

  test "converts successfully" do
    msg = %MessageWithFile{local_file_path: "/some/path.docx"}
    {:ok, document} = mock_docx_reading(:ok, msg.local_file_path)
    {:ok, document} = mock_docx_processing(:ok, document)
    {:ok, output_path} = mock_docx_writing(:ok, document)
    :ok = mock_file_rm(:ok, msg.local_file_path)

    assert {:ok, %MessageWithFile{message: msg.message, local_file_path: output_path}} ===
             WordPreprocessingConverter.convert(msg)
  end

  defp mock_docx_reading(:error, path) do
    expect(P4.Docx.Reader, :read_from_path, fn ^path, _ -> {:error, :reason} end)

    {:error, :reason}
  end

  defp mock_docx_reading(:ok, path) do
    extraction_path = "/some/extraction/path"

    result =
      {:ok,
       %P4.Docx.Document{
         file_list: [
           Path.join([extraction_path, "word/document.xml"]),
           Path.join([extraction_path, "word/styles.xml"]),
           Path.join([extraction_path, "word/theme/theme1.xml"])
         ],
         extraction_path: extraction_path,
         styles: [{"w:styles", [], []}],
         document: [{"w:document", [], []}]
       }}

    expect(P4.Docx.Reader, :read_from_path, fn ^path, _ -> result end)

    result
  end

  defp mock_docx_processing(:ok, document = %P4.Docx.Document{}) do
    expect(P4.Docx.PreProcessor, :process, fn ^document -> {:ok, document} end)

    {:ok, document}
  end

  defp mock_docx_processing(:error, document = %P4.Docx.Document{}) do
    expect(P4.Docx.PreProcessor, :process, fn ^document -> {:error, :reason} end)

    {:error, :reason}
  end

  defp mock_docx_writing(:ok, document) do
    expect(P4.Docx.Writer, :write, fn ^document -> {:ok, "/processed/path.docx"} end)

    {:ok, "/processed/path.docx"}
  end

  defp mock_docx_writing(:error, document) do
    expect(P4.Docx.Writer, :write, fn ^document -> {:error, :reason} end)

    {:error, :reason}
  end

  defp mock_file_rm(outcome, path, num_calls \\ 1)

  defp mock_file_rm(:ok, path, num_calls) do
    expect(File, :rm, num_calls, fn ^path -> :ok end)
    :ok
  end

  defp mock_file_rm(:error, path, num_calls) do
    expect(File, :rm, num_calls, fn ^path -> {:error, :reason} end)
    {:error, :reason}
  end
end
