defmodule P4.Amqp.Storage.S3Test do
  use ExUnit.Case, async: true
  use Mimic

  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Dto.MessageWithFile

  setup do
    stub(P4.Logger, :debug, fn _ -> :ok end)
    stub(P4.Logger, :error, fn _, _ -> :ok end)
    :ok
  end

  describe "download" do
    test "downloads file from S3 bucket configured in env" do
      key = "some/key"
      bucket = "some-bucket"
      temp_path = "/tmp/some/path"
      message = %ConversionMessage{data: %{content_location: key}}

      expect(Application, :get_env, 1, fn :p4, :s3_bucket -> bucket end)
      expect(P4.IO.File, :create_temp, 1, fn -> {:ok, temp_path} end)

      expect(ExAws.S3, :download_file, 1, fn actual_bucket, actual_key, actual_path ->
        assert actual_bucket === bucket
        assert actual_key === key
        assert actual_path === temp_path
        :ok
      end)

      expect(ExAws, :request, 1, fn _ -> {:ok, nil} end)

      expected_message = %MessageWithFile{message: message, local_file_path: temp_path}
      assert {:ok, expected_message} === P4.Amqp.Storage.S3.download(message)
    end

    test "returns error if creating temp path fails" do
      key = "some/key"
      message = %ConversionMessage{data: %{content_location: key}}

      expect(P4.IO.File, :create_temp, 1, fn -> {:error, :some_error} end)

      {:error, error} = P4.Amqp.Storage.S3.download(message)

      assert error === %P4.Error.S3.Download{
               message: "Failure downloading file from S3: :some_error",
               reason: :some_error,
               context: %{key: "some/key", bucket: Application.get_env(:p4, :s3_bucket)}
             }
    end

    test "returns error if download fails" do
      key = "some/key"
      bucket = "some-bucket"
      temp_path = "/tmp/some/path"
      message = %ConversionMessage{data: %{content_location: key}}

      expect(Application, :get_env, 1, fn :p4, :s3_bucket -> bucket end)
      expect(P4.IO.File, :create_temp, 1, fn -> {:ok, temp_path} end)

      expect(ExAws.S3, :download_file, 1, fn actual_bucket, actual_key, actual_path ->
        assert actual_bucket === bucket
        assert actual_key === key
        assert actual_path === temp_path
        :ok
      end)

      expect(ExAws, :request, 1, fn _ -> {:error, :some_error} end)

      expected_error = %P4.Error.S3.Download{
        message: "Failure downloading file from S3: :some_error",
        reason: :some_error,
        context: %{bucket: bucket, key: key}
      }

      assert {:error, expected_error} === P4.Amqp.Storage.S3.download(message)
    end
  end

  describe "upload" do
    test "uploads file to S3 bucket configured in env" do
      id = "some-identifier"
      key = "some/key"
      bucket = "some-bucket"
      local_path = "/tmp/some/path"
      message = %ConversionMessage{identifier: id, data: %{content_location: key}}
      message_with_file = %MessageWithFile{message: message, local_file_path: local_path}

      expect(Application, :get_env, 1, fn :p4, :s3_bucket -> bucket end)

      file_infix = "test"
      file_extension = "ext"
      output_key = "/some-identifier/p4.#{file_infix}.some-key.#{file_extension}"

      expect(P4.Misc.KeyGenerator, :generate, 1, fn length, opts ->
        assert length === 24

        assert opts === [
                 prefix: "/some-identifier/p4.#{file_infix}.",
                 suffix: ".#{file_extension}"
               ]

        output_key
      end)

      expect(ExAws.S3.Upload, :stream_file, 1, fn actual_path ->
        assert actual_path === local_path
        :ok
      end)

      expect(ExAws.S3, :upload, 1, fn :ok, actual_bucket, actual_key ->
        assert actual_bucket === bucket
        assert actual_key === output_key
        {:ok, :term}
      end)

      expect(ExAws, :request, 1, fn _ -> {:ok, nil} end)

      expected_output = %ConversionMessage{
        message
        | data: %{message.data | content_location: output_key}
      }

      assert {:ok, expected_output} ===
               P4.Amqp.Storage.S3.upload(message_with_file, file_infix, file_extension)
    end

    test "returns error if upload fails" do
      id = "some-identifier"
      key = "some/key"
      bucket = "some-bucket"
      local_path = "/tmp/some/path"
      message = %ConversionMessage{identifier: id, data: %{content_location: key}}
      message_with_file = %MessageWithFile{message: message, local_file_path: local_path}

      expect(Application, :get_env, 1, fn :p4, :s3_bucket -> bucket end)

      file_infix = "test"
      file_extension = "ext"
      output_key = "/some-identifier/p4.#{file_infix}some-key.#{file_extension}"

      expect(P4.Misc.KeyGenerator, :generate, 1, fn length, opts ->
        assert length === 24

        assert opts === [
                 prefix: "/some-identifier/p4.#{file_infix}.",
                 suffix: ".#{file_extension}"
               ]

        output_key
      end)

      expect(ExAws.S3.Upload, :stream_file, 1, fn actual_path ->
        assert actual_path === local_path
        :ok
      end)

      expect(ExAws.S3, :upload, 1, fn :ok, actual_bucket, actual_key ->
        assert actual_bucket === bucket
        assert actual_key === output_key
        {:ok, :term}
      end)

      expect(ExAws, :request, 1, fn _ -> {:error, :some_reason} end)

      expected_error = %P4.Error.S3.Upload{
        message: "Failure uploading file to S3: :some_reason",
        reason: :some_reason,
        context: %{bucket: bucket, key: output_key, local_path: local_path}
      }

      assert {:error, expected_error} ===
               P4.Amqp.Storage.S3.upload(message_with_file, file_infix, file_extension)
    end
  end
end
