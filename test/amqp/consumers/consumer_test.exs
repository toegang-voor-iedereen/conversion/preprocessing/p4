defmodule MockedConverter do
  @behaviour P4.Amqp.FileConverter

  @impl P4.Amqp.FileConverter
  def from() do
    "test-doc-from"
  end

  @impl P4.Amqp.FileConverter
  def to() do
    "test-doc-to"
  end

  @impl P4.Amqp.FileConverter
  def file_infix() do
    "test"
  end

  @impl P4.Amqp.FileConverter
  def file_extension() do
    "ext"
  end

  @impl P4.Amqp.FileConverter
  def convert(_message) do
    {:ok, :done}
  end
end

defmodule MockedConsumer do
  use P4.Amqp.Consumer, converter: MockedConverter

  @impl P4.Amqp.Consumer.Behaviour
  def queue_name() do
    "test"
  end

  @impl P4.Amqp.Consumer.Behaviour
  def consume(message) do
    {:ok, message}
  end
end

defmodule MockedFailingConsumer do
  use P4.Amqp.Consumer, converter: MockedConverter

  @impl P4.Amqp.Consumer.Behaviour
  def queue_name() do
    "test"
  end

  @impl P4.Amqp.Consumer.Behaviour
  def consume(_message) do
    {:error, :reason}
  end
end

# -------------------------------------------------------------------------------------------------

defmodule ConsumerTest do
  use ExUnit.Case, async: true
  use Mimic

  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Dto.ConversionMessageData
  alias P4.Amqp.Dto.ConversionTypeMapping
  alias P4.Amqp.Dto.MessageWithFile

  @spec a_fake_channel() :: %AMQP.Channel{}
  defp a_fake_channel() do
    %AMQP.Channel{pid: self()}
  end

  @spec a_fake_conversion_message() :: %ConversionMessage{}
  defp a_fake_conversion_message() do
    mapping = %ConversionTypeMapping{
      from: "test-doc-from",
      to: "test-doc-to"
    }

    %ConversionMessage{
      identifier: "abc",
      conversion_stack: [mapping],
      data: %ConversionMessageData{
        content_location: "abc.docx",
        assets: []
      },
      mapping: mapping
    }
  end

  setup do
    stub(P4.Logger, :debug, fn _ -> :ok end)
    stub(P4.Logger, :notice, fn _ -> :ok end)
    stub(P4.Logger, :error, fn _ -> :ok end)
    stub(P4.Logger, :error, fn _, _ -> :ok end)
    :ok
  end

  describe "init/0" do
    test "successfully initialises" do
      channel = a_fake_channel()

      expected_settings = %P4.Settings.Amqp{
        exchange: %P4.Settings.Amqp.Exchange{
          outbound: "exchange.conversion.outbound",
          conversion: "exchange.conversion",
          dlx: "exchange.graveyard"
        },
        queue: %P4.Settings.Amqp.Queue{
          p4_docx: "queue.conversion.p4_docx",
          p4_docx_annotator: "queue.conversion.p4_docx_annotator",
          p4_html_postprocessing: "queue.conversion.p4_html_postprocessing",
          dlx: "queue.conversion.graveyard",
          outbound: "queue.conversion.outbound"
        }
      }

      expect(AMQP.Application, :get_channel, 1, fn -> {:ok, channel} end)

      expect(P4.Amqp.ApplicationTopology, :construct, 1, fn actual_channel, actual_settings ->
        assert actual_channel === channel
        assert actual_settings === expected_settings
        {:ok, :done}
      end)

      assert {:ok, channel} === MockedConsumer.init(nil)

      assert_received :subscribe
    end
  end

  describe "subscribe/0" do
    test "actually subscribes to the intended queue" do
      channel = a_fake_channel()

      expect(AMQP.Application, :get_channel, 1, fn -> {:ok, channel} end)
      expect(AMQP.Basic, :qos, 1, fn ^channel, [prefetch_count: 1] -> :ok end)

      expect(AMQP.Basic, :consume, fn ^channel, "test" ->
        {:ok, "consumer-tag"}
      end)

      assert {:ok, ^channel} = MockedConsumer.subscribe()
    end

    test "retries when retrieving the channel fails" do
      expect(AMQP.Application, :get_channel, 1, fn -> {:error, :some_error} end)
      reject(&AMQP.Basic.qos/2)
      reject(&AMQP.Basic.consume/2)

      assert {:error, :retrying} = MockedConsumer.subscribe()
      assert_receive :subscribe, 1250
    end

    test "retries when setting basic qos fails" do
      channel = a_fake_channel()
      expect(AMQP.Application, :get_channel, 1, fn -> {:ok, channel} end)
      expect(AMQP.Basic, :qos, 1, fn ^channel, [prefetch_count: 1] -> {:error, :reason} end)
      reject(&AMQP.Basic.consume/2)

      assert {:error, :retrying} = MockedConsumer.subscribe()
      assert_receive :subscribe, 1250
    end

    test "retries when subscribing to the queue fails" do
      channel = a_fake_channel()
      expect(AMQP.Application, :get_channel, 1, fn -> {:ok, channel} end)
      expect(AMQP.Basic, :qos, 1, fn ^channel, [prefetch_count: 1] -> :ok end)
      expect(AMQP.Basic, :consume, fn ^channel, "test" -> {:error, :reason} end)

      assert {:error, :retrying} = MockedConsumer.subscribe()
      assert_receive :subscribe, 1250
    end
  end

  describe "consume/3" do
    test "correctly deserializes, downloads, consumes, uploads and acks the message" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      message_with_file = %MessageWithFile{message: message, local_file_path: "/tmp/abc.docx"}
      payload = "test payload"
      delivery_tag = "test tag"

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Storage.S3, :download, 1, fn actual_message ->
        assert actual_message == message
        {:ok, message_with_file}
      end)

      expect(P4.Amqp.Storage.S3, :upload, 1, fn actual_message_with_file,
                                                actual_infix,
                                                actual_extension ->
        assert actual_message_with_file == message_with_file
        assert actual_infix == MockedConverter.file_infix()
        assert actual_extension == MockedConverter.file_extension()
        {:ok, message}
      end)

      expect(P4.Amqp.Publisher, :publish, 1, fn actual_message, actual_channel ->
        assert actual_message == %{message | conversion_stack: []}
        assert actual_channel == channel
        {:ok, :done}
      end)

      expect(AMQP.Basic, :ack, 1, fn actual_channel, actual_delivery_tag ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        {:ok, :done}
      end)

      expect(P4.Logger, :debug, fn _ -> :ok end)

      reject(&Sentry.capture_exception/2)
      reject(&AMQP.Basic.nack/3)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, %{delivery_tag: delivery_tag})
    end

    test "nacks when deserialize fails" do
      channel = a_fake_channel()
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end

    test "nacks when conversion message validation fails" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Dto.ConversionMessage, :validate, 1, fn actual_message,
                                                             actual_from,
                                                             actual_to ->
        assert actual_message == message
        assert actual_from == MockedConverter.from()
        assert actual_to == MockedConverter.to()
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end

    test "nacks when download fails" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Storage.S3, :download, 1, fn actual_message ->
        assert actual_message == message
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end

    test "nacks when consume/1 fails" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      message_with_file = %MessageWithFile{message: message, local_file_path: "/tmp/abc.docx"}
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Storage.S3, :download, 1, fn actual_message ->
        assert actual_message == message
        {:ok, message_with_file}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      assert {:ok, :done} =
               MockedFailingConsumer.consume(channel, payload, meta)
    end

    test "nacks when upload fails" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      message_with_file = %MessageWithFile{message: message, local_file_path: "/tmp/abc.docx"}
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Storage.S3, :download, 1, fn actual_message ->
        assert actual_message == message
        {:ok, message_with_file}
      end)

      expect(P4.Amqp.Storage.S3, :upload, 1, fn actual_message_with_file,
                                                actual_infix,
                                                actual_extension ->
        assert actual_message_with_file == message_with_file
        assert actual_infix == MockedConverter.file_infix()
        assert actual_extension == MockedConverter.file_extension()
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end

    test "nacks when ack fails" do
      channel = a_fake_channel()
      message = a_fake_conversion_message()
      message_with_file = %MessageWithFile{message: message, local_file_path: "/tmp/abc.docx"}
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:ok, message}
      end)

      expect(P4.Amqp.Storage.S3, :download, 1, fn actual_message ->
        assert actual_message == message
        {:ok, message_with_file}
      end)

      expect(P4.Amqp.Storage.S3, :upload, 1, fn actual_message_with_file,
                                                actual_infix,
                                                actual_extension ->
        assert actual_message_with_file == message_with_file
        assert actual_infix == MockedConverter.file_infix()
        assert actual_extension == MockedConverter.file_extension()
        {:ok, message}
      end)

      expect(P4.Amqp.Publisher, :publish, 1, fn actual_message, actual_channel ->
        assert actual_message == %{message | conversion_stack: []}
        assert actual_channel == channel
        {:ok, :done}
      end)

      expect(AMQP.Basic, :ack, 1, fn actual_channel, actual_delivery_tag ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        expected_exception = %P4.Error.Amqp.Ack{
          message: "Failure acking: :reason",
          reason: :reason,
          context: meta
        }

        assert actual_exception == expected_exception

        assert actual_options == [
                 extra: %{exception: expected_exception, meta: meta, payload: payload}
               ]

        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        :ok
      end)

      expect(P4.Logger, :debug, fn _ -> :ok end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end

    test "Sentry captures nacking exception when nacking fails" do
      channel = a_fake_channel()
      payload = "test payload"
      delivery_tag = "test tag"
      meta = %{delivery_tag: delivery_tag}

      expect(P4.Amqp.Serialization.Serializer, :deserialize, 1, fn actual_payload ->
        assert actual_payload == payload
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        assert actual_exception == :reason
        assert actual_options == [extra: %{exception: :reason, meta: meta, payload: payload}]
        {:ok, :done}
      end)

      expect(AMQP.Basic, :nack, 1, fn actual_channel, actual_delivery_tag, [requeue: false] ->
        assert actual_channel == channel
        assert actual_delivery_tag == delivery_tag
        {:error, :reason}
      end)

      expect(Sentry, :capture_exception, 1, fn actual_exception, actual_options ->
        expected_exception = %P4.Error.Amqp.Nack{
          message: "Failure nacking",
          reason: :reason
        }

        assert actual_exception == expected_exception

        assert actual_options == [
                 extra: %{exception: expected_exception, meta: meta, payload: payload}
               ]

        {:ok, :done}
      end)

      assert {:ok, :done} =
               MockedConsumer.consume(channel, payload, meta)
    end
  end
end
