defmodule P4.Amqp.Dto.ConversionMessageTest do
  use ExUnit.Case, async: true

  describe "changeset/2" do
    test "validate empty" do
      assert %Ecto.Changeset{
               changes: %{},
               errors: [
                 identifier: {"can't be blank", [validation: :required]},
                 data: {"can't be blank", [validation: :required]},
                 mapping: {"can't be blank", [validation: :required]}
               ],
               valid?: false,
               data: %P4.Amqp.Dto.ConversionMessage{},
               required: [:identifier, :data, :mapping],
               params: %{}
             } = P4.Amqp.Dto.ConversionMessage.changeset(%P4.Amqp.Dto.ConversionMessage{}, %{})
    end

    test "validate valid with empty embeds" do
      params = %{"identifier" => "abc123"}

      assert %Ecto.Changeset{
               changes: %{},
               errors: [
                 {:data, {"can't be blank", [validation: :required]}},
                 {:mapping, {"can't be blank", [validation: :required]}}
               ],
               valid?: false,
               data: %P4.Amqp.Dto.ConversionMessage{},
               required: [:identifier, :data, :mapping],
               params: ^params
             } =
               P4.Amqp.Dto.ConversionMessage.changeset(
                 %P4.Amqp.Dto.ConversionMessage{},
                 params
               )
    end

    test "validate valid with filled embeds" do
      params = %{
        "identifier" => "abc123",
        "mapping" => %{
          "from" => "abc",
          "to" => "xyz"
        },
        "data" => %{
          "content_location" => "/some/location",
          "assets" => [
            %{"filename" => "image.jpg", "content_location" => "/data/image.jpg"},
            %{"filename" => "image.png", "content_location" => "/data/image.png"}
          ]
        },
        "conversion_stack" => [
          %{"from" => "abc", "to" => "123"},
          %{"from" => "123", "to" => "xyz"}
        ]
      }

      assert %Ecto.Changeset{
               changes: %{
                 conversion_stack: [
                   %Ecto.Changeset{
                     action: :insert,
                     changes: %{to: "123", from: "abc"},
                     errors: [],
                     data: %P4.Amqp.Dto.ConversionTypeMapping{},
                     valid?: true
                   },
                   %Ecto.Changeset{
                     action: :insert,
                     changes: %{to: "xyz", from: "123"},
                     errors: [],
                     data: %P4.Amqp.Dto.ConversionTypeMapping{},
                     valid?: true
                   }
                 ],
                 data: %Ecto.Changeset{
                   action: :insert,
                   changes: %{
                     content_location: "/some/location",
                     assets: [
                       %Ecto.Changeset{
                         action: :insert,
                         changes: %{
                           filename: "image.jpg",
                           content_location: "/data/image.jpg"
                         },
                         errors: [],
                         data: %P4.Amqp.Dto.Asset{},
                         valid?: true
                       },
                       %Ecto.Changeset{
                         action: :insert,
                         changes: %{
                           filename: "image.png",
                           content_location: "/data/image.png"
                         },
                         errors: [],
                         data: %P4.Amqp.Dto.Asset{},
                         valid?: true
                       }
                     ]
                   },
                   errors: [],
                   data: %P4.Amqp.Dto.ConversionMessageData{},
                   valid?: true
                 },
                 identifier: "abc123",
                 mapping: %Ecto.Changeset{
                   action: :insert,
                   changes: %{to: "xyz", from: "abc"},
                   errors: [],
                   data: %P4.Amqp.Dto.ConversionTypeMapping{},
                   valid?: true
                 }
               },
               errors: [],
               valid?: true,
               data: %P4.Amqp.Dto.ConversionMessage{},
               required: [:identifier, :data, :mapping],
               params: ^params
             } =
               data =
               P4.Amqp.Dto.ConversionMessage.changeset(
                 %P4.Amqp.Dto.ConversionMessage{},
                 params
               )

      assert Ecto.Changeset.apply_changes(data) === %P4.Amqp.Dto.ConversionMessage{
               conversion_stack: [
                 %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "123"},
                 %P4.Amqp.Dto.ConversionTypeMapping{from: "123", to: "xyz"}
               ],
               data: %P4.Amqp.Dto.ConversionMessageData{
                 content_location: "/some/location",
                 assets: [
                   %P4.Amqp.Dto.Asset{filename: "image.jpg", content_location: "/data/image.jpg"},
                   %P4.Amqp.Dto.Asset{filename: "image.png", content_location: "/data/image.png"}
                 ]
               },
               identifier: "abc123",
               mapping: %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "xyz"}
             }
    end
  end

  describe "validate/3" do
    test "returns error for empty conversion stack" do
      message = a_fake_conversion_message([])

      assert {:error, %P4.Error.Amqp.EmptyConversionStack{amqp_message: ^message}} =
               P4.Amqp.Dto.ConversionMessage.validate(message, "", "")
    end

    test "returns error for unsupported conversion stack" do
      message =
        a_fake_conversion_message([
          %P4.Amqp.Dto.ConversionTypeMapping{from: "text/markdown", to: "text/html"}
        ])

      assert {:error, %P4.Error.Amqp.InvalidConversion{amqp_message: ^message}} =
               P4.Amqp.Dto.ConversionMessage.validate(message, "text/markdown", "test/pdf")
    end

    test "returns valid for supported conversion stack" do
      message =
        a_fake_conversion_message([
          %P4.Amqp.Dto.ConversionTypeMapping{from: "text/markdown", to: "text/html"}
        ])

      assert {:ok, ^message} =
               P4.Amqp.Dto.ConversionMessage.validate(message, "text/markdown", "text/html")
    end
  end

  @spec a_fake_conversion_message([P4.Amqp.Dto.ConversionTypeMapping.t()]) ::
          P4.Amqp.Dto.ConversionMessage.t()
  defp a_fake_conversion_message(stack) do
    %P4.Amqp.Dto.ConversionMessage{
      conversion_stack: stack,
      data: %P4.Amqp.Dto.ConversionMessageData{},
      identifier: "abc123",
      mapping: %P4.Amqp.Dto.ConversionTypeMapping{}
    }
  end
end
