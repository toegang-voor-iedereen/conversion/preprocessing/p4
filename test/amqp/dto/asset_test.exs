defmodule P4.Amqp.Dto.AssetTest do
  use ExUnit.Case, async: true

  describe "changeset/2" do
    test "validate empty" do
      assert P4.Amqp.Dto.Asset.changeset(%P4.Amqp.Dto.Asset{}, %{}) === %Ecto.Changeset{
               changes: %{},
               data: %P4.Amqp.Dto.Asset{content_location: nil, filename: nil},
               errors: [
                 filename: {"can't be blank", [validation: :required]},
                 content_location: {"can't be blank", [validation: :required]}
               ],
               params: %{},
               required: [:filename, :content_location],
               types: %{filename: :string, content_location: :string},
               valid?: false,
               validations: []
             }
    end

    test "validate valid" do
      assert P4.Amqp.Dto.Asset.changeset(
               %P4.Amqp.Dto.Asset{},
               %{"content_location" => "/some/location", "filename" => "abc123"}
             ) === %Ecto.Changeset{
               changes: %{content_location: "/some/location", filename: "abc123"},
               data: %P4.Amqp.Dto.Asset{content_location: nil, filename: nil},
               empty_values: [&Ecto.Type.empty_trimmed_string?/1],
               errors: [],
               params: %{"content_location" => "/some/location", "filename" => "abc123"},
               required: [:filename, :content_location],
               types: %{content_location: :string, filename: :string},
               valid?: true,
               validations: []
             }
    end
  end
end
