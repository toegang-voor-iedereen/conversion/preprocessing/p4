defmodule P4.Amqp.Dto.ConversionTypeMappingTest do
  use ExUnit.Case, async: true

  alias P4.Amqp.Dto.ConversionTypeMapping

  describe "changeset/2" do
    test "validate empty" do
      assert ConversionTypeMapping.changeset(%ConversionTypeMapping{}, %{}) ===
               %Ecto.Changeset{
                 changes: %{},
                 errors: [
                   from: {"can't be blank", [validation: :required]},
                   to: {"can't be blank", [validation: :required]}
                 ],
                 valid?: false,
                 data: %ConversionTypeMapping{},
                 required: [:from, :to],
                 types: %{to: :string, from: :string},
                 params: %{}
               }
    end

    test "validate valid" do
      assert ConversionTypeMapping.changeset(
               %ConversionTypeMapping{},
               %{"from" => "text/markdown", "to" => "text/html"}
             ) === %Ecto.Changeset{
               changes: %{to: "text/html", from: "text/markdown"},
               errors: [],
               valid?: true,
               data: %ConversionTypeMapping{},
               required: [:from, :to],
               types: %{to: :string, from: :string},
               params: %{"from" => "text/markdown", "to" => "text/html"}
             }
    end
  end

  describe "validate/3" do
    test "valid" do
      mapping = %ConversionTypeMapping{from: "text/markdown", to: "text/html"}

      assert {:ok, mapping} ===
               ConversionTypeMapping.validate(
                 mapping,
                 "text/markdown",
                 "text/html"
               )
    end

    test "unsupported" do
      assert {:error, :unsupported} ===
               ConversionTypeMapping.validate(
                 %ConversionTypeMapping{from: "text/markdown", to: "text/html"},
                 "text/markdown",
                 "text/pdf"
               )
    end
  end
end
