defmodule P4.Amqp.Dto.ConversionMessageDataTest do
  use ExUnit.Case, async: true

  describe "changeset/2" do
    test "validate empty" do
      assert %Ecto.Changeset{
               changes: %{},
               errors: [
                 content_location: {"can't be blank", [validation: :required]}
               ],
               valid?: false,
               data: %P4.Amqp.Dto.ConversionMessageData{},
               required: [:content_location],
               params: %{}
             } =
               P4.Amqp.Dto.ConversionMessageData.changeset(
                 %P4.Amqp.Dto.ConversionMessageData{},
                 %{}
               )
    end

    test "validate valid with empty assets" do
      params = %{"content_location" => "/some/location", "assets" => []}

      assert %Ecto.Changeset{
               changes: %{content_location: "/some/location"},
               errors: [],
               valid?: true,
               data: %P4.Amqp.Dto.ConversionMessageData{},
               required: [:content_location],
               params: ^params
             } =
               P4.Amqp.Dto.ConversionMessageData.changeset(
                 %P4.Amqp.Dto.ConversionMessageData{},
                 params
               )
    end

    test "validate valid with two assets" do
      params = %{
        "content_location" => "/some/location",
        "assets" => [
          %{"filename" => "image.jpg", "content_location" => "/data/image.jpg"},
          %{"filename" => "image.png", "content_location" => "/data/image.png"}
        ]
      }

      assert %Ecto.Changeset{
               changes: %{
                 content_location: "/some/location",
                 assets: [
                   %Ecto.Changeset{
                     action: :insert,
                     changes: %{
                       filename: "image.jpg",
                       content_location: "/data/image.jpg"
                     },
                     errors: [],
                     data: %P4.Amqp.Dto.Asset{},
                     valid?: true
                   },
                   %Ecto.Changeset{
                     action: :insert,
                     changes: %{
                       filename: "image.png",
                       content_location: "/data/image.png"
                     },
                     errors: [],
                     data: %P4.Amqp.Dto.Asset{},
                     valid?: true
                   }
                 ]
               },
               errors: [],
               valid?: true,
               data: %P4.Amqp.Dto.ConversionMessageData{},
               required: [:content_location],
               params: ^params
             } =
               P4.Amqp.Dto.ConversionMessageData.changeset(
                 %P4.Amqp.Dto.ConversionMessageData{},
                 params
               )
    end
  end
end
