defmodule P4.Amqp.Serialization.SerializerTest do
  use ExUnit.Case, async: true
  use Mimic

  setup do
    stub(P4.Logger, :debug, fn _ -> :ok end)
    :ok
  end

  test "Deserialize data being invalid json" do
    payload = "{\"Hello\": \"World\""

    assert P4.Amqp.Serialization.Serializer.deserialize(payload) ===
             {:error,
              %Jason.DecodeError{
                position: 17,
                token: nil,
                data: payload
              }}
  end

  test "Deserialize data not matching schema" do
    payload = "{\"Hello\": \"World\"}"

    assert {
             :error,
             %P4.Error.Amqp.InvalidPayload{
               message: "Invalid payload delivered",
               changeset: %Ecto.Changeset{
                 action: nil,
                 changes: %{},
                 errors: [
                   identifier: {"can't be blank", [validation: :required]},
                   data: {"can't be blank", [validation: :required]},
                   mapping: {"can't be blank", [validation: :required]}
                 ],
                 params: %{"hello" => "World"},
                 required: [:identifier, :data, :mapping],
                 data: %P4.Amqp.Dto.ConversionMessage{},
                 valid?: false
               },
               payload: ^payload
             }
           } = P4.Amqp.Serialization.Serializer.deserialize(payload)
  end

  test "Deserializing missing data" do
    payload = """
    {
      "identifier": "World",
      "mapping": {
        "from": "abc",
        "to": "xyz"
      }
    }
    """

    assert {
             :error,
             %P4.Error.Amqp.InvalidPayload{
               message: "Invalid payload delivered",
               changeset: %Ecto.Changeset{
                 action: nil,
                 changes: %{
                   identifier: "World",
                   mapping: %Ecto.Changeset{
                     action: :insert,
                     changes: %{to: "xyz", from: "abc"},
                     types: %{to: :string, from: :string},
                     params: %{"from" => "abc", "to" => "xyz"},
                     errors: [],
                     data: %P4.Amqp.Dto.ConversionTypeMapping{},
                     valid?: true
                   }
                 },
                 required: [:identifier, :data, :mapping],
                 params: %{
                   "identifier" => "World",
                   "mapping" => %{"from" => "abc", "to" => "xyz"}
                 },
                 errors: [data: {"can't be blank", [validation: :required]}],
                 data: %P4.Amqp.Dto.ConversionMessage{},
                 valid?: false
               },
               payload: ^payload
             }
           } = P4.Amqp.Serialization.Serializer.deserialize(payload)
  end

  test "deserializing correct data" do
    payload = """
    {
      "identifier": "xyz123",
      "mapping": {
        "from": "abc",
        "to": "xyz"
      },
      "conversionStack": [
        {"from": "abc", "to": "123"},
        {"from": "123", "to": "xyz"}
      ],
      "data": {
        "contentLocation": "/some/location",
        "assets": [
          {"contentLocation": "/image.jpg", "filename": "image.jpg"},
          {"contentLocation": "/image.png", "filename": "image.png"}
        ]
      }
    }
    """

    assert P4.Amqp.Serialization.Serializer.deserialize(payload) ===
             {:ok,
              %P4.Amqp.Dto.ConversionMessage{
                identifier: "xyz123",
                mapping: %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "xyz"},
                data: %P4.Amqp.Dto.ConversionMessageData{
                  content_location: "/some/location",
                  assets: [
                    %P4.Amqp.Dto.Asset{filename: "image.jpg", content_location: "/image.jpg"},
                    %P4.Amqp.Dto.Asset{filename: "image.png", content_location: "/image.png"}
                  ]
                },
                conversion_stack: [
                  %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "123"},
                  %P4.Amqp.Dto.ConversionTypeMapping{from: "123", to: "xyz"}
                ]
              }}
  end

  test "serializing with valid data" do
    data = %P4.Amqp.Dto.ConversionMessage{
      identifier: "xyz123",
      mapping: %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "xyz"},
      data: %P4.Amqp.Dto.ConversionMessageData{
        content_location: "/some/location",
        assets: [
          %P4.Amqp.Dto.Asset{filename: "image.jpg", content_location: "/image.jpg"},
          %P4.Amqp.Dto.Asset{filename: "image.png", content_location: "/image.png"}
        ]
      },
      conversion_stack: [
        %P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "123"},
        %P4.Amqp.Dto.ConversionTypeMapping{from: "123", to: "xyz"}
      ]
    }

    assert P4.Amqp.Serialization.Serializer.serialize(data) ===
             {:ok,
              "{\"conversionStack\":[{\"from\":\"abc\",\"to\":\"123\"},{\"from\":\"123\",\"to\":\"xyz\"}],\"data\":{\"assets\":[{\"contentLocation\":\"/image.jpg\",\"filename\":\"image.jpg\"},{\"contentLocation\":\"/image.png\",\"filename\":\"image.png\"}],\"contentLocation\":\"/some/location\"},\"identifier\":\"xyz123\",\"mapping\":{\"from\":\"abc\",\"to\":\"xyz\"}}"}
  end
end
