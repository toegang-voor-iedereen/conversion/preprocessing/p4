defmodule P4.Amqp.Serialization.CaseConverterTest do
  use ExUnit.Case, async: true

  test "Converting data structure to snake" do
    assert P4.Amqp.Serialization.CaseConverter.to_snake_case([
             %{
               "contentLocation" => "/some/location",
               "assets" => [
                 %{
                   "filename" => "image.jpg",
                   "contentLocation" => "/data/image.jpg"
                 },
                 %{
                   "filename" => "image.png",
                   "contentLocation" => "/data/image.png"
                 }
               ],
               "xyz" => "abc",
               "lolCat" => "cat"
             },
             "abc"
           ]) === [
             %{
               "assets" => [
                 %{"content_location" => "/data/image.jpg", "filename" => "image.jpg"},
                 %{"content_location" => "/data/image.png", "filename" => "image.png"}
               ],
               "content_location" => "/some/location",
               "lol_cat" => "cat",
               "xyz" => "abc"
             },
             "abc"
           ]
  end

  test "Converting data structure to camel" do
    assert P4.Amqp.Serialization.CaseConverter.to_camel_case([
             %{
               "content_location" => "/some/location",
               "assets" => [
                 %{
                   "filename" => "image.jpg",
                   "content_location" => "/data/image.jpg"
                 },
                 %{
                   "filename" => "image.png",
                   "content_location" => "/data/image.png"
                 }
               ],
               "xyz" => "abc",
               "lol_cat" => "cat"
             },
             "abc"
           ]) === [
             %{
               "assets" => [
                 %{"contentLocation" => "/data/image.jpg", "filename" => "image.jpg"},
                 %{"contentLocation" => "/data/image.png", "filename" => "image.png"}
               ],
               "contentLocation" => "/some/location",
               "lolCat" => "cat",
               "xyz" => "abc"
             },
             "abc"
           ]
  end
end
