defmodule P4.Amqp.MessageRouterTest do
  use ExUnit.Case, async: true

  test "Routing with empty conversion stack" do
    message = %P4.Amqp.Dto.ConversionMessage{conversion_stack: []}

    assert P4.Amqp.MessageRouter.route(message) === {:outbound, ""}
  end

  test "routing with conversion stack" do
    message = %P4.Amqp.Dto.ConversionMessage{conversion_stack: [%P4.Amqp.Dto.ConversionTypeMapping{from: "abc", to: "xyz"}]}

    assert P4.Amqp.MessageRouter.route(message) === {:conversion, "ZnJvbTphYmM7dG86eHl6"}
  end
end
