defmodule P4.Settings.Amqp.Queue do
  defstruct p4_docx: nil,
            p4_docx_annotator: nil,
            p4_html_postprocessing: nil,
            dlx: nil,
            outbound: nil

  @spec from_config() :: %P4.Settings.Amqp.Queue{}
  def from_config do
    %P4.Settings.Amqp.Queue{
      p4_docx: System.get_env("QUEUE_P4", "queue.conversion.p4_docx"),
      p4_docx_annotator:
        System.get_env("QUEUE_P4_ANNOTATOR", "queue.conversion.p4_docx_annotator"),
      p4_html_postprocessing:
        System.get_env("QUEUE_P4_HTML_POSTPROCESSING", "queue.conversion.p4_html_postprocessing"),
      dlx: System.get_env("QUEUE_DLX", "queue.conversion.graveyard"),
      outbound: System.get_env("QUEUE_CONVERSION_OUTBOUND", "queue.conversion.outbound")
    }
  end
end
