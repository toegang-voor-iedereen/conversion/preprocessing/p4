defmodule P4.Settings.Amqp.Exchange do
  defstruct outbound: nil, conversion: nil, dlx: nil

  @spec from_config() :: %P4.Settings.Amqp.Exchange{}
  def from_config do
    %P4.Settings.Amqp.Exchange{
      outbound: System.get_env("EXCHANGE_OUTBOUND", "exchange.conversion.outbound"),
      conversion: System.get_env("EXCHANGE_CONVERSION", "exchange.conversion"),
      dlx: System.get_env("EXCHANGE_DLX", "exchange.graveyard")
    }
  end
end
