defmodule P4.Settings.Amqp do
  defstruct exchange: %P4.Settings.Amqp.Exchange{}, queue: %P4.Settings.Amqp.Queue{}

  @spec from_config() :: %P4.Settings.Amqp{}
  def from_config do
    %P4.Settings.Amqp{
      exchange: P4.Settings.Amqp.Exchange.from_config(),
      queue: P4.Settings.Amqp.Queue.from_config()
    }
  end
end
