defmodule P4.Xml.Helper do
  @doc """
  Get some child on an element by name.

  ## Examples

  If we try to get it's 'w:r', it'll return.

      iex> P4.Xml.Helper.get_child_by_name({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "w:r")
      {"w:r", [{"c", "x"}], []}

  But if you try any other object, it'll return nil.

      iex> P4.Xml.Helper.get_child_by_name({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz")
      nil

  Also if you try getting it on nil.

      iex> P4.Xml.Helper.get_child_by_name(nil, "xyz")
      nil

  And it's very possible to set your own default.

      iex> P4.Xml.Helper.get_child_by_name(nil, "xyz", {"w:p", [], []})
      {"w:p", [], []}
  """
  def get_child_by_name(element, name, default \\ nil)

  def get_child_by_name({_, _, children}, name, default) when is_binary(name) do
    get_element_by_name(children, name, default)
  end

  def get_child_by_name(_, name, default) when is_binary(name), do: default

  @doc """
  Get element from list by name

  ## Examples

  For example if element is present:

      iex> P4.Xml.Helper.get_element_by_name([{"w:r", [{"c", "x"}], []}], "w:r")
      {"w:r", [{"c", "x"}], []}

  And if not present:

      iex> P4.Xml.Helper.get_element_by_name([{"w:r", [{"c", "x"}], []}], "w:p")
      nil

  But we also support setting your own default value.

      iex> P4.Xml.Helper.get_element_by_name([{"w:r", [{"c", "x"}], []}], "w:p", {"w:p", [], []})
      {"w:p", [], []}
  """
  def get_element_by_name(elements, name, default \\ nil)

  def get_element_by_name([], name, default) when is_binary(name), do: default

  def get_element_by_name([element | others], name, default) when is_binary(name) do
    if element_has_name?(element, name) do
      element
    else
      get_element_by_name(others, name, default)
    end
  end

  @doc """
  Finds the first element for which `matcher` returns true,
  using a depth-first recursive search.
  """
  @spec find_element_recursively(
          Saxy.XML.element() | [Saxy.XML.element()],
          (Saxy.XML.content() -> as_boolean(term()))
        ) :: Saxy.XML.content() | nil
  def find_element_recursively(element, matcher) when not is_list(element) do
    find_element_recursively([element], matcher)
  end

  def find_element_recursively(elements, matcher) when is_list(elements) do
    Enum.find_value(elements, fn element ->
      if matcher.(element) do
        element
      else
        case element do
          {_, _, children} -> find_element_recursively(children, matcher)
          _ -> false
        end
      end
    end)
  end

  @doc """
  Recursively find the first element with a specific name.
  Use this if you want to e.g. find the first <img> element in a document, regardless of what it's nested in.
  """
  @spec find_element_recursively_by_name(Saxy.XML.element() | [Saxy.XML.element()], String.t()) ::
          Saxy.XML.element() | nil
  def find_element_recursively_by_name(element, name) when is_tuple(element) do
    find_element_recursively_by_name([element], name)
  end

  def find_element_recursively_by_name(elements, name) when is_list(elements) do
    elements
    |> Enum.find_value(fn
      element = {^name, _, _} -> element
      {_, _, children} -> find_element_recursively_by_name(children, name)
      _ -> false
    end)
  end

  @doc """
  Recursively find any child elements with a specific name.
  Use this if you want to e.g. find all the <img> elements in a document, regardless of what they're nested in.
  """
  @spec find_elements_recursively_by_name(Saxy.XML.element() | [Saxy.XML.element()], String.t()) ::
          [Saxy.XML.element()]
  def find_elements_recursively_by_name(element, name) when is_tuple(element) do
    find_elements_recursively_by_name([element], name)
  end

  def find_elements_recursively_by_name(elements, name) when is_list(elements) do
    find_by_name = fn element ->
      do_find_by_name = fn
        element = {^name, _, children}, recurse ->
          [element | Enum.flat_map(children, fn child -> recurse.(child, recurse) end)]

        {_, _, []}, _ ->
          []

        {_, _, children}, recurse ->
          Enum.flat_map(children, fn child -> recurse.(child, recurse) end)

        _string_contents, _ ->
          []
      end

      do_find_by_name.(element, do_find_by_name)
    end

    elements
    |> Enum.flat_map(find_by_name)
  end

  @doc """
  Get value from element or list.

  ## Examples

      If the attribute is present, we will be able to get it.

      iex> P4.Xml.Helper.get_attribute_value({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "a")
      123

      If not, returning 'nil'.

      iex> P4.Xml.Helper.get_attribute_value({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz")
      nil

      Or if trying to get it on nil, also returns nil.

      iex> P4.Xml.Helper.get_attribute_value({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz")
      nil

      Unless you set a default.

      iex> P4.Xml.Helper.get_attribute_value({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz", "lol")
      "lol"

      And ofcourse also works for numeric values.

      iex> P4.Xml.Helper.get_attribute_value([{"a", 123}, {"b", 6}], "b")
      6
  """
  def get_attribute_value(subject, name, default \\ nil) when is_binary(name) do
    case get_attr(subject, name) do
      {_key, value} -> value
      _ -> default
    end
  end

  @doc """
  Get attribute from element or list.

  ## Examples

  Let's get a attribute.

      iex> P4.Xml.Helper.get_attr({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "a")
      {"a", 123}

  And if the property does not exist.

      iex> P4.Xml.Helper.get_attr({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz")
      nil

   Or trying to get it on nil, then we return nil

      iex> P4.Xml.Helper.get_attr(nil, "xyz")
      nil

  Unless we set a default

      iex> P4.Xml.Helper.get_attr(nil, "xyz", "Microsoft Word sucks")
      "Microsoft Word sucks"

  Also works for numeric values ofcourse.

      iex> P4.Xml.Helper.get_attr([{"a", 123}, {"b", 6}], "b")
      {"b", 6}
  """
  def get_attr({_, attr, _}, name, default) when is_binary(name),
    do: get_attr(attr, name, default)

  def get_attr([], name, default) when is_binary(name), do: default

  def get_attr([{key, value} | attribs], name, default) when is_binary(name) do
    if key === name do
      {key, value}
    else
      get_attr(attribs, name, default)
    end
  end

  def get_attr(_, name, default) when is_binary(name), do: default

  def get_attr(subject, name) when is_binary(name), do: get_attr(subject, name, nil)

  @doc """
  Check if attribute exist.

  ## Examples

  Let's check a attribute.

      iex> P4.Xml.Helper.has_attr?({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "a")
      true

  And if the property does not exist.

      iex> P4.Xml.Helper.has_attr?({"w:p", [{"a", 123}, {"b", 6}], [{"w:r", [{"c", "x"}], []}]}, "xyz")
      false

  Or on a list.

      iex> P4.Xml.Helper.has_attr?([{"a", 123}, {"b", 6}], "a")
      true
      iex> P4.Xml.Helper.has_attr?([{"a", 123}, {"b", 6}], "xyz")
      false
  """
  def has_attr?({_, attr, _}, name) when is_list(attr) and is_binary(name),
    do: has_attr?(attr, name)

  def has_attr?(attr, name) when is_list(attr) and is_binary(name) do
    attr
    |> get_attr(name)
    |> is_not_nil()
  end

  defp is_not_nil(nil), do: false
  defp is_not_nil(_), do: true

  @doc """
  Check if element has specific name.

      iex> P4.Xml.Helper.element_has_name?({"w:p", [], []}, "w:p")
      true

      iex> P4.Xml.Helper.element_has_name?({"w:p", [], []}, "w:sdt")
      false
  """
  def element_has_name?({name, _, _}, name), do: true
  def element_has_name?(_, _), do: false

  @doc """
  Get the name of an element.

      iex> P4.Xml.Helper.get_element_name({"w:p", [], []})
      "w:p"

      iex> P4.Xml.Helper.get_element_name("some text")
      nil
  """
  def get_element_name({name, _, _}), do: name
  def get_element_name(_), do: nil

  @doc """
  Get names of multiple elements (not recursive).

      iex> P4.Xml.Helper.get_element_names(["Some string", {"w:p", [], []}, {"w:sdt", [], []}])
      ["w:p", "w:sdt"]
  """
  def get_element_names([{name, _, _} | others]), do: [name | get_element_names(others)]
  def get_element_names([_ | others]), do: get_element_names(others)
  def get_element_names([]), do: []

  @doc """
  Filter elements with specific names

      iex> P4.Xml.Helper.filter_elements_on_names(
      ...>  [
      ...>   {"w:r", [], []},
      ...>   {"w:p", [], []},
      ...>   {"w:pPr", [], []},
      ...>   "Some text" ],
      ...> ["w:r", "w:p"])
      [{"w:pPr", [], []}, "Some text"]
  """
  def filter_elements_on_names([{name, _, _} = elem | others], names) do
    if Enum.member?(names, name) do
      filter_elements_on_names(others, names)
    else
      [elem | filter_elements_on_names(others, names)]
    end
  end

  def filter_elements_on_names([elem | others], names),
    do: [elem | filter_elements_on_names(others, names)]

  def filter_elements_on_names([], _),
    do: []

  @doc """
  Merge two sets of attributes

      iex> P4.Xml.Helper.merge_attributes([{"abc", "123"}, {"foo", "bar"}], [{"abc", "xyz"}, {"hello", "world"}])
      [{"abc", "xyz"}, {"foo", "bar"}, {"hello", "world"}]

      iex> P4.Xml.Helper.merge_attributes([{"abc", "123"}, {"foo", "bar"}], [])
      [{"abc", "123"}, {"foo", "bar"}]

      iex> P4.Xml.Helper.merge_attributes([], [{"abc", "xyz"}, {"hello", "world"}])
      [{"abc", "xyz"}, {"hello", "world"}]
  """
  def merge_attributes(left, right) do
    left
    |> Map.new()
    |> Map.merge(Map.new(right))
    |> Enum.to_list()
  end

  @doc """
  Upsert attribute value.

  ## Examples

      iex> P4.Xml.Helper.upsert_attribute([{"abc", "123"}, {"foo", "bar"}], "abc", "xyz")
      [{"abc", "xyz"}, {"foo", "bar"}]

      iex> P4.Xml.Helper.upsert_attribute([{"abc", "123"}, {"foo", "bar"}], "hello", "world")
      [{"abc", "123"}, {"foo", "bar"}, {"hello", "world"}]

      iex> P4.Xml.Helper.upsert_attribute([], "new", "value")
      [{"new", "value"}]
  """
  def upsert_attribute(attributes, key, value) do
    attributes
    |> Map.new()
    |> Map.put(key, value)
    |> Enum.to_list()
  end

  @doc """
  Wrap element by other

  ## Examples

      iex> P4.Xml.Helper.wrap({"w:t", [], ["Some text"]}, "w:p")
      {"w:p", [], [{"w:t", [], ["Some text"]}]}
  """
  def wrap(element, name) when is_binary(name), do: wrap_elements([element], name)

  @doc """
  Wrap elements by other

  ## Examples

      iex> P4.Xml.Helper.wrap_elements([{"w:t", [], ["Some text"]}, {"w:t", [], ["Some more text"]}], "w:p")
      {"w:p", [], [{"w:t", [], ["Some text"]}, {"w:t", [], ["Some more text"]}]}
  """
  def wrap_elements(elements, name) when is_binary(name) and is_list(elements) do
    {name, [], elements}
  end

  @doc """
  ## Examples

      iex> P4.Xml.Helper.remove_children_by_name({"w:p", [], [{"w:p", [], []}, {"w:r", [], []}, "Some text"]}, "w:p")
      {"w:p", [], [{"w:r", [], []}, "Some text"]}
  """
  def remove_children_by_name({name, attr, children}, name_to_remove)
      when is_binary(name_to_remove) do
    {name, attr, remove_elements_by_name(children, name_to_remove)}
  end

  @doc """
  ## Examples

      iex> P4.Xml.Helper.remove_elements_by_name([{"w:p", [], []}, {"w:r", [], []}, "Some text"], "w:p")
      [{"w:r", [], []}, "Some text"]
  """
  def remove_elements_by_name(elements, name) when is_list(elements) and is_binary(name) do
    Enum.filter(elements, fn x -> !element_has_name?(x, name) end)
  end

  @doc """
  ## Examples

      iex> P4.Xml.Helper.add_child({"w:p", [], [{"w:p", [], []}, {"w:r", [], []}, "Some text"]}, {"w:t", [], []})
      {"w:p", [], [{"w:t", [], []}, {"w:p", [], []}, {"w:r", [], []}, "Some text"]}
  """
  def add_child({name, attr, children}, child) when is_list(children) do
    {name, attr, [child | children]}
  end

  @doc """
  ## Examples

      iex> P4.Xml.Helper.remove_attr_by_name([{"w:val", "x"}, {"w:color", "000000"}], "w:val")
      [{"w:color", "000000"}]
  """
  def remove_attr_by_name(attributes, name) when is_list(attributes) and is_binary(name) do
    attributes
    |> Enum.filter(fn {key, _value} -> key !== name end)
  end

  @doc """
  Upsert an element in the list. If an element with the same tag is found, it is replaced; otherwise, the element is
  added.

  ## Examples

      iex> P4.Xml.Helper.upsert_element([{"w:p", [], []}], {"w:t", [], []})
      [{"w:p", [], []}, {"w:t", [], []}]

      iex> P4.Xml.Helper.upsert_element([{"w:t", [{"x", "y"}], []}], {"w:t", [{"a", "b"}], []})
      [{"w:t", [{"a", "b"}], []}]

  On an empty list:

      iex> P4.Xml.Helper.upsert_element([], {"w:t", [{"a", "b"}], []})
      [{"w:t", [{"a", "b"}], []}]
  """
  def upsert_element(elements, element = {name, _, _})
      when is_list(elements) and is_tuple(element) do
    {is_updated?, updated_elements} =
      Enum.reduce(
        elements,
        {false, []},
        fn
          {current_name, _, _}, {false, acc} when current_name === name ->
            {true, [element | acc]}

          current_element, {updated?, acc} ->
            {updated?, [current_element | acc]}
        end
      )

    # Reverse to maintain the original order
    if is_updated? do
      Enum.reverse(updated_elements)
    else
      Enum.reverse([element | updated_elements])
    end
  end

  @spec transform_recursive_child(
          Saxy.XML.content(),
          (Saxy.XML.content() -> as_boolean(term())),
          (Saxy.XML.content() -> Saxy.XML.content() | [Saxy.XML.content()])
        ) :: Saxy.XML.element()
  def transform_recursive_child(element, matcher, transformer) do
    if matcher.(element) do
      transformer.(element)
    else
      case element do
        {name, attrs, children} ->
          {name, attrs,
           Enum.map(children, fn child ->
             transform_recursive_child(child, matcher, transformer)
           end)}

        _ ->
          element
      end
    end
  end
end
