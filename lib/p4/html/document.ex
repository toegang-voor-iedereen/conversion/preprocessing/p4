defmodule P4.Html.Document do
  @type t :: %__MODULE__{id: String.t(), document: Saxy.XML.element()}
  defstruct id: nil, document: nil
end
