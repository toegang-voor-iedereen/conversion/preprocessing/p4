defmodule P4.Html.PostProcessor do
  @moduledoc """
  This module is responsible for post-processing HTML documents.
  It lists and calls all the transformers in the `P4.Html.Transformer` module.
  """

  alias P4.Html.Transformer

  @spec transformers() :: [module()]
  def transformers() do
    [
      Transformer.DecodeImgAltJson,
      Transformer.RemoveMiniImages,
      Transformer.StripUUIDComments,
      Transformer.StripSpans,
      Transformer.StripBlockQuotes,
      Transformer.RemovePAroundImg,
      Transformer.CombineMultilineTitle
    ]
  end

  @spec process(P4.Html.Document.t()) :: {:ok, P4.Html.Document.t()} | {:error, Exception.t()}
  def process(document = %P4.Html.Document{}) do
    {:ok,
     Enum.reduce(transformers(), document, fn transformer, document ->
       transformer.transform(document)
     end)}
  end
end
