require OK

defmodule P4.Html.Reader do
  use OK.Pipe

  @spec read_from_path(String.t(), String.t()) ::
          {:ok, %P4.Html.Document{}} | {:error, Exception.t()}
  def read_from_path(path, id) when is_binary(path) and is_binary(id) do
    OK.try do
      document <-
        File.read(path)
        ~>> Saxy.SimpleForm.parse_string(expand_entity: :never)
        |> P4.Error.Html.Reader.FromPath.wrap(%{path: path})
    after
      {:ok, %P4.Html.Document{id: id, document: document}}
    rescue
      reason -> {:error, reason}
    end
  end
end
