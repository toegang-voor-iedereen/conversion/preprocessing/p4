defmodule P4.Html.Transformer.DecodeImgAltJson do
  @moduledoc """
  This module is a template for a P4.Html.Transformer. Simply copy and implement how you see fit.
  """

  use P4.Html.Transformer

  def transform({"img", attributes, children}) do
    # e30K is base64 for {}
    {message, uuid} =
      P4.Xml.Helper.get_attribute_value(attributes, "alt", "e30K")
      |> Base.decode64!()
      |> Jason.decode!()
      |> parse_alt()

    new_attributes =
      attributes
      |> Enum.filter(fn {key, _} -> key != "alt" end)
      |> P4.Std.prepend_if(uuid != "", {"data-uuid", uuid})
      |> P4.Std.prepend_if(message != "", {"alt", message})

    {"img", new_attributes, transform(children)}
  rescue
    reason ->
      P4.Logger.error("failed to decode alt tag", error: reason)
      {"img", attributes, transform(children)}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}

  @spec parse_alt(map) :: {String.t(), String.t()}
  defp parse_alt(%{"message" => message, "uuid" => uuid}), do: {message, uuid}
  defp parse_alt(%{"message" => message}), do: {message, ""}
  defp parse_alt(%{"uuid" => uuid}), do: {"", uuid}
  defp parse_alt(%{}), do: {"", ""}
end
