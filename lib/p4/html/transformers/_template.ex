defmodule P4.Html.Transformer.Template do
  @moduledoc """
  This module is a template for a P4.Html.Transformer. Simply copy and implement how you see fit.
  """

  use P4.Html.Transformer

  def transform({"p", attributes, children}) do
    # Implement your custom transformation here.
    # Return a single element, or a list of elements.
    # Returning a list of more than 1 element, will put those elements in place of the current element.
    # Returning an empty list, will remove the element.
    {"p", attributes, transform(children)}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}
end
