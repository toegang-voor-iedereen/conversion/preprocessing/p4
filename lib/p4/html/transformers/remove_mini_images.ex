defmodule P4.Html.Transformer.RemoveMiniImages do
  @moduledoc """
  This module removes all images with a height or width less than 0.5 inch or equivalent in cm or px.
  This is useful for removing small images that are not useful in the context of the document,
  such as chevrons, dividers or other small icons.
  """

  use P4.Html.Transformer

  @mini_image_threshold_inch 0.5
  @mini_image_threshold_cm 1.27
  @mini_image_threshold_px 48

  @impl P4.Html.Transformer.Behaviour
  def transform({"img", attributes, children}) do
    styles =
      P4.Xml.Helper.get_attribute_value(attributes, "style", "")
      |> String.split(";", trim: true)
      |> Enum.map(fn style_line ->
        [key, value] = String.split(style_line, ":", parts: 2, trim: true)
        {key, value}
      end)

    is_mini_image =
      Enum.any?(styles, fn
        {"height", height} -> Float.parse(height) |> is_mini_image?()
        {"width", width} -> Float.parse(width) |> is_mini_image?()
        _ -> false
      end)

    if is_mini_image do
      []
    else
      {"img", attributes, transform(children)}
    end
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}

  defp is_mini_image?({float, "in"}) when is_float(float), do: float <= @mini_image_threshold_inch
  defp is_mini_image?({float, "cm"}) when is_float(float), do: float <= @mini_image_threshold_cm
  defp is_mini_image?({float, "px"}) when is_float(float), do: float <= @mini_image_threshold_px
  defp is_mini_image?(:error), do: false
  defp is_mini_image?(_), do: false
end
