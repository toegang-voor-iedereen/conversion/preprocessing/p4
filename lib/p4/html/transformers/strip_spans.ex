defmodule P4.Html.Transformer.StripSpans do
  @moduledoc """
  This transformer removes:
  - all remaining `<span class='comment-start'>` and `<span class='comment-end'>` tags from the document
    that might still be left over from annotating the original document with UUID comments. Those comments should have been
    stripped out by the `StripUUIDComments` transformer, but if they weren't, this transformer will remove them and log a warning.
  - all spans with class `anchor`
  - all spans from `<title>` elements.
  """

  use P4.Html.Transformer

  def transform({"span", _, _} = element) do
    class = P4.Xml.Helper.get_attribute_value(element, "class")

    cond do
      class in ["comment-start", "comment-end"] ->
        P4.Logger.warning(
          "Removing a comment span that should have been removed by the StripUUIDComments transformer"
        )

        []

      class == "anchor" ->
        []

      true ->
        element
    end
  end

  def transform({"title", attrs, children}) do
    new_children =
      children
      |> Enum.filter(fn
        {"span", _, _} -> false
        _ -> true
      end)

    {"title", attrs, transform(new_children)}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}
end
