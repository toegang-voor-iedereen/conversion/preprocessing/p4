defmodule P4.Html.Transformer.RemovePAroundImg do
  @moduledoc """
  This module removes <p> tags around <img> tags and strips all but the src, alt and data-uuid properties from <img> tags, i.e. transforms

      <p><img src="image.jpg" alt="An image" class='test'></p>

  into

      <img src="image.jpg" alt="An image">

  and transforms

      <p>
        Some text before image
        <img src="image.jpg" alt="An image">
        Some text after image
        Some more text after the image
      </p>

  into

      <p>Some text before image</p>
      <img src="image.jpg" alt="An image">
      <p>Some text after image</p>
      <p>Some more text after the image</p>
  """

  use P4.Html.Transformer

  def transform({"p", _, [img_element = {"img", _, _}]}), do: transform(img_element)

  def transform({"p", attrs, children}) do
    if Enum.any?(children, &is_img_element?/1) do
      Enum.map(children, fn
        element ->
          if is_img_element?(element) do
            transform(element)
          else
            {"p", attrs, [transform(element)]}
          end
      end)
    else
      {"p", attrs, transform(children)}
    end
  end

  # for image elements, we only want to keep the src, alt and data-uuid attributes, all other attributes are removed.
  @img_allowed_attributes ["src", "alt", "data-uuid"]

  def transform({"img", attrs, children}) do
    filtered_attrs = attrs |> Enum.filter(fn {key, _} -> key in @img_allowed_attributes end)
    {"img", filtered_attrs, transform(children)}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}

  defp is_img_element?({"img", _, _}), do: true
  defp is_img_element?(_), do: false
end
