defmodule P4.Html.Transformer.StripUUIDComments do
  @moduledoc """
  This module is responsible for post-processing HTML documents.
  The post-processing currently entails removing UUID comments, e.g. it transforms:

      <h1>
        <span class="comment-start" id="1000" data-author="P4">a1936bc7-0156-4fa9-90d3-991d9574850c</span>
        Heading 1
        <span class="comment-end" id="1000"></span>
      </h1>

  into

      <h1 data-uuid="a1936bc7-0156-4fa9-90d3-991d9574850c">
        Heading 1
      </h1>

  It does this for all headings (h1-h6) in the document, as well as all paragraphs (p).
  """
  use P4.Html.Transformer

  def transform({"h1", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"h2", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"h3", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"h4", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"h5", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"h6", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"p", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"td", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"th", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({"title", _attrs, _children} = element),
    do: transform_element_with_uuid_comment(element)

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}

  @doc """
  This function removes the UUID comments from an HTML element.

  Pandoc transforms our Word UUID comments into spans with the classes `comment-start` and `comment-end`,
  which are direct children of the element they are defined on. This function takes the UUID from the `comment-start` span's content
  and places it in a `data-uuid` attribute on the element itself. It then removes the `comment-start` and `comment-end` spans.
  """
  @spec transform_element_with_uuid_comment(Saxy.XML.element()) :: Saxy.XML.element()
  def transform_element_with_uuid_comment({name, attributes, children}) do
    comment_span =
      Enum.find(children, fn
        {"span", [{"class", "comment-start"} | _], _} -> true
        _ -> false
      end)

    if comment_span != nil do
      {uuid, extra_child} = get_uuid_from_comment_start(comment_span)

      new_children =
        Enum.filter(children, fn
          {"span", [{"class", "comment-start"} | _], _} -> false
          {"span", [{"class", "comment-end"} | _], _} -> false
          _ -> true
        end)
        |> transform()
        |> P4.Std.prepend_if(extra_child != nil, extra_child)

      {name, [{"data-uuid", uuid} | attributes], new_children}
    else
      {name, attributes, transform(children)}
    end
  end

  # Regular case: the UUID is the only content of the comment-start span
  @spec get_uuid_from_comment_start(Saxy.XML.element()) :: {String.t(), Saxy.XML.content() | nil}
  defp get_uuid_from_comment_start({_, _, [uuid]}) when is_binary(uuid), do: {uuid, nil}

  # Special case: drop caps have a comment-start span within the element's original comment-start span.
  defp get_uuid_from_comment_start(
         {_, _, [{"span", [{"class", "comment-start"} | _], _}, dropcap | rest]}
       ) do
    {find_uuid(rest), dropcap}
  end

  # Special contingency case: the comment-start span has a strange structure. We'll log this and try to find a valid UUID in there anyways to return that.
  defp get_uuid_from_comment_start({name, _, children}) do
    uuid = find_uuid(children)

    P4.Logger.warning(
      "Found element #{name} with strange UUID comment-start structure (uuid: #{uuid}): #{inspect(children)}"
    )

    {uuid, nil}
  end

  defp find_uuid(list) when is_list(list) do
    Enum.find(list, fn
      content when is_binary(content) -> Ecto.UUID.cast(content) != :error
      _ -> false
    end)
  end
end
