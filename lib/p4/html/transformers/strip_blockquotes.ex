defmodule P4.Html.Transformer.StripBlockQuotes do
  @moduledoc """
  This Html transformer fixes some weird Pandoc behaviour by removing blockquote elements that it may insert in table cells and list items.
  """

  use P4.Html.Transformer

  def transform({"th", attributes, [{"blockquote", _, children}]}) do
    {"th", attributes, children}
  end

  def transform({"td", attributes, [{"blockquote", _, children}]}) do
    {"td", attributes, children}
  end

  def transform({"li", attributes, [{"blockquote", _, children}]}) do
    {"li", attributes, children}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}
end
