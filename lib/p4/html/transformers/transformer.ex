require OK

defmodule P4.Html.Transformer.Behaviour do
  @moduledoc """
  Defines the behaviour for a transformer that can transform elements from HTML documents into other elements.
  """

  @callback transform(P4.Html.Document.t()) :: P4.Html.Document.t()
  @callback transform(Saxy.XML.content()) :: Saxy.XML.content() | [Saxy.XML.content()]
  @callback transform([Saxy.XML.content()]) :: [Saxy.XML.content()]
end

defmodule P4.Html.Transformer do
  @moduledoc """
  A module that can be used to define a transformer for HTML documents.
  """
  defmacro __using__(_opts) do
    quote do
      @behaviour P4.Html.Transformer.Behaviour
      defoverridable P4.Html.Transformer.Behaviour

      def transform(document = %P4.Html.Document{document: root}),
        do: %P4.Html.Document{document: transform(root)}

      def transform([]), do: []
      def transform([head | tail]), do: [transform(head) | transform(tail)] |> List.flatten()

      def transform(string_contents) when is_binary(string_contents), do: string_contents
    end
  end
end
