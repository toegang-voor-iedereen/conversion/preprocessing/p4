defmodule P4.Html.Transformer.CombineMultilineTitle do
  @moduledoc """
  This module is an Html transformer that combines multiline titles into one single line.
  Pandoc translates multi-line titles into title elements that contain multiple `<p>` tags.
  This transformer filters out those `<p>` tags and removes any formatting from the title contents.
  """

  use P4.Html.Transformer

  def transform({"title", attributes, children}) do
    {"title", attributes, transform_title_element(children)}
  end

  def transform({name, attributes, children}), do: {name, attributes, transform(children)}

  def transform_title_element([]), do: []

  def transform_title_element([head | tail]),
    do: [transform_title_element(head) | transform_title_element(tail)] |> List.flatten()

  def transform_title_element({"p", _, children}) do
    transform_title_element(children)
  end

  def transform_title_element({"u", _, children}) do
    transform_title_element(children)
  end

  def transform_title_element({"strong", _, children}) do
    transform_title_element(children)
  end

  def transform_title_element({"em", _, children}) do
    transform_title_element(children)
  end

  def transform_title_element(other), do: other
end
