require OK

defmodule P4.Html.Writer do
  @spec write(%P4.Html.Document{}) :: {:ok, String.t()} | {:error, Exception.t()}
  def write(%P4.Html.Document{document: document}) do
    OK.try do
      path_to <- P4.IO.File.create_temp()
      encoded_document = Saxy.encode!(document)
    after
      case File.write(path_to, encoded_document) do
        :ok ->
          {:ok, path_to}

        {:error, reason} ->
          {:error,
           %P4.Error.Html.Writer{reason: reason, context: %{path: path_to, document: document}}}
      end
    rescue
      reason = %P4.Error.Html.Writer{} -> {:error, reason}
      reason -> {:error, %P4.Error.Html.Writer{reason: reason, context: %{document: document}}}
    end
  end
end
