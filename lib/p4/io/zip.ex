require OK

defmodule P4.IO.Zip do
  @spec create_from_directory(String.t(), [String.t()], String.t()) :: {:ok, :done} | {:error, Exception.t()}
  def create_from_directory(directory_path, file_list, output_zip_path)
      when is_binary(directory_path) and is_list(file_list) and is_binary(output_zip_path) do
    output_zip_path
    |> String.to_charlist()
    |> :zip.create(Enum.map(file_list, &String.to_charlist/1), cwd: directory_path)
    |> P4.Error.Zip.CreatingFromDirectory.wrap(%{
      directory_path: directory_path,
      file_list: file_list,
      output_zip_path: output_zip_path
    })
  end

  @spec extract(String.t(), String.t()) :: {:ok, :done} | {:error, Exception.t()}
  def extract(archive_path, extraction_path)
      when is_binary(archive_path) and is_binary(extraction_path) do
    archive_path
    |> String.to_charlist()
    |> :zip.extract(cwd: extraction_path)
    |> P4.Error.Zip.Extracting.wrap(%{archive_path: archive_path, extraction_path: extraction_path})
  end
end
