defmodule P4.IO.File do
  @moduledoc """
  This module provides functions for interacting with the local file system.
  """

  @doc """
  Create a file in the system's temporary files directory, prefixing the file name with the given path.
  """
  @spec create_temp(String.t() | nil) :: {:ok, String.t()} | {:error, Exception.t()}
  def create_temp(prefix \\ nil) do
    Temp.path(prefix)
    |> P4.Error.IO.CreateTempFile.wrap(%{})
  end

  @doc """
  Remove a file from the file system.
  """
  @spec remove(String.t()) :: {:ok, :done} | {:error, Exception.t()}
  def remove(path) when is_binary(path) do
    path
    |> File.rm()
    |> P4.Error.IO.RemoveFile.wrap(%{path: path})
  end
end
