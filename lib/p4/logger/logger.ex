defmodule P4.Logger do
  @moduledoc """
  The Logger module provides a simple logging interface for the P4 application.
  """
  require Logger

  def init() do
    Logger.metadata(
      "service.name": Application.get_env(:p4, :name),
      "service.version": Application.get_env(:p4, :version),
      "service.environment": Application.get_env(:p4, :env)
    )
  end

  for level <- Logger.levels() do
    @spec unquote(level)(String.t(), keyword() | nil) :: :ok
    @spec unquote(level)(keyword(), keyword() | nil) :: :ok
    @spec unquote(level)(Map, keyword() | nil) :: :ok
    def unquote(level)(message, metadata \\ []) do
      Logger.unquote(level)(
        message,
        [{:__caller, get_caller()} | metadata]
        |> Enum.flat_map(fn
          # Transform error into crash_reason
          {:error, error} when is_exception(error) ->
            # 4x tl() to remove this anonymous function, Enum.flat_map and this log function from the stacktrace.
            [{:crash_reason, {error, tl(tl(tl(tl(get_stacktrace()))))}}]

          # Overwrite Logger's default file, line and mfa metadata with our own,
          # since we want those to reflect the caller of the `P4.Logger` functions,
          # and not the caller of Elixir's Logger functions (which is `P4.Logger` itself).
          {:__caller, caller} ->
            transform_caller(caller)

          x ->
            [x]
        end)
      )
    end
  end

  @doc """
  Returns the stacktrace of the calling function.
  """
  @spec get_stacktrace() :: [{Module, atom(), integer(), [file: charlist(), line: integer()]}]
  def get_stacktrace() do
    {:current_stacktrace, stacktrace} = Process.info(self(), :current_stacktrace)
    # Remove the first two elements of the stacktrace, which are Process.info and this function.
    tl(tl(stacktrace))
  end

  # Returns the module, function, arity, file and line of the function that calls the function that calls this function.
  @spec get_caller() :: {Module, atom(), integer(), [file: charlist(), line: integer()]}
  defp get_caller() do
    hd(tl(tl(get_stacktrace())))
  end

  @spec transform_caller({Module, atom(), integer(), [file: charlist(), line: integer()]}) ::
          keyword()
  defp transform_caller({module, function, arity, [file: file, line: line]}) do
    [
      {:file, file},
      {:line, line},
      {:mfa, {module, function, arity}}
    ]
  end
end
