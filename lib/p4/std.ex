defmodule P4.Std do
  @doc """
  Returns the first argument if it is not nil, otherwise returns the second argument.
  """
  @spec default(any(), any()) :: any()
  def default(nil, right), do: right
  def default(left, _), do: left

  @doc """
  Returns true if the value is not nil.
  """
  @spec is_not_nil(any()) :: boolean()
  def is_not_nil(value), do: not is_nil(value)

  @doc """
  Prepends the value to the list if the condition is true.
  """
  @spec prepend_if(list(), boolean(), any()) :: list()
  def prepend_if(list, condition, value) do
    if condition, do: [value | list], else: list
  end
end
