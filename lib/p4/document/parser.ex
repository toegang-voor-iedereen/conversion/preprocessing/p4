defmodule P4.Document.Parser do
  require P4.Xml.Helper

  @doc """
  Check if element could be considered a Table of Contents

  ### Example of element that is a Table of Contents

      iex> P4.Document.Parser.is_toc(
      ...>   {"w:sdt", [],
      ...>     [{"w:sdtPr", [],
      ...>       [{"w:docPartObj", [],
      ...>         [{"w:docPartGallery", [{"w:val", "Table of Contents"}], []}]}]}]})
      true

  ### Example of element that is not a Table of Contents

      iex> P4.Document.Parser.is_toc(
      ...>   {"w:sdt", [],
      ...>     [{"w:sdtPr", [],
      ...>       [{"w:docPartObj", [],
      ...>         [{"w:docPartGallery", [{"w:val", "Not a TOC"}], []}]}]}]})
      false

  ### Example of different element

      iex> P4.Document.Parser.is_toc({"w:p", [], []})
      false
  """
  def is_toc({"w:sdt", _, _} = elem) do
    doc_part_gallery_value =
      elem
      |> P4.Xml.Helper.get_child_by_name("w:sdtPr")
      |> P4.Xml.Helper.get_child_by_name("w:docPartObj")
      |> P4.Xml.Helper.get_child_by_name("w:docPartGallery")
      |> P4.Xml.Helper.get_attribute_value("w:val")

    doc_part_gallery_value === "Table of Contents"
  end

  def is_toc(_), do: false

  @doc """
  Get Style id of a 'w:p'`', 'w:pStyle' or 'w:pPr'

  ## Examples

  Getting style from paragraph:

      iex> P4.Document.Parser.get_style_id({"w:p", [], [{"w:pPr", [], [{"w:pStyle", [{"w:val", "ID"}], []}]}]})
      "ID"

  Getting style from paragraph, but it's not present:

      iex> P4.Document.Parser.get_style_id({"w:p", [], [{"w:pPr", [], [{"w:pStyle", [], []}]}]})
      nil

  Getting style from paragraph, it's not present, but we set a default:

      iex> P4.Document.Parser.get_style_id({"w:p", [], [{"w:pPr", [], [{"w:pStyle", [], []}]}]}, "Normal")
      "Normal"

  Getting style from paragraph properties:

      iex> P4.Document.Parser.get_style_id({"w:pPr", [], [{"w:pStyle", [{"w:val", "ID"}], []}]})
      "ID"

  Getting style from a 'pStyle':

      iex> P4.Document.Parser.get_style_id({"w:pStyle", [{"w:val", "ID"}], []})
      "ID"
  """
  def get_style_id(element), do: get_style_id(element, nil)

  def get_style_id({"w:p", _, children}, default) do
    children
    |> P4.Xml.Helper.get_element_by_name("w:pPr")
    |> get_style_id(default)
  end

  def get_style_id({"w:pPr", _, children}, default) do
    children
    |> P4.Xml.Helper.get_element_by_name("w:pStyle")
    |> get_style_id(default)
  end

  def get_style_id({"w:pStyle", attr, _}, default), do: P4.Xml.Helper.get_attribute_value(attr, "w:val", default)
  def get_style_id(nil, default), do: default

  @doc """
  Set Style id of a 'w:p'`' or 'w:pPr'

  ## Examples

  Setting style to paragraph:

      iex> P4.Document.Parser.set_style_id(
      ...>   {"w:p", [], [{"w:pPr", [], [{"w:pStyle", [{"w:val", "Earlier ID"}], []}]}]}, "New ID")
      {"w:p", [], [{"w:pPr", [], [{"w:pStyle", [{"w:val", "New ID"}], []}]}]}

  Setting style to paragraph, but it's not present:

      iex> P4.Document.Parser.set_style_id({"w:p", [], []}, "New ID")
      {"w:p", [], [{"w:pPr", [], [{"w:pStyle", [{"w:val", "New ID"}], []}]}]}

  Setting style to paragraph properties:

      iex> P4.Document.Parser.set_style_id({"w:pPr", [], [{"w:pStyle", [{"w:val", "Earlier ID"}], []}]}, "New ID")
      {"w:pPr", [], [{"w:pStyle", [{"w:val", "New ID"}], []}]}
  """
  def set_style_id({"w:p", attr, children}, value) when is_binary(value) do
    properties =
      children
      |> P4.Xml.Helper.get_element_by_name("w:pPr", {"w:pPr", [], []})
      |> set_style_id(value)

    {
      "w:p",
      attr,
      P4.Xml.Helper.upsert_element(children, properties)
    }
  end

  def set_style_id({"w:pPr", attr, children}, value) when is_binary(value) do
    {
      "w:pPr",
      attr,
      P4.Xml.Helper.upsert_element(children, {"w:pStyle", [{"w:val", value}], []})
    }
  end
end
