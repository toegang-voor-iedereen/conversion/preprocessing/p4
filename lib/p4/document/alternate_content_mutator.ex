defmodule P4.Document.AlternateContentMutator do
  def process_alternate_content(element) do
    extra_elements = extract_alternate_content(element)
    remove_alternate_contents(element) ++ extra_elements
  end

  defp extract_alternate_content({"mc:Choice", _, _children}), do: []

  defp extract_alternate_content({"mc:Fallback", _, children}) do
    extract_alternate_content_within_fallback(children)
  end

  defp extract_alternate_content({_, _, children}) do
    extract_alternate_content(children)
  end

  defp extract_alternate_content(elements) when is_list(elements) do
    Enum.flat_map(elements, &extract_alternate_content/1)
  end

  defp extract_alternate_content(_), do: []

  defp remove_alternate_contents({"mc:AlternateContent", _, _}), do: []

  defp remove_alternate_contents(elements) when is_list(elements) do
    Enum.flat_map(elements, &remove_alternate_contents/1)
  end

  defp remove_alternate_contents({name, attr, children}) do
    [{name, attr, remove_alternate_contents(children)}]
  end

  defp remove_alternate_contents(any), do: [any]

  defp extract_alternate_content_within_fallback({"w:p", _attr, _children} = para), do: [para]

  defp extract_alternate_content_within_fallback({"v:imagedata", _, _} = imagedata) do
    [
      {"w:p", [], [{"w:r", [], [{"w:pict", [], [{"v:shape", [], [imagedata]}]}]}]}
    ]
  end

  defp extract_alternate_content_within_fallback(elements) when is_list(elements) do
    Enum.flat_map(elements, &extract_alternate_content_within_fallback/1)
  end

  defp extract_alternate_content_within_fallback({_name, _attr, children}) do
    extract_alternate_content_within_fallback(children)
  end

  defp extract_alternate_content_within_fallback(any), do: [any]
end
