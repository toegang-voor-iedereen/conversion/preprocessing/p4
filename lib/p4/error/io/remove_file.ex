defmodule P4.Error.IO.RemoveFile do
  use P4.Macro.ErrorDefinition, message: "Error removing file"
end
