defmodule P4.Error.IO.CreateTempFile do
  use P4.Macro.ErrorDefinition, message: "Failure creating temporary path"
end
