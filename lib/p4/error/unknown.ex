defmodule P4.Error.Unknown do
  defexception message: "Unknown error", reason: nil
end
