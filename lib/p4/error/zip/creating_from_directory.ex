defmodule P4.Error.Zip.CreatingFromDirectory do
  use P4.Macro.ErrorDefinition, message: "Failure creating zip from directory"
end
