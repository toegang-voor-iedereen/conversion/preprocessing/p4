defmodule P4.Error.Zip.Extracting do
  use P4.Macro.ErrorDefinition, message: "Failure extracting zip to directory"
end
