defmodule P4.Error.P4.Docx.Reader.CreateTempDir do
  use P4.Macro.ErrorDefinition, message: "Failure creating temporary directory"
end
