defmodule P4.Error.P4.Docx.Reader.ParsingXml do
  use P4.Macro.ErrorDefinition, message: "Failure parsing XML"
end
