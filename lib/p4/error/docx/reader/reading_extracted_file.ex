defmodule P4.Error.P4.Docx.Reader.ReadingExtractedFile do
  use P4.Macro.ErrorDefinition, message: "Failure reading extracted file"
end
