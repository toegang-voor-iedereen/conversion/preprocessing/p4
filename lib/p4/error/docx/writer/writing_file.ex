defmodule P4.Error.P4.Docx.Writer.WritingFile do
  use P4.Macro.ErrorDefinition, message: "Failure writing file during writing outcome"
end
