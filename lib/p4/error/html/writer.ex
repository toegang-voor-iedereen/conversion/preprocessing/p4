defmodule P4.Error.Html.Writer do
  use P4.Macro.ErrorDefinition, message: "Failure writing HTML file"
end
