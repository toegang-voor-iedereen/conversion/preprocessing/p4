defmodule P4.Error.Html.Reader.FromPath do
  use P4.Macro.ErrorDefinition, message: "Failure reading HTML file from path"
end
