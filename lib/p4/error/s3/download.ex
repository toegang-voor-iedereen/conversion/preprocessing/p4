defmodule P4.Error.S3.Download do
  use P4.Macro.ErrorDefinition, message: "Failure downloading file from S3"
end
