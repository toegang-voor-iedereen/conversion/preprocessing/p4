defmodule P4.Error.S3.Upload do
  use P4.Macro.ErrorDefinition, message: "Failure uploading file to S3"
end
