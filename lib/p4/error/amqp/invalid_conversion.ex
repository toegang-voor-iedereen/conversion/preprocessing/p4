defmodule P4.Error.Amqp.InvalidConversion do
  defexception message: "Requested conversion is invalid and unsupported",
               amqp_message: %P4.Amqp.Dto.ConversionMessage{}
end
