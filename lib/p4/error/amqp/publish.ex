defmodule P4.Error.Amqp.Publish do
  use P4.Macro.ErrorDefinition, message: "Failure publishing"
end
