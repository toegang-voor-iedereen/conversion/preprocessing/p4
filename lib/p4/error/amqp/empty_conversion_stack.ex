defmodule P4.Error.Amqp.EmptyConversionStack do
  defexception message: "Conversion Stack is empty", amqp_message: %P4.Amqp.Dto.ConversionMessage{}
end
