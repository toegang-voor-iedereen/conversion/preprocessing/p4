defmodule P4.Error.Amqp.InvalidPayload do
  defexception message: "Invalid payload delivered", changeset: nil, payload: nil
end
