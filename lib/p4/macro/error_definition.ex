defmodule P4.Macro.ErrorDefinition do
  defmacro __using__(opts) do
    quote do
      unquoted_opts = unquote(opts)

      defexception message: Keyword.get(unquoted_opts, :message, "An error occurred"),
                   reason: Keyword.get(unquoted_opts, :reason, :unknown),
                   context: Keyword.get(unquoted_opts, :context, nil)

      def wrap({:error, reason}, context) do
        message =
          P4.Macro.ErrorDefinition.format_message(__MODULE__.message(%__MODULE__{}), reason)

        {:error, %__MODULE__{message: message, reason: reason, context: context}}
      end

      def wrap(:ok, _),
        do: {:ok, :done}

      def wrap(any, _),
        do: any
    end
  end

  def format_message(message, :unknown) when is_binary(message), do: message

  def format_message(message, reason) when is_binary(message) and is_binary(reason) do
    "#{message}: #{reason}"
  end

  def format_message(message, reason) when is_binary(message) and is_exception(reason) do
    reason_message = Exception.message(reason)
    "#{message}: #{reason_message}"
  end

  def format_message(message, reason) when is_binary(message) do
    "#{message}: #{inspect(reason)}"
  end
end
