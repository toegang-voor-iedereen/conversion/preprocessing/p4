defmodule P4.Misc.KeyGenerator do
  @alphabet ~c"abcdefghijklmnopqrstuvwxyz0123456789"

  @spec generate(number(), Keyword.t()) :: String.t()
  def generate(length, opts \\ []) when is_number(length) do
    prefix = Keyword.get(opts, :prefix, "")
    suffix = Keyword.get(opts, :suffix, "")

    middle =
      1..length
      |> Enum.map(fn _ -> Enum.random(@alphabet) end)
      |> List.to_string()

    prefix <> middle <> suffix
  end
end
