defmodule P4.Amqp.Serialization.CaseConverter do
  @spec to_snake_case(term()) :: term()
  def to_snake_case(data) when is_map(data) do
    Enum.reduce(
      data,
      %{},
      fn
        {key, value}, acc when is_binary(key) ->
          Map.put(acc, Macro.underscore(key), to_snake_case(value))
      end
    )
  end

  def to_snake_case(data) when is_list(data),
    do: Enum.map(data, &to_snake_case/1)

  def to_snake_case(data),
    do: data

  @spec to_camel_case(term()) :: term()
  def to_camel_case(data) when is_map(data) do
    Enum.reduce(
      data,
      %{},
      fn
        {key, value}, acc when is_binary(key) ->
          Map.put(acc, camelize(key), to_camel_case(value))
      end
    )
  end

  def to_camel_case(data) when is_list(data),
    do: Enum.map(data, &to_camel_case/1)

  def to_camel_case(data),
    do: data

  defp camelize(string) when is_binary(string) do
    string
    |> Macro.camelize()
    |> downcase_first()
  end

  defp downcase_first(string) when is_binary(string) do
    {first, rest} = String.split_at(string, 1)

    String.downcase(first) <> rest
  end
end
