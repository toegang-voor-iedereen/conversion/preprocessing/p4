defmodule P4.Amqp.Serialization.Serializer do
  def deserialize(payload) when is_binary(payload) do
    P4.Logger.debug("AMQP: deserializing message")

    case Jason.decode(payload) do
      {:ok, decoded_payload} ->
        changeset =
          decoded_payload
          |> P4.Amqp.Serialization.CaseConverter.to_snake_case()
          |> P4.Amqp.Dto.ConversionMessage.changeset()

        if changeset.valid? do
          {:ok, Ecto.Changeset.apply_changes(changeset)}
        else
          {:error, %P4.Error.Amqp.InvalidPayload{changeset: changeset, payload: payload}}
        end

      result = {:error, _} ->
        result
    end
  end

  def serialize(payload) when is_struct(payload) do
    P4.Logger.debug("AMQP: serializing message")

    payload
    |> map_from_struct()
    |> P4.Amqp.Serialization.CaseConverter.to_camel_case()
    |> Jason.encode()
  end

  defp map_from_struct(list) when is_list(list),
    do: Enum.map(list, &map_from_struct/1)

  defp map_from_struct(struct) when is_struct(struct) do
    struct
    |> Map.from_struct()
    |> map_from_struct()
  end

  defp map_from_struct(map) when is_map(map) do
    Enum.reduce(
      map,
      %{},
      fn
        {key, value}, acc when is_atom(key) ->
          Map.put(acc, Atom.to_string(key), map_from_struct(value))
      end
    )
  end

  defp map_from_struct(value),
    do: value
end
