defmodule P4.Amqp.Topology do
  @moduledoc """
  This module is syntactical sugar over the AMQP library to declare exchanges, queues and bindings.
  See application_topology.ex for the actual queues, exchanges and bindings that are declared in the application.
  """

  defmodule Queue do
    defstruct name: nil, durable: true, arguments: []
  end

  defmodule Exchange do
    defstruct name: nil, durable: true, type: nil, arguments: []
  end

  defmodule Binding do
    defstruct queue: nil, exchange: nil, routing_key: nil
  end

  defstruct exchanges: [], queues: [], bindings: []

  @spec construct(Topology.t(), Channel.t()) :: {:ok, :done} | {:error, Exception.t()}
  def construct(
        topology = %P4.Amqp.Topology{exchanges: [exchange | rest]},
        channel = %AMQP.Channel{}
      ) do
    case construct_exchange(exchange, channel) do
      :ok ->
        construct(%{topology | exchanges: rest}, channel)

      {:error, reason} ->
        {:error, reason}
    end
  end

  def construct(topology = %P4.Amqp.Topology{queues: [queue | rest]}, channel = %AMQP.Channel{}) do
    case construct_queue(queue, channel) do
      {:ok, _} ->
        construct(%{topology | queues: rest}, channel)

      :ok ->
        construct(%{topology | queues: rest}, channel)

      {:error, reason} ->
        {:error, reason}
    end
  end

  def construct(
        topology = %P4.Amqp.Topology{bindings: [binding | rest]},
        channel = %AMQP.Channel{}
      ) do
    case construct_binding(binding, channel) do
      :ok ->
        construct(%{topology | bindings: rest}, channel)

      {:error, reason} ->
        {:error, reason}
    end
  end

  def construct(%P4.Amqp.Topology{}, %AMQP.Channel{}) do
    {:ok, :done}
  end

  defp construct_queue(queue = %Queue{}, channel = %AMQP.Channel{}) do
    AMQP.Queue.declare(channel, queue.name, durable: queue.durable, arguments: queue.arguments)
  end

  defp construct_binding(binding = %Binding{}, channel = %AMQP.Channel{}) do
    opts =
      if is_nil(binding.routing_key) do
        []
      else
        [routing_key: binding.routing_key]
      end

    AMQP.Queue.bind(channel, binding.queue, binding.exchange, opts)
  end

  defp construct_exchange(exchange = %Exchange{}, channel = %AMQP.Channel{}) do
    AMQP.Exchange.declare(channel, exchange.name, exchange.type,
      durable: exchange.durable,
      arguments: exchange.arguments
    )
  end
end
