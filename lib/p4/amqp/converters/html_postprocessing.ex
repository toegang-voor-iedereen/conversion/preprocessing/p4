require OK

defmodule P4.Amqp.FileConverter.HtmlPostProcessingConverter do
  @moduledoc """
  This module implements the `P4.Amqp.FileConverter` behaviour for converting HTML documents,
  delegating the actual conversion to `P4.Html.PostProcessor.process/1`
  """
  @behaviour P4.Amqp.FileConverter
  @name HtmlPostProcessingConverter

  use OK.Pipe
  alias P4.Amqp.Dto.MessageWithFile

  @impl P4.Amqp.FileConverter
  def from() do
    "text/html"
  end

  @impl P4.Amqp.FileConverter
  def to() do
    "text/vnd-nldoc.postprocessed+html"
  end

  @impl P4.Amqp.FileConverter
  def file_infix() do
    "postprocessed"
  end

  @impl P4.Amqp.FileConverter
  def file_extension() do
    "html"
  end

  @impl P4.Amqp.FileConverter
  def convert(%MessageWithFile{message: message, local_file_path: local_path}) do
    P4.Logger.debug(
      "#{@name}: started converting file from message #{message.identifier}",
      "data.converter": @name
    )

    OK.try do
      output_path <-
        P4.Html.Reader.read_from_path(local_path, message.identifier)
        ~>> P4.Html.PostProcessor.process()
        ~>> P4.Html.Writer.write()

      _ <- P4.IO.File.remove(local_path)
    after
      P4.Logger.debug(
        "#{@name}: finished converting file from message #{message.identifier}",
        "data.converter": @name
      )

      {:ok, %MessageWithFile{message: message, local_file_path: output_path}}
    rescue
      reason ->
        P4.IO.File.remove(local_path)
        {:error, reason}
    end
  end
end
