require OK

defmodule P4.Amqp.FileConverter.WordPreprocessingConverter do
  @moduledoc """
  This module implements the `P4.Amqp.FileConverter` behaviour for converting Word documents,
  delegating the actual conversion to `P4.Docx.Processor.process/1`
  """
  @behaviour P4.Amqp.FileConverter
  @name WordPreprocessingConverter

  use OK.Pipe
  alias P4.Amqp.Dto.MessageWithFile

  @impl P4.Amqp.FileConverter
  def from() do
    "application/vnd-nldoc.annotated+openxmlformats-officedocument.wordprocessingml.document"
  end

  @impl P4.Amqp.FileConverter
  def to() do
    "application/vnd-nldoc.preprocessed+openxmlformats-officedocument.wordprocessingml.document"
  end

  @impl P4.Amqp.FileConverter
  def file_infix() do
    "preprocessed"
  end

  @impl P4.Amqp.FileConverter
  def file_extension() do
    "docx"
  end

  @impl P4.Amqp.FileConverter
  def convert(%MessageWithFile{message: message, local_file_path: local_path}) do
    P4.Logger.debug(
      "#{@name}: started converting file from message #{message.identifier}",
      "data.converter": @name
    )

    OK.try do
      output_path <-
        P4.Docx.Reader.read_from_path(local_path, message.identifier)
        ~>> P4.Docx.PreProcessor.process()
        ~>> P4.Docx.Writer.write()

      _ <- P4.IO.File.remove(local_path)
    after
      P4.Logger.debug(
        "#{@name}: finished converting file from message #{message.identifier}",
        "data.converter": @name
      )

      {:ok, %MessageWithFile{message: message, local_file_path: output_path}}
    rescue
      reason ->
        P4.IO.File.remove(local_path)
        {:error, reason}
    end
  end
end
