defmodule P4.Amqp.FileConverter do
  @moduledoc """
  This module defines the behaviour of a file converter.
  A FileConverter converts a file on disk from one mime type to another and saves the converted file to disk.
  """
  alias P4.Amqp.Dto.MessageWithFile

  @doc """
  The mime type of the file format that this converter converts from.
  """
  @callback from() :: String.t()

  @doc """
  The mime type of the file format that this converter converts to.
  """
  @callback to() :: String.t()

  @doc """
  The infix to use in the converted file name.
  E.g. if the file name is `file.docx` and the infix is `annotated`, the converted file will be `p4.annotated.file.docx`.
  """
  @callback file_infix() :: String.t()

  @doc """
  The extension to use in the converted file name.
  E.g. 'docx' or 'html'
  """
  @callback file_extension() :: String.t()

  @doc """
  Convert the file from the `from` mime type to the `to` mime type.
  """
  @callback convert(%MessageWithFile{}) :: {:ok, %MessageWithFile{}} | {:error, Exception.t()}
end
