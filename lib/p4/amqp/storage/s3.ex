defmodule P4.Amqp.Storage.S3 do
  @moduledoc """
  This module implements the P4.Amqp.Storage behaviour for downloading from / uploading to an S3 bucket.
  The bucket name is read from the application environment.
  """
  require OK
  use OK.Pipe

  @behaviour P4.Amqp.Storage

  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Dto.MessageWithFile

  @impl P4.Amqp.Storage
  def download(message = %ConversionMessage{}) do
    key = message.data.content_location
    bucket = Application.get_env(:p4, :s3_bucket)

    P4.Logger.debug("AMQP: downloading #{bucket}/#{key} from S3")

    OK.try do
      local_path <- P4.IO.File.create_temp()

      _ <-
        bucket
        |> ExAws.S3.download_file(key, local_path)
        |> ExAws.request()
    after
      P4.Logger.debug("AMQP: finished downloading #{bucket}/#{key} from S3")
      {:ok, %MessageWithFile{message: message, local_file_path: local_path}}
    rescue
      reason ->
        P4.Logger.error("AMQP: failed to download #{bucket}/#{key} from S3", error: reason)
        P4.Error.S3.Download.wrap({:error, reason}, %{bucket: bucket, key: key})
    end
  end

  @impl P4.Amqp.Storage
  def upload(
        %MessageWithFile{message: message, local_file_path: local_path},
        file_infix,
        file_extension
      ) do
    bucket = Application.get_env(:p4, :s3_bucket)

    output_key =
      P4.Misc.KeyGenerator.generate(24,
        prefix: "/#{message.identifier}/p4.#{file_infix}.",
        suffix: ".#{file_extension}"
      )

    P4.Logger.debug("AMQP: uploading #{bucket}/#{output_key} to S3")

    OK.try do
      _ <-
        local_path
        |> ExAws.S3.Upload.stream_file()
        |> ExAws.S3.upload(bucket, output_key)
        |> ExAws.request()
    after
      P4.Logger.debug("AMQP: finished uploading #{bucket}/#{output_key} to S3")
      {:ok, %{message | data: %{message.data | content_location: output_key}}}
    rescue
      reason ->
        P4.Logger.error("AMQP: failed to upload #{bucket}/#{output_key} to S3", error: reason)

        P4.Error.S3.Upload.wrap({:error, reason}, %{
          bucket: bucket,
          key: output_key,
          local_path: local_path
        })
    end
  end
end
