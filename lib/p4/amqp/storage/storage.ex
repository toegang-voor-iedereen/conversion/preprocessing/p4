defmodule P4.Amqp.Storage do
  @moduledoc """
  This module defines the behaviour for downloading and uploading files from/to storage.
  The download happens before a consumer's consume function is called, and the upload
  happens afterwards.
  """
  @callback download(P4.Amqp.Dto.ConversionMessage.t()) ::
              {:ok, P4.Amqp.Dto.MessageWithFile.t()} | {:error, Exception.t()}
  @callback upload(P4.Amqp.Dto.MessageWithFile.t(), String.t(), String.t()) ::
              {:ok, P4.Amqp.Dto.ConversionMessage.t()} | {:error, Exception.t()}
end
