require OK

defmodule P4.Amqp.Consumer.Behaviour do
  alias P4.Amqp.Dto.MessageWithFile

  @callback queue_name() :: String.t()
  @callback consume(MessageWithFile.t()) :: {:ok, MessageWithFile.t()} | {:error, Exception.t()}
end

defmodule P4.Amqp.Consumer do
  @moduledoc """
  This module defines the behaviour for consuming messages from an AMQP queue.
  To implement this module, you must define the `queue_name/0` and `consume/1` callbacks, e.g.

      defmodule MyConsumer do
        use P4.Amqp.Consumer, converter: MyConverter

        @impl P4.Amqp.Consumer.Behaviour
        def queue_name() do
          "my_queue"
        end

        @impl P4.Amqp.Consumer.Behaviour
        def consume(message) do
          IO.inspect(message)
          {:ok, message}
        end
      end
  """
  defmacro __using__(opts) do
    quote location: :keep do
      use GenServer
      use OK.Pipe
      require OK

      @behaviour P4.Amqp.Consumer.Behaviour
      defoverridable P4.Amqp.Consumer.Behaviour

      @converter unquote(opts[:converter])

      def start_link(args) do
        GenServer.start_link(__MODULE__, args, name: __MODULE__)
      end

      @impl GenServer
      def init(_args) do
        settings = P4.Settings.Amqp.from_config()

        OK.try do
          channel <- AMQP.Application.get_channel()
          _ <- P4.Amqp.ApplicationTopology.construct(channel, settings)
        after
          Kernel.send(self(), :subscribe)
          {:ok, channel}
        rescue
          reason -> {:error, reason}
        end
      end

      @impl GenServer
      def handle_info(:subscribe, state) do
        case subscribe() do
          {:ok, channel} ->
            {:noreply, Map.put(state, :channel, channel)}

          _ ->
            {:noreply, state}
        end
      end

      def handle_info({:DOWN, _, :process, pid, _reason}, %{channel: %{pid: pid}} = state) do
        send(self(), :subscribe)
        {:noreply, Map.put(state, :channel, nil)}
      end

      def handle_info({:basic_consume_ok, %{consumer_tag: _consumer_tag}}, chan) do
        {:noreply, chan}
      end

      def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, chan) do
        {:stop, :normal, chan}
      end

      def handle_info({:basic_cancel_ok, %{consumer_tag: _consumer_tag}}, chan) do
        {:noreply, chan}
      end

      def handle_info({:basic_deliver, payload, meta}, channel) do
        consume(channel, payload, meta)
        {:noreply, channel}
      end

      @spec basic_qos(AMQP.Channel.t()) :: {:ok, AMQP.Channel.t()} | {:error, atom()}
      defp basic_qos(channel = %AMQP.Channel{}) do
        case AMQP.Basic.qos(channel, prefetch_count: 1) do
          :ok ->
            {:ok, channel}

          other ->
            other
        end
      end

      def subscribe() do
        OK.try do
          channel <- AMQP.Application.get_channel()
          Process.monitor(channel.pid)
          _ <- basic_qos(channel)
          _consumer_tag <- AMQP.Basic.consume(channel, queue_name())
        after
          P4.Logger.notice("AMQP: subscribed to queue #{queue_name()}")
          {:ok, channel}
        rescue
          _reason ->
            Process.send_after(self(), :subscribe, 1000)
            {:error, :retrying}
        end
      end

      defp clean_message_for_next_queue(
             message = %P4.Amqp.Dto.ConversionMessage{
               conversion_stack: []
             }
           ),
           do: message

      defp clean_message_for_next_queue(
             message = %P4.Amqp.Dto.ConversionMessage{
               conversion_stack: [_head | tail]
             }
           ),
           do: %P4.Amqp.Dto.ConversionMessage{message | conversion_stack: tail}

      @spec consume(AMQP.Channel.t(), binary(), keyword()) ::
              {:ok, :done} | {:error, Exception.t()}
      def consume(channel, payload, meta) do
        P4.Logger.debug("AMQP: consuming message")

        OK.try do
          next <-
            P4.Amqp.Serialization.Serializer.deserialize(payload)
            ~>> P4.Amqp.Dto.ConversionMessage.validate(@converter.from(), @converter.to())
            ~>> P4.Amqp.Storage.S3.download()
            ~>> consume()
            ~>> P4.Amqp.Storage.S3.upload(@converter.file_infix(), @converter.file_extension())

          _ <-
            clean_message_for_next_queue(next)
            |> ack_and_queue(channel, meta)
        after
          {:ok, :done}
        rescue
          exception ->
            P4.Logger.error("AMQP: failed to consume message", error: exception)

            Sentry.capture_exception(exception,
              extra: %{exception: exception, meta: meta, payload: payload}
            )

            case AMQP.Basic.nack(channel, meta.delivery_tag, requeue: false) do
              :ok ->
                {:ok, :done}

              {:error, reason} ->
                nacking_exception = %P4.Error.Amqp.Nack{reason: reason}

                Sentry.capture_exception(
                  nacking_exception,
                  extra: %{
                    exception: nacking_exception,
                    meta: meta,
                    payload: payload
                  }
                )

                P4.Logger.error("AMQP: failed to nack", error: nacking_exception)

                {:ok, :done}
            end
        end
      end

      @spec ack_and_queue(Amqp.Dto.ConversionMessage.t(), AMQP.Channel.t(), %{
              delivery_tag: AMQP.Basic.delivery_tag()
            }) ::
              {:ok, :done} | {:error, Exception.t()}
      defp ack_and_queue(message, channel, meta) do
        P4.Logger.debug("AMQP: publishing message #{message.identifier}")

        OK.try do
          _ <- P4.Amqp.Publisher.publish(message, channel)
          _ <- ack(channel, meta)
        after
          P4.Logger.debug("AMQP: published message #{message.identifier} and acked previous")
          {:ok, :done}
        rescue
          reason ->
            {:error, reason}
        end
      end

      @spec ack(AMQP.Channel.t(), %{delivery_tag: AMQP.Basic.delivery_tag()}) ::
              {:ok, :done} | {:error, Exception.t()}
      defp ack(channel, meta) do
        channel
        |> AMQP.Basic.ack(meta.delivery_tag)
        |> P4.Error.Amqp.Ack.wrap(meta)
      end
    end
  end
end
