require OK

defmodule P4.Amqp.Consumer.WordPreprocessingConsumer do
  alias P4.Amqp.FileConverter.WordPreprocessingConverter

  use P4.Amqp.Consumer, converter: WordPreprocessingConverter

  @impl P4.Amqp.Consumer.Behaviour
  def queue_name() do
    P4.Settings.Amqp.Queue.from_config().p4_docx
  end

  @impl P4.Amqp.Consumer.Behaviour
  def consume(message) do
    WordPreprocessingConverter.convert(message)
  end
end
