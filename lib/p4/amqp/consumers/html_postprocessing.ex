require OK

defmodule P4.Amqp.Consumer.HtmlPostProcessingConsumer do
  alias P4.Amqp.FileConverter.HtmlPostProcessingConverter

  use P4.Amqp.Consumer, converter: HtmlPostProcessingConverter

  @impl P4.Amqp.Consumer.Behaviour
  def queue_name() do
    P4.Settings.Amqp.Queue.from_config().p4_html_postprocessing
  end

  @impl P4.Amqp.Consumer.Behaviour
  def consume(message) do
    HtmlPostProcessingConverter.convert(message)
  end
end
