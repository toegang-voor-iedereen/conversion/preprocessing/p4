require OK

defmodule P4.Amqp.Consumer.WordHeadingAnnotatingConsumer do
  alias P4.Amqp.FileConverter.WordHeadingAnnotatingConverter

  use P4.Amqp.Consumer, converter: WordHeadingAnnotatingConverter

  @impl P4.Amqp.Consumer.Behaviour
  def queue_name() do
    P4.Settings.Amqp.Queue.from_config().p4_docx_annotator
  end

  @impl P4.Amqp.Consumer.Behaviour
  def consume(message) do
    WordHeadingAnnotatingConverter.convert(message)
  end
end
