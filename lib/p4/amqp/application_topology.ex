defmodule P4.Amqp.ApplicationTopology do
  @moduledoc """
  This module is responsible for declaring the RabbitMQ exchanges,
  queues and bindings that are used by P4.
  """
  alias P4.Amqp.FileConverter.HtmlPostProcessingConverter
  alias P4.Amqp.FileConverter.WordHeadingAnnotatingConverter
  alias P4.Amqp.FileConverter.WordPreprocessingConverter

  @spec construct(Channel.t(), P4.Settings.Amqp.t()) :: {:ok, :done} | {:error, term()}
  def construct(channel = %AMQP.Channel{}, settings = %P4.Settings.Amqp{}) do
    %P4.Amqp.Topology{
      exchanges: [
        %P4.Amqp.Topology.Exchange{
          name: settings.exchange.dlx,
          durable: true,
          type: :fanout
        },
        %P4.Amqp.Topology.Exchange{
          name: settings.exchange.outbound,
          durable: true,
          type: :fanout
        },
        %P4.Amqp.Topology.Exchange{
          name: settings.exchange.conversion,
          durable: true,
          type: :direct,
          # TODO paramaterize
          arguments: [{"alternate-exchange", :longstr, "exchange.unrouted"}]
        }
      ],
      queues: [
        %P4.Amqp.Topology.Queue{
          name: settings.queue.dlx,
          durable: true
        },
        %P4.Amqp.Topology.Queue{
          name: settings.queue.p4_docx,
          durable: true,
          arguments: [
            {"x-dead-letter-exchange", :longstr, settings.exchange.dlx}
          ]
        },
        %P4.Amqp.Topology.Queue{
          name: settings.queue.p4_docx_annotator,
          durable: true,
          arguments: [
            {"x-dead-letter-exchange", :longstr, settings.exchange.dlx}
          ]
        },
        %P4.Amqp.Topology.Queue{
          name: settings.queue.p4_html_postprocessing,
          durable: true,
          arguments: [
            {"x-dead-letter-exchange", :longstr, settings.exchange.dlx}
          ]
        },
        %P4.Amqp.Topology.Queue{
          name: settings.queue.outbound,
          durable: true,
          arguments: [
            {"x-dead-letter-exchange", :longstr, settings.exchange.dlx}
          ]
        }
      ],
      bindings: [
        %P4.Amqp.Topology.Binding{
          queue: settings.queue.dlx,
          exchange: settings.exchange.dlx
        },
        %P4.Amqp.Topology.Binding{
          queue: settings.queue.outbound,
          exchange: settings.exchange.outbound
        },
        %P4.Amqp.Topology.Binding{
          queue: settings.queue.p4_docx,
          exchange: settings.exchange.conversion,
          routing_key:
            Base.encode64(
              "from:#{WordPreprocessingConverter.from()};to:#{WordPreprocessingConverter.to()}"
            )
        },
        %P4.Amqp.Topology.Binding{
          queue: settings.queue.p4_docx_annotator,
          exchange: settings.exchange.conversion,
          routing_key:
            Base.encode64(
              "from:#{WordHeadingAnnotatingConverter.from()};to:#{WordHeadingAnnotatingConverter.to()}"
            )
        },
        %P4.Amqp.Topology.Binding{
          queue: settings.queue.p4_html_postprocessing,
          exchange: settings.exchange.conversion,
          routing_key:
            Base.encode64(
              "from:#{HtmlPostProcessingConverter.from()};to:#{HtmlPostProcessingConverter.to()}"
            )
        }
      ]
    }
    |> P4.Amqp.Topology.construct(channel)
  end
end
