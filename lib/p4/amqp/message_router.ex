defmodule P4.Amqp.MessageRouter do
  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Dto.ConversionTypeMapping

  @spec route(ConversionMessage.t()) :: {:outbound, String.t()} | {:conversion, String.t()}
  def route(%ConversionMessage{conversion_stack: []}),
    do: {:outbound, ""}

  def route(%ConversionMessage{conversion_stack: [%ConversionTypeMapping{from: from, to: to} | _]}),
      do: {:conversion, encode(from, to)}

  def encode(from, to) when is_binary(from) and is_binary(to),
    do: Base.encode64("from:#{from};to:#{to}")
end
