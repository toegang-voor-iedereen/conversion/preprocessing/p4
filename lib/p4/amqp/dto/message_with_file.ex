defmodule P4.Amqp.Dto.MessageWithFile do
  defstruct message: %P4.Amqp.Dto.ConversionMessage{}, local_file_path: nil
end
