defmodule P4.Amqp.Dto.ConversionMessageData do
  use Ecto.Schema

  @primary_key false

  embedded_schema do
    field :content_location, :string

    embeds_many :assets, P4.Amqp.Dto.Asset
  end

  def changeset(struct, params) do
    struct
    |> Ecto.Changeset.cast(params, [:content_location])
    |> Ecto.Changeset.cast_embed(:assets)
    |> Ecto.Changeset.validate_required(:content_location)
  end
end
