defmodule P4.Amqp.Dto.ConversionMessage do
  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Dto.ConversionMessageData
  alias P4.Amqp.Dto.ConversionTypeMapping

  use Ecto.Schema

  @primary_key false

  embedded_schema do
    field(:identifier, :string)

    embeds_one(:mapping, ConversionTypeMapping)
    embeds_one(:data, ConversionMessageData)

    embeds_many(:conversion_stack, ConversionTypeMapping)
  end

  def changeset(struct, params) do
    struct
    |> Ecto.Changeset.cast(params, [:identifier])
    |> Ecto.Changeset.cast_embed(:conversion_stack)
    |> Ecto.Changeset.cast_embed(:mapping, required: true)
    |> Ecto.Changeset.cast_embed(:data, required: true)
    |> Ecto.Changeset.validate_required(:identifier)
  end

  def changeset(params),
    do: changeset(%ConversionMessage{}, params)

  @spec validate(%ConversionMessage{}, String.t(), String.t()) ::
          {:ok, %ConversionMessage{}} | {:error, Exception.t()}
  def validate(
        message = %ConversionMessage{conversion_stack: conversion_stack},
        supported_from,
        supported_to
      ) do
    case validate_conversion_stack(conversion_stack, supported_from, supported_to) do
      {:error, :empty} ->
        {:error, %P4.Error.Amqp.EmptyConversionStack{amqp_message: message}}

      {:error, :unsupported} ->
        {:error, %P4.Error.Amqp.InvalidConversion{amqp_message: message}}

      {:ok, _} ->
        {:ok, message}
    end
  end

  @spec validate_conversion_stack([ConversionTypeMapping.t()], String.t(), String.t()) ::
          {:ok, [ConversionTypeMapping.t()]} | {:error, :empty} | {:error, :unsupported}
  defp validate_conversion_stack([], _supported_from, _supported_to),
    do: {:error, :empty}

  defp validate_conversion_stack(
         stack = [%ConversionTypeMapping{} = mapping | _],
         supported_from,
         supported_to
       ) do
    case ConversionTypeMapping.validate(mapping, supported_from, supported_to) do
      {:ok, _} -> {:ok, stack}
      error -> error
    end
  end
end
