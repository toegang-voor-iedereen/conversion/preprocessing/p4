defmodule P4.Amqp.Dto.Asset do
  use Ecto.Schema

  @primary_key false

  @fields [:filename, :content_location]

  embedded_schema do
    field :filename, :string
    field :content_location, :string
  end

  def changeset(struct, params) do
    struct
    |> Ecto.Changeset.cast(params, @fields)
    |> Ecto.Changeset.validate_required(@fields)
  end
end
