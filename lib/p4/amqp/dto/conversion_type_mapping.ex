defmodule P4.Amqp.Dto.ConversionTypeMapping do
  alias P4.Amqp.Dto.ConversionTypeMapping
  use Ecto.Schema

  @primary_key false

  @fields [:from, :to]

  embedded_schema do
    field(:from, :string)
    field(:to, :string)
  end

  def changeset(struct, params) do
    struct
    |> Ecto.Changeset.cast(params, @fields)
    |> Ecto.Changeset.validate_required(@fields)
  end

  @spec validate(%ConversionTypeMapping{}, String.t(), String.t()) ::
          {:ok, %ConversionTypeMapping{}} | {:error, :unsupported}
  def validate(
        mapping = %P4.Amqp.Dto.ConversionTypeMapping{from: from, to: to},
        supported_from,
        supported_to
      ) do
    if from == supported_from && to == supported_to do
      {:ok, mapping}
    else
      {:error, :unsupported}
    end
  end
end
