defmodule P4.Amqp.Publisher do
  use OK.Pipe

  alias P4.Amqp.Dto.ConversionMessage
  alias P4.Amqp.Serialization.Serializer
  alias P4.Amqp.MessageRouter

  @spec publish(ConversionMessage.t(), AMQP.Channel.t()) :: {:ok, :done} | {:error, Exception.t()}
  def publish(message = %ConversionMessage{}, channel = %AMQP.Channel{}) do
    Serializer.serialize(message) ~>> basic_publish(channel, next_queue(message))
  end

  @spec basic_publish(String.t(), AMQP.Channel.t(), {String.t(), String.t()}) ::
          {:ok, :done} | {:error, Exception.t()}
  defp basic_publish(payload, channel = %AMQP.Channel{}, {exchange_name, routing_key})
       when is_binary(exchange_name) and is_binary(routing_key) and is_binary(payload) do
    AMQP.Basic.publish(channel, exchange_name, routing_key, payload)
    |> P4.Error.Amqp.Publish.wrap(%{
      exchange_name: exchange_name,
      routing_key: routing_key,
      payload: payload
    })
  end

  @spec next_queue(ConversionMessage.t()) :: {String.t(), String.t()}
  defp next_queue(msg = %ConversionMessage{}) do
    case MessageRouter.route(msg) do
      {:conversion, routing_key} ->
        {P4.Settings.Amqp.Exchange.from_config().conversion, routing_key}

      {:outbound, routing_key} ->
        {P4.Settings.Amqp.Exchange.from_config().outbound, routing_key}
    end
  end
end
