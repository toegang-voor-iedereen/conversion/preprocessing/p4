defmodule P4.Style.StyleMutator do
  require P4.Xml.Helper
  require P4.Style.StyleMerger
  require P4.Docx.Parser.Styles
  require P4.Docx.Parser.Style

  def merge_document_styles(sheet = {"w:styles", _attr, children}) do
    merge_document_styles(
      children,
      %{},
      sheet,
      P4.Docx.Parser.Styles.get_document_defaults(sheet)
    )
  end

  def merge_document_styles(
        [style = {"w:style", _, _} | tail],
        style_map,
        sheet = {"w:styles", _, _},
        defaults
      ) do
    style_id = P4.Xml.Helper.get_attribute_value(style, "w:styleId")

    if Map.has_key?(style_map, style_id) do
      merge_document_styles(tail, style_map, sheet, defaults)
    else
      merge_document_styles(
        tail,
        merge_document_style(style, style_map, sheet, defaults),
        sheet,
        defaults
      )
    end
  end

  def merge_document_styles(
        [_ | tail],
        style_map,
        sheet = {"w:styles", _, _},
        defaults
      ),
      do: merge_document_styles(tail, style_map, sheet, defaults)

  def merge_document_styles([], style_map, {"w:styles", _, _}, _defaults),
    do: style_map

  defp merge_document_style(
         style = {"w:style", _, _},
         style_map,
         sheet = {"w:styles", _, _},
         defaults
       ) do
    based_on_style_id = P4.Docx.Parser.Style.get_based_on_value(style)
    style_id = P4.Xml.Helper.get_attribute_value(style, "w:styleId")
    base_style = Map.get(style_map, based_on_style_id)

    if is_nil(base_style) do
      style_from_sheet = P4.Docx.Parser.Styles.get_style_by_style_id(sheet, based_on_style_id)

      # If we have no basedOn found, which should never occur, just save the style..
      if is_nil(style_from_sheet) do
        Map.put(style_map, style_id, merge_style_with_defaults(style, defaults))
      else
        merge_document_style(
          style,
          merge_document_style(style_from_sheet, style_map, sheet, defaults),
          sheet,
          defaults
        )
      end
    else
      Map.put(
        style_map,
        style_id,
        P4.Style.StyleMerger.merge(base_style, style)
      )
    end
  end

  def merge_style_with_defaults({"w:style", attr, children}, defaults = {"w:docDefaults", _, _}) do
    default_run_properties =
      defaults
      |> P4.Xml.Helper.get_child_by_name("w:rPrDefault")
      |> P4.Xml.Helper.get_child_by_name("w:rPr", {"w:rPr", [], []})

    default_paragraph_properties =
      defaults
      |> P4.Xml.Helper.get_child_by_name("w:pPrDefault")
      |> P4.Xml.Helper.get_child_by_name("w:pPr", {"w:pPr", [], []})

    P4.Style.StyleMerger.merge(
      {
        "w:style",
        [],
        [
          # If no specific default text-color is given, we assume it's black.
          P4.Style.StyleMerger.merge(
            {"w:rPr", [], [{"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []}]},
            default_run_properties
          ),
          default_paragraph_properties
        ]
      },
      {"w:style", attr, children}
    )
  end

  def merge_style_with_defaults(style = {"w:style", _, _}, _), do: style
end
