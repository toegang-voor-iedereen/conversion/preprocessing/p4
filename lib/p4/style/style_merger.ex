defmodule P4.Style.StyleMerger do
  @moduledoc """
  Provides functionality for merging style elements in XML-based document formats like Office Open XML (OOXML).
  This module focuses on ensuring consistency and adherence to formatting rules by merging various style attributes
  and properties, which is essential for handling styles in document formatting.

  It offers functions tailored for specific XML elements, enabling precise and context-aware style merging.
  """

  require P4.Xml.Helper

  @doc """
  Merges two elements with the same type, such as 'w:style', 'w:rPr', 'w:rFonts', from a DOCX styles.xml file. This
  function is designed to handle the merging of style definitions, ensuring that the style attributes and children
  elements from the right element take precedence over those in the left.

  ## Parameters

    - `left`: The base element
    - `right`: The element whose style attributes and children will be merged into the base.

  ## Returns

  A tuple representing the merged 'w:style' element. This element combines attributes and children from both input
  elements, with precedence given to the 'right' element.

  ## Notes

    - This function is crucial for achieving consistent and accurate font styling in XML document processing.
    - It follows the merging rules defined in the ECMA-376 standard, ensuring compatibility with Office Open XML
      (OOXML) formats.
    - The function handles the complexities of merging font themes and specific font attributes, ensuring that the
      resulting merged properties are correctly formatted and adhere to the defined precedence.

  ## Examples

  Merging two 'w:style' elements:

      iex> P4.Style.StyleMerger.merge(
      ...>   {"w:style", [], [
      ...>     {"w:name", [{"w:val", "BaseStyle"}], []}, {"w:rPr", [], [
      ...>       {"w:color", [{"w:val", "Red"}], []}, {"w:bold", [], []}]}]},
      ...>   {"w:style", [], [
      ...>     {"w:name", [{"w:val", "MergedStyle"}], []}, {"w:rPr", [], [
      ...>       {"w:color", [{"w:val", "Blue"}], []}, {"w:i", [], []}]}]})
      {"w:style", [], [
        {"w:name", [{"w:val", "MergedStyle"}], []},
        {"w:rPr", [], [
          {"w:bold", [], []},
          {"w:color", [{"w:val", "Blue"}], []},
          {"w:i", [], []}]}]}

  In this example, the 'w:style' attributes and children from the second element ('right') override those in the first
  element ('left'), resulting in a merged style definition.

  Merging two 'w:rPr' elements:

      iex> P4.Style.StyleMerger.merge(
      ...>   {"w:rPr", [], [{"w:b", [{"w:val", "0"}], []}, {"w:i", [{"w:val", "true"}], []}]},
      ...>   {"w:rPr", [], [{"w:b", [], []}]})
      {"w:rPr", [], [{"w:b", [], []}, {"w:i", [{"w:val", "true"}], []}]}

  Merging font properties from two different sources. In this example, 'w:rFonts' attributes from the second element
  ('style_font_properties') override those in the first element ('base_font_properties') where applicable. Attributes
  not present in the second element are retained from the first.

      iex> P4.Style.StyleMerger.merge(
      ...>   {"w:rFonts", [
      ...>     {"w:cstheme", "Foo"},
      ...>     {"w:ascii", "XFont"},
      ...>     {"w:cs", "CSFont"},], []},
      ...>   {"w:rFonts", [
      ...>     {"w:cs", "QFont"},
      ...>     {"w:cstheme", "YFont"},
      ...>     {"w:hAnsi", "hFont"}], []})
      {"w:rFonts", [{"w:hAnsi", "hFont"}, {"w:ascii", "XFont"}, {"w:cstheme", "YFont"}], []}

  This should never occur, but if it does, we make sure the application does not crash.

      iex> P4.Style.StyleMerger.merge({"w:rFonts", [], []}, {"w:rFonts", [], []})
      {"w:rFonts", [], []}

  With a color on one side but not on the other, take the color.

      iex> P4.Style.StyleMerger.merge({"w:color", [{"w:val", "000000"}], []}, nil)
      {"w:color", [{"w:val", "000000"}], []}
      iex> P4.Style.StyleMerger.merge(nil, {"w:color", [{"w:val", "000000"}], []})
      {"w:color", [{"w:val", "000000"}], []}
  """
  def merge(
        {"w:style", _, left_children},
        {"w:style", right_attr, right_children}
      ),
      do: {"w:style", right_attr, merge(left_children, right_children)}

  def merge(nil, right = {"w:color", _, _}), do: right
  def merge(left = {"w:color", _, _}, nil), do: left
  def merge({"w:color", _, _}, right = {"w:color", _, _}), do: right

  def merge({"w:rFonts", base_attr, _}, {"w:rFonts", style_attr, style_children}) do
    {
      "w:rFonts",
      Enum.reduce(
        [
          {"w:cstheme", "w:cs"},
          {"w:asciiTheme", "w:ascii"},
          {"w:hAnsiTheme", "w:hAnsi"},
          {"w:eastAsiaTheme", "w:eastAsia"}
        ],
        [],
        fn {theme_attr_key, specific_attr_key}, acc ->
          attr =
            merge_run_font_theme_and_specific(
              P4.Xml.Helper.get_attr(base_attr, theme_attr_key),
              P4.Xml.Helper.get_attr(base_attr, specific_attr_key),
              P4.Xml.Helper.get_attr(style_attr, theme_attr_key),
              P4.Xml.Helper.get_attr(style_attr, specific_attr_key)
            )

          if is_nil(attr) do
            acc
          else
            [attr | acc]
          end
        end
      ),
      style_children
    }
  end

  def merge(left, right) when is_list(left) and is_list(right) do
    left_map = Enum.reduce(
      left,
      %{},
      fn
        el = {name, _, _}, element_map ->
          Map.put(element_map, name, el)
        _, element_map ->
          element_map
      end
    )

    Enum.reduce(
      right,
      left_map,
      fn
        el = {name, _, _}, element_map  ->
          existing_value = Map.get(element_map, name)

          if is_nil(existing_value) do
            Map.put(element_map, name, el)
          else
            Map.put(element_map, name, merge(existing_value, el))
          end

        _, element_map ->
          element_map
      end
    )
    |> Map.values()
  end

  def merge({name, left_attr, base_children}, {name, right_attr, element_children}) do
    merged_attr = P4.Xml.Helper.merge_attributes(left_attr, right_attr)

    # If the right does not have a 'w:val' but the left does, we don't want it, as a missing 'w:val' attributes
    # indicates it's value basically being 'true'.
    if P4.Xml.Helper.has_attr?(left_attr, "w:val") and not P4.Xml.Helper.has_attr?(right_attr, "w:val") do
      {
        name,
        P4.Xml.Helper.remove_attr_by_name(merged_attr, "w:val"),
        merge(base_children, element_children)
      }
    else
      {name, merged_attr, merge(base_children, element_children)}
    end
  end

  @doc """
  Merges theme and specific font attributes from different 'w:rFonts' elements in XML styles, following specific
  rules for precedence as defined in the OOXML standard. This function plays a crucial role in determining how font
  themes (like 'w:cstheme') and specific font attributes (like 'w:cs') are combined when merging two sets of 'w:rFonts'
  attributes.

  ## Parameters

    - `base_theme_attr`: A tuple representing the theme attribute (e.g., 'w:cstheme') from the base 'w:rFonts' element.
    - `base_specific_attr`: A tuple representing a specific font attribute (e.g., 'w:cs') from the base 'w:rFonts'
      element.
    - `style_theme_attr`: A tuple representing the theme attribute from the 'w:rFonts' element to be merged.
    - `style_specific_attr`: A tuple representing a specific font attribute from the 'w:rFonts' element to be merged.

  ## Returns

  A tuple representing the merged attribute. The function prioritizes theme attributes over specific attributes. If a
  theme attribute is present in the style attributes, it takes precedence; otherwise, the specific attribute is used.

  ## Notes

    - This function is integral to achieving accurate and consistent font styling in XML document formatting.
    - It adheres to the rules specified in §17.3.2.26 - rFonts (Run Fonts) of ECMA-376 Part 1, ensuring compatibility
      with the Office Open XML (OOXML) standard.
    - Understanding the precedence of theme over specific attributes is key to utilizing this function effectively in
      style merging scenarios.

  ## Examples

  Merging font theme and specific attributes:

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific(
      ...>   {"w:cstheme", "foo"}, {"w:cs", "bar"}, {"w:cstheme", "lorem"}, nil)
      {"w:cstheme", "lorem"}

  When all available, choose highest level theme font.

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific(
      ...>   {"w:cstheme", "foo"}, {"w:cs", "bar"}, {"w:cstheme", "lorem"}, {"w:cs", "ipsum"})
      {"w:cstheme", "lorem"}

  Higher level over lower level:

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific(
      ...>   {"w:cstheme", "foo"}, {"w:cs", "bar"}, nil, {"w:cs", "ipsum"})
      {"w:cs", "ipsum"}

  Theme font over specific font:

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific({"w:cstheme", "foo"}, {"w:cs", "bar"}, nil, nil)
      {"w:cstheme", "foo"}

  Only available attribute:

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific(nil, {"w:cs", "bar"}, nil, nil)
      {"w:cs", "bar"}

  Merging all nil's will return nil.

      iex> P4.Style.StyleMerger.merge_run_font_theme_and_specific(nil, nil, nil, nil)
      nil

  In these examples, the function correctly applies precedence rules to determine the resulting attribute,
  considering both theme and specific font attributes from the base and style 'w:rFonts' elements.
  """
  def merge_run_font_theme_and_specific(_, _, attr_style_theme, _)
      when is_tuple(attr_style_theme),
      do: attr_style_theme

  def merge_run_font_theme_and_specific(_, _, _, attr_style_specific)
      when is_tuple(attr_style_specific),
      do: attr_style_specific

  def merge_run_font_theme_and_specific(attr_base_theme, _, _, _)
      when is_tuple(attr_base_theme),
      do: attr_base_theme

  def merge_run_font_theme_and_specific(_, attr_base_specific, _, _)
      when is_tuple(attr_base_specific),
      do: attr_base_specific

  def merge_run_font_theme_and_specific(nil, nil, nil, nil), do: nil
end
