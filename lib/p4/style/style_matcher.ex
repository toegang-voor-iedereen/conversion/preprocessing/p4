defmodule P4.Style.StyleMatcher do
  require P4.Style.StyleMerger

  @relevant_run_properties [
    "w:color",
    "w:sz",
    "w:rFonts",
    "w:b",
    "w:i",
    "w:strike",
    "w:u",
    "w:vertAlign"
  ]

  @relevant_paragraph_properties [
    "w:shd"
    # "w:outlineLvl"
  ]

  @doc """
  Modify style with matching one.
  """
  def modify_style_properties(paragraph = {"w:p", _, _}, %P4.Docx.DocumentContext{} = context) do
    paragraph_style_id = P4.Document.Parser.get_style_id(paragraph, "Normal")

    merged_paragraph_run_properties =
      merge_paragraph_run_properties_with_style(paragraph, context)

    merged_paragraph_properties = merge_paragraph_properties_with_style(paragraph, context)

    potential_new_style_ids = Enum.filter(
      [context.specific_style_ids.normal | context.specific_style_ids.heading],
      fn style_id ->
        style = Map.get(context.style_map, style_id)

        style_run_properties = get_run_properties(style)
        style_paragraph_properties = get_paragraph_properties(style)

        paragraph_properties_matches?(
          merged_paragraph_properties,
          style_paragraph_properties
        ) and
          run_properties_matches?(merged_paragraph_run_properties, style_run_properties)
      end
    )

    if Enum.member?(potential_new_style_ids, paragraph_style_id) or Enum.empty?(potential_new_style_ids) do
      paragraph
    else
      new_style_id = Enum.at(potential_new_style_ids, 0)

      paragraph
      |> P4.Document.Parser.set_style_id(new_style_id)
    end
  end

  def merge_paragraph_properties_with_style(
        paragraph = {"w:p", _, _},
        context = %P4.Docx.DocumentContext{}
      ) do
    style_id = P4.Document.Parser.get_style_id(paragraph, context.specific_style_ids.normal)

    context.style_map
    |> Map.get(style_id, {"w:style", [], []})
    |> get_paragraph_properties()
    |> P4.Style.StyleMerger.merge(get_paragraph_properties(paragraph))
  end

  def merge_paragraph_run_properties_with_style(
        paragraph = {"w:p", _, _},
        context = %P4.Docx.DocumentContext{}
      ) do
    style_id = P4.Document.Parser.get_style_id(paragraph, context.specific_style_ids.normal)

    context.style_map
    |> Map.get(style_id, {"w:style", [], []})
    |> get_run_properties()
    |> P4.Style.StyleMerger.merge(get_run_properties(paragraph))
  end

  @doc """
  ## Examples

      iex> P4.Style.StyleMatcher.style_matches?({"w:style", [], []}, {"w:style", [], []})
      true

      iex> P4.Style.StyleMatcher.style_matches?(
      ...>   {"w:style", [], [{"w:rPr", [], [{"w:b", [{"w:val", "0"}], []}, {"w:i", [{"w:val", "true"}], []}]}]},
      ...>   {"w:style", [], [{"w:rPr", [], [{"w:i", [], []}]}]}
      ...> )
      true

      iex> P4.Style.StyleMatcher.style_matches?(
      ...>   {"w:style", [], [
      ...>     {"w:rPr", [], [{"w:b", [{"w:val", "0"}], []}, {"w:i", [{"w:val", "true"}], []}, {"w:u", [], []}]}]},
      ...>   {"w:style", [], [{"w:rPr", [], [{"w:i", [], []}]}]}
      ...> )
      false
  """
  def style_matches?(left = {"w:style", _, _}, right = {"w:style", _, _}) do
    run_properties_matches?(get_run_properties(left), get_run_properties(right))
  end

  def get_run_properties(element = {"w:p", _, _}) do
    element
    |> P4.Xml.Helper.get_child_by_name("w:pPr")
    |> get_run_properties()
  end

  def get_run_properties(element) do
    P4.Xml.Helper.get_child_by_name(element, "w:rPr", {"w:rPr", [], []})
  end

  def get_paragraph_properties(element) do
    P4.Xml.Helper.get_child_by_name(element, "w:pPr", {"w:pPr", [], []})
  end

  @doc """
  Method to check if specific run properties match with others on a fixed set of
  relevant stylistic properties

  ## Examples

      iex> P4.Style.StyleMatcher.run_properties_matches?(
      ...>   {"w:rPr", [], [{"w:b", [{"w:val", "0"}], []}, {"w:i", [{"w:val", "true"}], []}]},
      ...>   {"w:rPr", [], [{"w:i", [], []}]}
      ...> )
      true

      iex> P4.Style.StyleMatcher.run_properties_matches?(
      ...>   {"w:rPr", [], [{"w:b", [{"w:val", "0"}], []}, {"w:i", [{"w:val", "true"}], []}, {"w:u", [], []}]},
      ...>   {"w:rPr", [], [{"w:i", [], []}]}
      ...> )
      false
  """
  def run_properties_matches?(left = {"w:rPr", _, _}, right = {"w:rPr", _, _}) do
    Enum.reduce(
      @relevant_run_properties,
      true,
      fn
        property_name, true ->
          left_prop = P4.Xml.Helper.get_child_by_name(left, property_name)
          right_prop = P4.Xml.Helper.get_child_by_name(right, property_name)

          run_property_matches?(left_prop, right_prop)

        _, false ->
          false
      end
    )
  end

  def paragraph_properties_matches?(left = {"w:pPr", _, _}, right = {"w:pPr", _, _}) do
    Enum.reduce(
      @relevant_paragraph_properties,
      true,
      fn
        property_name, true ->
          left_prop = P4.Xml.Helper.get_child_by_name(left, property_name)
          right_prop = P4.Xml.Helper.get_child_by_name(right, property_name)

          paragraph_property_matches?(left_prop, right_prop)

        _, false ->
          false
      end
    )
  end

  @doc """
  Check equivalence on specific run property.

  ## Examples

  The default value when an element is available is always 'true'.

      iex> P4.Style.StyleMatcher.run_property_matches?({"w:b", [], []}, nil)
      false

  If an element is available but the value is falsy, then it should be regarded to not exist.

      iex> P4.Style.StyleMatcher.run_property_matches?({"w:b", [{"w:val", "0"}], []}, nil)
      true

  The logic for themes/fonts is a bit different, themes get precedence

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:asciiTheme", "XYZ"}, {"w:ascii", "abc"}], []},
      ...>   {"w:rFonts", [{"w:asciiTheme", "XYZ"}, {"w:ascii", "123"}], []}
      ...> )
      true

  For example, if the themes don't match, but the specific fonts do, it's no match!

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:asciiTheme", "ABC"}, {"w:ascii", "123"}], []},
      ...>   {"w:rFonts", [{"w:asciiTheme", "XYZ"}, {"w:ascii", "123"}], []}
      ...> )
      false

  If they're both minor, they're both probably the same.

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:asciiTheme", "majorAscii"}, {"w:ascii", "123"}], []},
      ...>   {"w:rFonts", [{"w:asciiTheme", "majorHAnsi"}, {"w:ascii", "222"}], []}
      ...> )
      true

  Same goes for minor.

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:asciiTheme", "minorAscii"}, {"w:ascii", "123"}], []},
      ...>   {"w:rFonts", [{"w:asciiTheme", "minorHAnsi"}, {"w:ascii", "222"}], []}
      ...> )
      true

  Further more, we also match on the specific ascii font matching:

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:ascii", "123"}], []}, {"w:rFonts", [{"w:ascii", "123"}], []})
      true

  And when they don't match:

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:rFonts", [{"w:ascii", "123"}], []}, {"w:rFonts", [{"w:ascii", "222"}], []})
      false

  And well, if we don't have a specific font, currently we say we couldn't match.

      iex> P4.Style.StyleMatcher.run_property_matches?(nil, {"w:rFonts", [{"w:asciiTheme", "minorAscii"}], []})
      false

  And also on the other side:

      iex> P4.Style.StyleMatcher.run_property_matches?({"w:rFonts", [{"w:asciiTheme", "minorAscii"}], []}, nil)
      false

  It also compares other attributes than 'w:val', w:color is a bit complex; we chose to only match 'w:val' as Microsoft
  Word's implementation of 'w:themeShade' seems to differ from the ECMA-376 standard and we don't know how.

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []},
      ...>   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "other_theme"}], []})
      true

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:color", [{"w:val", "FFFFFF"}, {"w:themeColor", "text1"}], []},
      ...>   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []})
      false

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:color", [{"w:val", "000000"}], []},
      ...>   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []})
      true

      iex> P4.Style.StyleMatcher.run_property_matches?(
      ...>   {"w:color", [{"w:val", "000000"}], []},
      ...>   {"w:color", [{"w:val", "000000"}, {"w:themeColor", "text1"}], []})
      true

      iex> P4.Style.StyleMatcher.run_property_matches?({"w:color", [{"w:val", "000000"}], []}, nil)
      true

      iex> P4.Style.StyleMatcher.run_property_matches?(nil, {"w:color", [{"w:val", "FFFFFF"}], []})
      false

  """
  def run_property_matches?(left = {"w:rFonts", _, _}, right = {"w:rFonts", _, _}) do
    cond do
      font_is_major?(left) and font_is_major?(right) ->
        true

      font_is_minor?(left) and font_is_minor?(right) ->
        true

      true ->
        left_ascii_theme = P4.Xml.Helper.get_attribute_value(left, "w:asciiTheme")
        right_ascii_theme = P4.Xml.Helper.get_attribute_value(right, "w:asciiTheme")

        left_ascii = P4.Xml.Helper.get_attribute_value(left, "w:ascii")
        right_ascii = P4.Xml.Helper.get_attribute_value(right, "w:ascii")

        if left_ascii_theme || right_ascii_theme do
          left_ascii_theme === right_ascii_theme
        else
          left_ascii === right_ascii
        end
    end
  end

  # We chose to only match 'w:val' as Microsoft Word's implementation of 'w:themeShade' seems to differ from the
  # ECMA-376 standard and we don't know how.
  def run_property_matches?(left = {"w:color", _, _}, right = {"w:color", _, _}),
    do: get_property_value(left) === get_property_value(right)

  # When we don't have a color, we assume the other color is just black.
  def run_property_matches?(nil, right = {"w:color", _, _}),
    do: run_property_matches?({"w:color", [{"w:val", "000000"}], []}, right)

  # When we don't have a color, we assume the other color is just black.
  def run_property_matches?(left = {"w:color", _, _}, nil),
    do: run_property_matches?(left, {"w:color", [{"w:val", "000000"}], []})

  def run_property_matches?(nil, {"w:rFonts", _, _}), do: false
  def run_property_matches?({"w:rFonts", _, _}, nil), do: false

  def run_property_matches?(left, right), do: property_matches?(left, right)

  @doc """
  ## Examples

      iex> P4.Style.StyleMatcher.paragraph_property_matches?(
      ...>   {"w:shd", [
      ...>     {"w:val", "clear"}, {"w:color", "auto"}, {"w:fill", "DEEAF6"},
      ...>     {"w:themeFill", "accent1"}, {"w:themeFillTint", "33"}], []},
      ...>   {"w:shd", [
      ...>     {"w:color", "auto"}, {"w:val", "clear"}, {"w:fill", "DEEAF6"},
      ...>     {"w:themeFillTint", "33"}, {"w:themeFill", "accent1"}], []})
      true

      iex> P4.Style.StyleMatcher.paragraph_property_matches?(
      ...>   {"w:shd", [
      ...>     {"w:val", "clear"}, {"w:color", "auto"}, {"w:fill", "DEEAF6"},
      ...>     {"w:themeFill", "accent1", "w:themeFillTint": "33"}], []},
      ...>   {"w:shd", [{"w:color", "auto"}, {"w:val", "clear"}, {"w:fill", "DEEAF6"}]})
      false
  """
  def paragraph_property_matches?(left, right), do: property_matches?(left, right)

  def property_matches?({_, left_attr, _}, {_, right_attr, _}),
    do: property_attributes_match?(left_attr, right_attr)

  # When right or left side is empty, we can regard it as empty side having the
  # property but with 'w:val' being false.
  def property_matches?(left = {name, _, _}, nil),
    do: property_matches?(left, {name, [{"w:val", "false"}], []})

  # Same here.
  def property_matches?(nil, right = {name, _, _}),
    do: property_matches?({name, [{"w:val", "false"}], []}, right)

  def property_matches?(left, right), do: left === right

  @doc """
      iex> P4.Style.StyleMatcher.property_attributes_match?(
      ...>   [{"w:val", "false"}, {"w:other", "value"}],
      ...>   [{"w:val", "0"}, {"w:other", "value"}])
      true

      iex> P4.Style.StyleMatcher.property_attributes_match?([], [])
      true

      iex> P4.Style.StyleMatcher.property_attributes_match?(
      ...>   [{"w:val", "some"}, {"w:other", "value"}],
      ...>   [{"w:val", "some"}, {"w:other", "value"}])
      true

      iex> P4.Style.StyleMatcher.property_attributes_match?(
      ...>   [{"w:val", "some"}, {"w:other", "value"}],
      ...>   [{"w:val", "some"}])
      false
  """
  def property_attributes_match?(left_attr, right_attr)
      when is_list(left_attr) and is_list(right_attr) do
    # w:val functions as default here, for attributes like 'w:b'.
    # When there is no w:val available, it evaluates to true automatically.
    attribute_list_to_map([{"w:val", "true"} | left_attr]) ===
      attribute_list_to_map([{"w:val", "true"} | right_attr])
  end

  @doc """
      iex> P4.Style.StyleMatcher.attribute_list_to_map([{"w:val", "0"}, {"w:other", "lol"}])
      %{"w:val" => false, "w:other" => "lol"}
  """
  def attribute_list_to_map(attr) when is_list(attr) do
    Enum.reduce(
      attr,
      %{},
      fn
        {"w:val", value}, attr_map ->
          Map.put(attr_map, "w:val", translate_property_value(value))

        {key, value}, attr_map ->
          Map.put(attr_map, key, value)
      end
    )
  end

  defp font_is_major?(font = {"w:rFonts", _, _}) do
    element_attribute_value_starts_with?(font, "w:asciiTheme", "major")
  end

  defp font_is_minor?(font = {"w:rFonts", _, _}) do
    element_attribute_value_starts_with?(font, "w:asciiTheme", "minor")
  end

  defp element_attribute_value_starts_with?(element = {_, _, _}, key, value)
       when is_binary(key) and is_binary(value) do
    element
    |> P4.Xml.Helper.get_attribute_value(key)
    |> P4.Std.default("")
    |> String.starts_with?(value)
  end

  @doc """
  ## Examples

      iex> P4.Style.StyleMatcher.get_property_value(nil)
      false

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [], []})
      true

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "true"}], []})
      true

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "0"}], []})
      false

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "1"}], []})
      true

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "none"}], []})
      false

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "false"}], []})
      false

      iex> P4.Style.StyleMatcher.get_property_value({"w:b", [{"w:val", "0"}], []})
      false

      iex> P4.Style.StyleMatcher.get_property_value({"w:meaningOfLife", [{"w:val", "Nothing"}], []})
      "Nothing"
  """
  def get_property_value({_, attr, _}), do: get_property_value(attr)

  def get_property_value(attr) when is_list(attr) do
    attr
    |> P4.Xml.Helper.get_attribute_value("w:val")
    |> translate_property_value()
  end

  def get_property_value(nil), do: false

  def translate_property_value(value) do
    case value do
      nil -> true
      "true" -> true
      "1" -> true
      "false" -> false
      "0" -> false
      "none" -> false
      literal_value -> literal_value
    end
  end
end
