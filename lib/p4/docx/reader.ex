require OK

defmodule P4.Docx.Reader do
  @external_resource Path.join(__DIR__, "resources/empty-comments.xml")
  @external_resource Path.join(__DIR__, "resources/default-styles.xml")

  @spec read_from_path(String.t(), String.t()) ::
          {:ok, %P4.Docx.Document{}} | {:error, Exception.t()}
  def read_from_path(path, id) when is_binary(path) and is_binary(id) do
    OK.try do
      temporary_directory <-
        Temp.mkdir()
        |> P4.Error.P4.Docx.Reader.CreateTempDir.wrap(%{})

      file_list <-
        extract(path, temporary_directory)

      document_content <- read_extracted(temporary_directory, "word/document.xml")

      # if styles.xml is missing, use the default styles.xml from the resources
      styles_content <-
        case read_extracted(temporary_directory, "word/styles.xml") do
          {:error, %{reason: :enoent}} -> read_extracted(__DIR__, "resources/default-styles.xml")
          other -> other
        end

      # if comments.xml is missing, use the empty comments.xml from the resources
      comments_content <-
        case read_extracted(temporary_directory, "word/comments.xml") do
          {:error, %{reason: :enoent}} -> read_extracted(__DIR__, "resources/empty-comments.xml")
          other -> other
        end

      document_xml <- parse_xml(document_content, "word/document.xml")
      styles_xml <- parse_xml(styles_content, "word/styles.xml")
      comments_xml <- parse_xml(comments_content, "word/comments.xml")
    after
      %P4.Docx.Document{
        id: id,
        file_list:
          if not Enum.member?(file_list, "word/comments.xml") do
            ["word/comments.xml" | file_list]
          else
            file_list
          end,
        extraction_path: temporary_directory,
        document: document_xml,
        comments: comments_xml,
        styles: styles_xml
      }
      |> OK.success()
    rescue
      reason -> {:error, reason}
    end
  end

  @spec parse_xml(String.t(), String.t()) :: {:ok, tuple()} | {:error, Exception.t()}
  def parse_xml(content, path) when is_binary(path) and is_binary(content) do
    content
    |> Saxy.SimpleForm.parse_string(expand_entity: :never)
    |> P4.Error.P4.Docx.Reader.ParsingXml.wrap(path)
  end

  @spec read_extracted(String.t(), String.t()) :: {:ok, String.t()} | {:error, Exception.t()}
  defp read_extracted(temporary_directory, path)
       when is_binary(temporary_directory) and is_binary(path) do
    temporary_directory
    |> Path.join(path)
    |> File.stream!([:trim_bom, encoding: :utf8])
    |> Enum.join()
    |> OK.success()
  rescue
    # reason will be an instance of File.Error
    reason -> {:error, reason}
  end

  @spec extract(String.t(), String.t()) :: {:ok, [String.t()]} | {:error, Exception.t()}
  defp extract(path_to_zip, destination_path)
       when is_binary(path_to_zip) and is_binary(destination_path) do
    case P4.IO.Zip.extract(path_to_zip, destination_path) do
      {:ok, file_list} ->
        {:ok, Enum.map(file_list, fn x -> Path.relative_to(x, destination_path) end)}

      result = {:error, _} ->
        result
    end
  end
end
