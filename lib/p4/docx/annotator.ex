defmodule P4.Docx.Annotator do
  @moduledoc """
  This module implements functions for annotatating elements in Word files with UUIDs,
  so that we can track their conversion throughout P4, Pandoc and the ESC/RE.

  Currently, this is done for:
  - all text elements, i.e. all paragraphs, i.e. all `w:p`s. This includes headings, regular text paragraphs and (nested) list items.
  - all structured document tags, i.e. all `w:sdt`s. This includes TOCs, TOFs and other structured content.

  Will later be extended to images and tables as well.
  """

  defmodule State do
    @type t :: %__MODULE__{comments: [Saxy.XML.element()], comment_id: integer()}
    defstruct comments: [], comment_id: 1000

    @doc """
    `add_comment/2` appends a comment to the state's list of comments.
    """
    @spec add_comment(State.t(), Saxy.XML.element()) :: State.t()
    def add_comment(state, comment = {"w:comment", _, _}) do
      %State{state | comments: [comment | state.comments]}
    end
  end

  @doc """
  `annotate/1` recursively traverses the entire Word document, finds all elements to be annotated and annotates them with UUIDs.
  """
  @spec annotate(P4.Docx.Document.t()) :: {:ok, P4.Docx.Document.t()} | {:error, Exception.t()}
  def annotate(docx_document = %P4.Docx.Document{}) do
    context = P4.Docx.Document.get_context(docx_document)
    state = %State{}

    {new_document_xml, new_state} = annotate_element({docx_document.document, state}, context)

    {comments_name, comments_attributes, comments_children} = docx_document.comments
    new_comments = {comments_name, comments_attributes, new_state.comments ++ comments_children}

    {:ok,
     %P4.Docx.Document{
       docx_document
       | document: new_document_xml,
         comments: new_comments
     }}
  end

  @doc """
  `annotate_elements/2` recursively traverses a list of elements and calls `annotate_element/2` on each element.
  """
  @spec annotate_elements({[Saxy.XML.content()], State.t()}, P4.Docx.DocumentContext.t()) ::
          {[Saxy.XML.content()], State.t()}
  def annotate_elements({[], _state} = bundle, %P4.Docx.DocumentContext{} = _context), do: bundle

  def annotate_elements({[head | tail], state}, %P4.Docx.DocumentContext{} = context) do
    {annotated_head, new_state} = annotate_element({head, state}, context)
    {annotated_tail, final_state} = annotate_elements({tail, new_state}, context)

    {[annotated_head | annotated_tail], final_state}
  end

  @doc """
  `annotate_element/2` annotates an element of interest with a UUID comment.

  Currently, this is done for:
  - all text elements, i.e. all paragraphs, i.e. all `w:p`s. This includes headings, regular text paragraphs and (nested) list items.
  - all structured document tags, i.e. all `w:sdt`s. This includes TOCs, TOFs and other structured content.

  Will later be extended to images and tables as well.
  """
  @spec annotate_element({Saxy.XML.content(), State.t()}, P4.Docx.DocumentContext.t()) ::
          {Saxy.XML.content(), State.t()}
  def annotate_element(
        {{"w:p", _, _} = paragraph, state},
        %P4.Docx.DocumentContext{} = context
      ) do
    add_uuid_comment({paragraph, state}, context)
  end

  def annotate_element(
        {{"w:sdt", _, _} = paragraph, state},
        %P4.Docx.DocumentContext{} = context
      ) do
    add_uuid_comment({paragraph, state}, context)
  end

  def annotate_element(
        {{"w:drawing", _, _} = image, state},
        %P4.Docx.DocumentContext{} = context
      ) do
    annotate_image({image, state}, context)
  end

  def annotate_element(
        {{"w:pict", _, _} = image, state},
        %P4.Docx.DocumentContext{} = context
      ) do
    annotate_image({image, state}, context)
  end

  def annotate_element({{name, attributes, children}, state}, context) do
    {new_children, new_state} = annotate_elements({children, state}, context)
    {{name, attributes, new_children}, new_state}
  end

  def annotate_element({string_contents, state}, _context) when is_binary(string_contents) do
    {string_contents, state}
  end

  @spec annotate_image({Saxy.XML.element(), State.t()}, P4.Docx.DocumentContext.t()) ::
          {Saxy.XML.element(), State.t()}
  def annotate_image({image, state}, _ctx) do
    # NOTE: for images, we need to find the `wp:docPr` element and transform it so the `descr` attribute becomes a JSON object with the existing description and the UUID.
    # This is necessary because Pandoc does not support comments on images and only the image content and description are preserved (as alt text) when transforming to HTML.
    # We unpack and restore the description from the JSON object in the HTML postprocessing.
    {P4.Xml.Helper.transform_recursive_child(
       image,
       fn
         {"wp:docPr", _, _} -> true
         _ -> false
       end,
       fn
         {"wp:docPr", attrs, children} ->
           descr_attr = P4.Xml.Helper.get_attr(attrs, "descr")

           descr_message =
             case descr_attr do
               {"descr", message} -> message
               nil -> ""
             end

           new_descr =
             Jason.encode!(%{message: descr_message, uuid: Ecto.UUID.generate()})
             |> Base.encode64()

           {"wp:docPr", P4.Xml.Helper.upsert_attribute(attrs, "descr", new_descr), children}
       end
     ), state}
  end

  @doc """
  `add_uuid_comment/2` adds a UUID comment to the given element, updating the state accordingly.
  It calls annotate_elements/2 to annotate any of the element's children where necessary.
  """
  @spec add_uuid_comment({Saxy.XML.element(), State.t()}, P4.Docx.DocumentContext.t()) ::
          {Saxy.XML.element(), State.t()}
  def add_uuid_comment({{name, attributes, children}, state}, context) do
    uuid = Ecto.UUID.generate()
    {id, comment, new_state} = new_comment(uuid, state)
    new_state = State.add_comment(new_state, comment)

    {annotated_children, new_state} = annotate_elements({children, new_state}, context)

    new_children =
      [new_comment_range_start(id, uuid) | annotated_children] ++ [new_comment_range_end(id)]

    {{name, attributes, new_children}, new_state}
  end

  @spec new_comment(String.t(), State.t()) :: {String.t(), Saxy.XML.element(), State.t()}
  defp new_comment(content, state) when is_binary(content) do
    id = state.comment_id
    id_str = Integer.to_string(state.comment_id)
    state = %State{state | comment_id: id + 1}

    {id_str,
     {"w:comment", [{"w:id", id_str}, {"w:author", "P4"}],
      [{"w:p", [], [{"w:r", [], [{"w:t", [], [content]}]}]}]}, state}
  end

  @spec new_comment_range_start(String.t(), String.t()) :: Saxy.XML.element()
  defp new_comment_range_start(id, uuid) do
    {"w:commentRangeStart", [{"w:id", id}, {"data-uuid", uuid}], []}
  end

  @spec new_comment_range_end(String.t()) :: Saxy.XML.element()
  defp new_comment_range_end(id) do
    {"w:commentRangeEnd", [{"w:id", id}], []}
  end
end
