defmodule P4.Docx.PreProcessor do
  @moduledoc """
  P4 is a Pandoc Pre-Processor.

  This module preprocesses Docx documents before they are put through Pandoc for further conversion.
  """

  alias P4.Docx.Transformer

  @spec transformers() :: [module()]
  def transformers() do
    [
      Transformer.RemoveTOC,
      Transformer.StyleMatching,
      Transformer.RemoveMiniImages
    ]
  end

  @spec process(P4.Docx.Document.t()) :: {:ok, P4.Docx.Document.t()} | {:error, Exception.t()}
  def process(document = %P4.Docx.Document{}) do
    {:ok,
     Enum.reduce(transformers(), document, fn transformer, document ->
       transformer.transform(document)
     end)}
  end
end
