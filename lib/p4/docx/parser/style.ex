defmodule P4.Docx.Parser.Style do
  @moduledoc """
  This module contains functions to get information about styles in a docx file,
  abstracting the XML structure of the style objects.
  """

  @doc """
  Returns true if this style is a heading style, i.e. if it has a name starting with 'heading ' (case-insensitive).

  Contrary to Section 17.3.1.20 of the ECMA-376-1:2016 standard, this implementation does not look at the `w:outlineLvl` property,
  as this property is used inconsistently by different Word processors and is less reliable than depending on the style's name,
  which is also what Pandoc does.

  ### Examples

      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "heading 1"}], []}]})
      true
      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "Heading 1"}], []}]})
      true
      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:pPr", [], [{"w:outlineLvl", [{"w:val", "1"}], []}]}]})
      false
      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "Normal"}], []}]})
      false
  """
  @spec is_heading?(Saxy.XML.element()) :: boolean()
  def is_heading?(style = {"w:style", _, _}) do
    has_heading_name?(style)
  end

  @doc """
  Returns true if this style has a name starting with 'heading ' (case-insensitive).

  ### Examples

      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "heading 1"}], []}]})
      true
      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "Heading 1"}], []}]})
      true
      iex> P4.Docx.Parser.Style.is_heading?({"w:style", [], [{"w:name", [{"w:val", "Normal"}], []}]})
      false
  """
  @spec has_heading_name?(Saxy.XML.element()) :: boolean()
  def has_heading_name?(style = {"w:style", _, _}) do
    style
    |> P4.Xml.Helper.get_child_by_name("w:name", {"w:name", [], []})
    |> P4.Xml.Helper.get_attribute_value("w:val", "")
    |> String.downcase()
    |> String.starts_with?("heading ")
  end

  @doc """
  Returns true if this style is a Table of Contents style, i.e. its styleId starts with 'toc' or id 'tableoffigures' (case-insensitive).

  ### Examples

      iex> P4.Docx.Parser.Style.is_toc?({"w:style", [{"w:styleId", "toc"}], []})
      true
      iex> P4.Docx.Parser.Style.is_toc?({"w:style", [{"w:styleId", "TOC1"}], []})
      true
      iex> P4.Docx.Parser.Style.is_toc?({"w:style", [{"w:styleId", "Normal"}], []})
      false
  """
  @spec is_toc?(Saxy.XML.element()) :: boolean()
  def is_toc?(style = {"w:style", _, _}) do
    style_id =
      style
      |> P4.Xml.Helper.get_attribute_value("w:styleId", "")
      |> String.downcase()

    String.starts_with?(style_id, "toc") || style_id === "tableoffigures"
  end

  @doc """
  Returns true if this style is of the given type.

  ### Examples

      iex> P4.Docx.Parser.Style.is_of_type?({"w:style", [{"w:type", "paragraph"}], []}, "paragraph")
      true
      iex> P4.Docx.Parser.Style.is_of_type?({"w:style", [{"w:type", "numbering"}], []}, "paragraph")
      false
      iex> P4.Docx.Parser.Style.is_of_type?({"w:style", [], []}, "paragraph")
      false
  """
  @spec is_of_type?(Saxy.XML.element(), String.t()) :: boolean()
  def is_of_type?(style = {"w:style", _, _}, type) when is_binary(type) do
    P4.Xml.Helper.get_attribute_value(style, "w:type") === type
  end

  @doc """
  Returns true if this style is the default style of the document.

  ### Examples

      iex> P4.Docx.Parser.Style.is_default?({"w:style", [{"w:default", "1"}], []})
      true
      iex> P4.Docx.Parser.Style.is_default?({"w:style", [{"w:default", "0"}], []})
      false
      iex> P4.Docx.Parser.Style.is_default?({"w:style", [], []})
      false
  """
  @spec is_default?(Saxy.XML.element()) :: boolean()
  def is_default?(style = {"w:style", _, _}) do
    P4.Xml.Helper.get_attribute_value(style, "w:default") === "1"
  end

  @doc """
  Returns true if this is top level (i.e. is not based on another style, i.e. has no w:basedOn-value).

  ### Examples

      iex> P4.Docx.Parser.Style.is_top_level?({"w:style", [], [{"w:basedOn", [{"w:val", "Other"}], []}]})
      false
      iex> P4.Docx.Parser.Style.is_top_level?({"w:style", [], []})
      true
      iex> P4.Docx.Parser.Style.is_top_level?({"w:docDefaults", [], []})
      false
  """
  @spec is_top_level?(Saxy.XML.element()) :: boolean()
  def is_top_level?({"w:style", _, children}) do
    children
    |> P4.Xml.Helper.get_element_by_name("w:basedOn")
    |> P4.Xml.Helper.get_attribute_value("w:val")
    |> is_nil()
  end

  def is_top_level?(_), do: false

  @spec get_based_on_value(Saxy.XML.element()) :: String.t() | nil
  def get_based_on_value({"w:style", _, children}) do
    children
    |> P4.Xml.Helper.get_element_by_name("w:basedOn")
    |> P4.Xml.Helper.get_attribute_value("w:val")
  end

  def get_based_on_value(_), do: nil
end
