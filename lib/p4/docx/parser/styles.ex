defmodule P4.Docx.Parser.Styles do
  require P4.Xml.Helper
  require P4.Docx.SpecificStyleIdsState

  @doc """
  Get all Table of Contents, Table of Figures styles from a given
  _word/styles.xml_ (SimpleForm).

  Returns a list of style id's (list of strings).
  """
  def get_specific_style_ids({"w:styles", _, children}) do
    get_specific_style_ids(children, %P4.Docx.SpecificStyleIdsState{})
  end

  def get_specific_style_ids(
        [style = {"w:style", _, _} | tail],
        %P4.Docx.SpecificStyleIdsState{} = state
      ) do
    style_id = P4.Xml.Helper.get_attribute_value(style, "w:styleId")

    cond do
      P4.Docx.Parser.Style.is_toc?(style) ->
        get_specific_style_ids(tail, %{
          state
          | table_of_contents: [style_id | state.table_of_contents]
        })

      is_default?(style) ->
        get_specific_style_ids(tail, %{
          state
          | normal: style_id
        })

       is_heading?(style) ->
        get_specific_style_ids(tail, %{
          state
          | heading: [style_id | state.heading]
        })

      true ->
        get_specific_style_ids(tail, state)
    end
  end

  def get_specific_style_ids([_ | tail], state), do: get_specific_style_ids(tail, state)

  def get_specific_style_ids([], state), do: state

  defp is_heading?({"w:style", _, _} = style) do
      P4.Docx.Parser.Style.is_of_type?(style, "paragraph") && P4.Docx.Parser.Style.is_heading?(style)
  end

  defp is_default?({"w:style", _, _} = style) do
    P4.Docx.Parser.Style.is_of_type?(style, "paragraph") && P4.Docx.Parser.Style.is_default?(style)
  end

  def get_document_defaults({"w:styles", _, _} = sheet) do
    P4.Xml.Helper.get_child_by_name(sheet, "w:docDefaults")
  end

  def get_style_by_style_id({"w:styles", _, sheet_children}, style_id) do
    sheet_children
    |> Enum.find(fn
      style = {"w:style", _, _} ->
        P4.Xml.Helper.get_attribute_value(style, "w:styleId") === style_id

      _ ->
        false
    end)
  end
end
