defmodule P4.Docx.Transformer.Behaviour do
  @callback transform(P4.Docx.Document.t()) :: P4.Docx.Document.t()
  @callback transform(Saxy.XML.content(), P4.Docx.DocumentContext) ::
              Saxy.XML.content() | [Saxy.XML.content()]
  @callback transform([Saxy.XML.content()], P4.Docx.DocumentContext) :: [Saxy.XML.content()]
end

defmodule P4.Docx.Transformer do
  defmacro __using__(_opts) do
    quote do
      @behaviour P4.Docx.Transformer.Behaviour
      defoverridable P4.Docx.Transformer.Behaviour

      def transform(document = %P4.Docx.Document{document: root}) do
        ctx = %P4.Docx.DocumentContext{
          specific_style_ids: P4.Docx.Parser.Styles.get_specific_style_ids(document.styles),
          style_map: P4.Style.StyleMutator.merge_document_styles(document.styles)
        }

        %P4.Docx.Document{document | document: transform(root, ctx)}
      end

      def transform([], _), do: []

      def transform([head | tail], ctx),
        do: [transform(head, ctx) | transform(tail, ctx)] |> List.flatten()

      def transform(string_contents, _) when is_binary(string_contents), do: string_contents
    end
  end
end
