defmodule P4.Docx.Transformer.RemoveMiniImages do
  @moduledoc """
  This Docx transformer removes small images like chevrons, dividers and other small icons from Word documents.
  This module removes all images with a height or width less than the equivalent of 0.5 inch.
  """

  use P4.Docx.Transformer

  # There are 914400 EMU in one inch, thus 457200 is 0.5 inch.
  @mini_image_threshold_emu 457_200
  @mini_image_threshold_inch 0.5
  @mini_image_threshold_cm 1.27
  # Points are equal to 1/72 of an inch, so 36pt is 0.5 inch.
  @mini_image_threshold_pt 36

  def transform({"w:drawing", attributes, children} = drawing, _) do
    extent =
      drawing
      |> P4.Xml.Helper.get_child_by_name(
        "wp:anchor",
        P4.Xml.Helper.get_child_by_name(drawing, "wp:inline")
      )
      |> P4.Xml.Helper.get_child_by_name("wp:extent")

    width_emu = P4.Xml.Helper.get_attribute_value(extent, "cx", "0") |> Float.parse()
    height_emu = P4.Xml.Helper.get_attribute_value(extent, "cy", "0") |> Float.parse()

    if is_mini_image?(width_emu, height_emu, "emu") do
      []
    else
      # no need to recurse children as there won't be images inside images.
      {"w:drawing", attributes, children}
    end
  end

  def transform({"w:pict", attributes, children} = drawing, _) do
    styles =
      drawing
      |> P4.Xml.Helper.get_child_by_name("v:shape")
      |> P4.Xml.Helper.get_attribute_value("style", "")
      |> String.split(";", trim: true)
      |> Enum.map(fn style_line ->
        split_line = String.split(style_line, ":", parts: 2, trim: true)
        key = Enum.at(split_line, 0, "")
        value = Enum.at(split_line, 1, "")
        {key, value}
      end)

    is_mini_image =
      Enum.any?(styles, fn
        {"height", height} -> Float.parse(height) |> is_mini_image?()
        {"width", width} -> Float.parse(width) |> is_mini_image?()
        _ -> false
      end)

    if is_mini_image do
      []
    else
      # no need to recurse children as there won't be images inside images.
      {"w:pict", attributes, children}
    end
  end

  def transform({name, attributes, children}, ctx),
    do: {name, attributes, transform(children, ctx)}

  @spec is_mini_image?({float(), binary()} | :error, {float(), binary()} | :error, String.t()) ::
          boolean()
  defp is_mini_image?({width, _}, {height, _}, "emu") when is_float(width) and is_float(height),
    do: width <= @mini_image_threshold_emu or height <= @mini_image_threshold_emu

  defp is_mini_image?(_, {height, _}, "emu") when is_float(height),
    do: height <= @mini_image_threshold_emu

  defp is_mini_image?({width, _}, _, "emu") when is_float(width),
    do: width <= @mini_image_threshold_emu

  defp is_mini_image?(:error, _, _), do: false
  defp is_mini_image?(_, :error, _), do: false
  defp is_mini_image?(_, _, _), do: false

  @spec is_mini_image?({float(), binary()} | :error) :: boolean()
  defp is_mini_image?({float, "pt"}), do: float <= @mini_image_threshold_pt
  defp is_mini_image?({float, "mm"}), do: float <= @mini_image_threshold_cm * 10
  defp is_mini_image?({float, "cm"}), do: float <= @mini_image_threshold_cm
  defp is_mini_image?({float, "in"}), do: float <= @mini_image_threshold_inch
  defp is_mini_image?(:error), do: false
  defp is_mini_image?(_), do: false
end
