defmodule P4.Docx.Transformer.RemoveTOC do
  @moduledoc """
  This Docx transformer removes the table of contents.
  """

  use P4.Docx.Transformer

  def transform({"w:sdt", attributes, children} = sdt, ctx) do
    if P4.Document.Parser.is_toc(sdt) do
      []
    else
      [{"w:sdt", attributes, transform(children, ctx)}]
    end
  end

  def transform({"w:p", attributes, children} = paragraph, ctx) do
    p_style_id =
      paragraph
      |> P4.Xml.Helper.get_child_by_name("w:pPr")
      |> P4.Xml.Helper.get_child_by_name("w:pStyle")
      |> P4.Xml.Helper.get_attribute_value("w:val")
      |> P4.Std.default(ctx.specific_style_ids.normal)

    if P4.Docx.DocumentContext.is_toc_style_id?(p_style_id, ctx) do
      P4.Logger.info("Autofix: ToC removed from document #{ctx.id}",
        "event.action": "autofix-removed-toc",
        "data.documentId": ctx.id
      )

      []
    else
      {"w:p", attributes, transform(children, ctx)}
    end
  end

  def transform({name, attributes, children}, ctx),
    do: {name, attributes, transform(children, ctx)}
end
