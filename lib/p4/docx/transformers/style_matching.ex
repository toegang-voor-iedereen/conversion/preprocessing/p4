defmodule P4.Docx.Transformer.StyleMatching do
  @moduledoc """
  This module is a template for a P4.Docx.Transformer. Simply copy and implement how you see fit.
  """

  use P4.Docx.Transformer

  def transform({"w:p", _, _} = paragraph, ctx) do
    p_style_id =
      paragraph
      |> P4.Xml.Helper.get_child_by_name("w:pPr")
      |> P4.Xml.Helper.get_child_by_name("w:pStyle")
      |> P4.Xml.Helper.get_attribute_value("w:val")
      |> P4.Std.default(ctx.specific_style_ids.normal)

    cond do
      # if nil, it is style Normal as that's always the default.
      P4.Docx.DocumentContext.is_heading_style_id?(p_style_id, ctx) ||
          P4.Docx.DocumentContext.is_normal_style_id?(p_style_id, ctx) ->
        paragraph
        |> P4.Style.StyleMatcher.modify_style_properties(ctx)
        |> P4.Document.AlternateContentMutator.process_alternate_content()

      true ->
        paragraph
        |> P4.Document.AlternateContentMutator.process_alternate_content()
    end
  end

  def transform({name, attributes, children}, ctx),
    do: {name, attributes, transform(children, ctx)}
end
