defmodule P4.Docx.Transformer.Template do
  @moduledoc """
  This module is a template for a P4.Docx.Transformer. Simply copy and implement how you see fit.
  """

  use P4.Docx.Transformer

  def transform({"w:p", attributes, children}, ctx) do
    # Implement your custom transformation here.
    # Return a single element, or a list of elements.
    # Returning a list of more than 1 element, will put those elements in place of the current element.
    # Returning an empty list, will remove the element.
    {"w:p", attributes, transform(children, ctx)}
  end

  def transform({name, attributes, children}, ctx),
    do: {name, attributes, transform(children, ctx)}
end
