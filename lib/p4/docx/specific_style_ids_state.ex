defmodule P4.Docx.SpecificStyleIdsState do
  @type t :: %__MODULE__{
          table_of_contents: list(String.t()),
          normal: String.t() | nil,
          heading: list(String.t())
        }
  defstruct table_of_contents: [], normal: nil, heading: []
end
