defmodule P4.Docx.DocumentContext do
  require P4.Docx.SpecificStyleIdsState
  alias P4.Docx.SpecificStyleIdsState
  alias P4.Docx.DocumentContext

  @type t :: %__MODULE__{
          id: String.t(),
          specific_style_ids: SpecificStyleIdsState.t(),
          style_map: map()
        }
  defstruct id: nil, specific_style_ids: %SpecificStyleIdsState{}, style_map: %{}

  @doc """
  Get the style with the given id from the style map.
  """
  @spec get_style(DocumentContext.t(), String.t(), any()) ::
          Saxy.XML.element() | nil
  def get_style(%DocumentContext{} = context, style_id, default \\ nil) do
    context.style_map
    |> Map.get(style_id)
    |> P4.Std.default(default)
  end

  @doc """
  Returns true if the given style id is denoted in the Table of Contents specific style ids.
  """
  @spec is_toc_style_id?(String.t(), DocumentContext.t()) :: boolean()
  def is_toc_style_id?(style_id, %DocumentContext{
        specific_style_ids: %SpecificStyleIdsState{table_of_contents: toc_style_ids}
      }) do
    Enum.member?(toc_style_ids, style_id)
  end

  @doc """
  Returns true if the given style id is denoted in the Heading specific style ids.
  """
  @spec is_heading_style_id?(String.t(), DocumentContext.t()) :: boolean()
  def is_heading_style_id?(style_id, %DocumentContext{
        specific_style_ids: %SpecificStyleIdsState{heading: heading_style_ids}
      }) do
    Enum.member?(heading_style_ids, style_id)
  end

  @doc """
  Returns true if the given style id is the specific style id normal.
  """
  @spec is_normal_style_id?(String.t(), DocumentContext.t()) :: boolean()
  def is_normal_style_id?(style_id, %DocumentContext{
        specific_style_ids: %SpecificStyleIdsState{normal: normal_id}
      }) do
    style_id === normal_id
  end
end
