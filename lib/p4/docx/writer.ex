require OK

defmodule P4.Docx.Writer do
  @prolog %Saxy.Prolog{encoding: "UTF-8", standalone: true, version: "1.0"}

  def write(%P4.Docx.Document{
        file_list: file_list,
        extraction_path: extraction_path,
        document: document,
        comments: comments,
        styles: styles
      }) do
    OK.try do
      _ <-
        document
        |> Saxy.encode!(@prolog)
        |> write_file(extraction_path, "word/document.xml")

      _ <-
        comments
        |> Saxy.encode!(@prolog)
        |> write_file(extraction_path, "word/comments.xml")

      _ <-
        styles
        |> Saxy.encode!(@prolog)
        |> write_file(extraction_path, "word/styles.xml")

      path_to <- P4.IO.File.create_temp()

      _ <- P4.IO.Zip.create_from_directory(extraction_path, file_list, path_to)
    after
      {:ok, path_to}
    rescue
      reason -> {:error, reason}
    end
  end

  defp write_file(content, extraction_path, relative_file_path)
       when is_binary(content) and is_binary(extraction_path) and is_binary(relative_file_path) do
    full_path = Path.join(extraction_path, relative_file_path)

    case File.write(full_path, content) do
      :ok ->
        {:ok, :done}

      result = {:error, _reason} ->
        result
        |> P4.Error.P4.Docx.Writer.WritingFile.wrap(%{
          extraction_path: extraction_path,
          relative_file_path: relative_file_path
        })
    end
  end
end
