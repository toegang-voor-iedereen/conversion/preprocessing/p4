defmodule P4.Docx.Document do
  @type t :: %__MODULE__{
          id: String.t(),
          document: Saxy.XML.element(),
          comments: Saxy.XML.element(),
          styles: Saxy.XML.element(),
          file_list: [String.t()],
          extraction_path: String.t()
        }
  defstruct id: nil,
            file_list: [],
            extraction_path: nil,
            document: nil,
            comments: nil,
            styles: nil

  @spec get_context(P4.Docx.Document.t()) :: %P4.Docx.DocumentContext{}
  def get_context(%P4.Docx.Document{id: id, styles: styles}) do
    %P4.Docx.DocumentContext{
      id: id,
      specific_style_ids: P4.Docx.Parser.Styles.get_specific_style_ids(styles),
      style_map: P4.Style.StyleMutator.merge_document_styles(styles)
    }
  end

  @spec get_element_uuid(Saxy.XML.element()) :: String.t() | nil
  def get_element_uuid({"w:drawing", _, _} = element) do
    get_image_uuid(element)
  end

  def get_element_uuid({"w:pict", _, _} = element) do
    get_image_uuid(element)
  end

  def get_element_uuid({_, _, children}) do
    children
    |> P4.Xml.Helper.find_element_recursively(fn
      {"w:commentRangeStart", _, _} -> true
      _ -> false
    end)
    |> P4.Xml.Helper.get_attribute_value("data-uuid")
  end

  defp get_image_uuid({_, _, children}) do
    children
    |> P4.Xml.Helper.find_element_recursively(fn
      {"wp:docPr", _, _} -> true
      _ -> false
    end)
    |> P4.Xml.Helper.get_attribute_value("descr", "e30K")
    |> Base.decode64!()
    |> Jason.decode!()
    |> Map.get("uuid")
  end
end
