use OK.Pipe
require OK

defmodule P4 do
  @moduledoc """
  P4 is a Pandoc Pre-Processor
  """
  use Application

  alias P4.Amqp.Consumer.{
    WordHeadingAnnotatingConsumer,
    WordPreprocessingConsumer,
    HtmlPostProcessingConsumer
  }

  def get_printable_config() do
    p4_config =
      %{}
      |> Map.put("env", Application.get_env(:p4, :env))
      |> Map.put("name", Application.get_env(:p4, :name))
      |> Map.put("version", Application.get_env(:p4, :version))

    amqp_config =
      %{}
      |> Map.put("host", Application.get_env(:amqp, :connection)[:host])
      |> Map.put("port", Application.get_env(:amqp, :connection)[:port])
      |> Map.put("vhost", Application.get_env(:amqp, :connection)[:virtual_host])
      |> Map.put("username", redact(Application.get_env(:amqp, :connection)[:username]))
      |> Map.put("password", redact(Application.get_env(:amqp, :connection)[:password]))

    s3_config =
      %{}
      |> Map.put("scheme", Application.get_env(:ex_aws, :s3)[:scheme])
      |> Map.put("host", Application.get_env(:ex_aws, :s3)[:host])
      |> Map.put("port", Application.get_env(:ex_aws, :s3)[:port])
      |> Map.put("region", Application.get_env(:ex_aws, :region))
      |> Map.put("bucket", Application.get_env(:p4, :s3_bucket))
      |> Map.put("access_key_id", redact(Application.get_env(:ex_aws, :access_key_id)))
      |> Map.put("secret_access_key", redact(Application.get_env(:ex_aws, :secret_access_key)))

    %{}
    |> Map.put("p4", p4_config)
    |> Map.put("s3", s3_config)
    |> Map.put("amqp", amqp_config)
  end

  def start(_, _) do
    config = get_printable_config()
    P4.Logger.init()
    P4.Logger.notice("Starting P4", "p4.config": config)

    consumers = consumers()
    result = Supervisor.start_link(consumers, strategy: :one_for_one)

    P4.Logger.notice("P4 started: #{inspect(consumers)}")
    result
  end

  @spec consumers() :: [
          P4.Amqp.Consumer.HtmlPostProcessingConsumer
          | P4.Amqp.Consumer.WordHeadingAnnotatingConsumer
          | P4.Amqp.Consumer.WordPreprocessingConsumer
        ]
  def consumers() do
    if Application.get_env(:p4, :env) === "test" do
      []
    else
      [WordHeadingAnnotatingConsumer, WordPreprocessingConsumer, HtmlPostProcessingConsumer]
    end
  end

  # -------------------------------------------------------------------------------

  defp redact(nil), do: nil

  defp redact(string) when is_binary(string) do
    String.duplicate("*", String.length(string))
  end

  defp redact({:system, env_var}) do
    redact(System.get_env(env_var))
  end

  defp redact([{:system, env_var}, _]) do
    redact(System.get_env(env_var))
  end
end
