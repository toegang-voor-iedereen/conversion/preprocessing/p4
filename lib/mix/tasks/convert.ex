defmodule Mix.Tasks.Convert do
  @moduledoc """
  This module defines the Mix task `convert` which is used to preprocess DocX files.

  The `convert` task takes two arguments:
  - `from`: Path to the file to convert.
  - `to`: Path to the output file.

  The task reads the input file, processes it using the `P4.Docx.PreProcessor` module, and writes the result to the output file.

  Example usage:
  ```
  mix convert --from input.docx --to output.docx
  ```

  For more information, refer to the documentation of the `P4.Docx.PreProcessor` module.

  TODO: update this so it can also annotates, then preprocesses.
  """

  require OK

  use OK.Pipe

  use Mix.Task

  def run(argv) do
    %Optimus.ParseResult{args: %{from: from, to: to}} =
      Optimus.new!(
        name: "p4",
        description: "P4 is a Pandoc Pre-Processor",
        version: "0.0.0",
        author: "Stephan Meijer <stephan.meijer@logius.nl>",
        about: "Utility for pre-processing DocX files",
        allow_unknown_args: false,
        parse_double_dash: true,
        args: [
          from: [
            value_name: "FROM",
            help: "Path to file to convert.",
            required: true,
            parser: :string
          ],
          to: [
            value_name: "TO",
            help: "Path to output",
            required: true,
            parser: :string
          ]
        ],
        flags: [],
        options: []
      )
      |> Optimus.parse!(argv)

    convert(from, to)
  end

  def convert(from, to) do
    IO.puts("Preprocessing from #{from} to #{to}")

    OK.try do
      output_path <-
        P4.Docx.Reader.read_from_path(from, Ecto.UUID.generate())
        ~>> P4.Docx.PreProcessor.process()
        ~>> P4.Docx.Writer.write()

      _ <- File.rename(output_path, to)
    after
      IO.puts("Converted, saved as #{to}")
    rescue
      error ->
        IO.puts(:stderr, "Failure preprocessing #{from} to #{to}.")
        IO.inspect(error)
    end
  end
end
