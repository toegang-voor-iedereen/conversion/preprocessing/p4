defmodule P4.MixProject do
  use Mix.Project

  def project do
    [
      app: :p4,
      version: "0.8.10",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        p4: [
          include_erts: true,
          include_src: false
        ]
      ],
      escript: [main_module: P4],
      elixirc_options: [
        # treat warnings as errors when compiling, so that references to undefined functions will also break the build
        warnings_as_errors: true
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {P4, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # AMQP
      {:amqp, "~> 3.3"},
      {:ecto, "~> 3.11"},

      # DOCX XML, AMQP
      {:saxy, "~> 1.5"},
      {:temp, "~> 0.4"},

      # CLI
      {:optimus, "~> 0.5"},

      # Logging
      {:logger_json, "~> 6.0"},

      # Sentry
      {:sentry, "~> 10.6.0"},

      # S3
      {:ex_aws, "~> 2.0"},
      {:ex_aws_s3, "~> 2.0"},
      {:sweet_xml, "~> 0.7.0"},

      # Sentry, S3
      {:hackney, "~> 1.9"},

      # Sentry, AMQP
      {:jason, "~> 1.4"},

      # Misc.
      {:ok, "~> 2.3"},

      # Testing
      {:mimic, "~> 1.7", only: :test},
      {:meck, "~> 0.9", only: :test},
      {:ex_parameterized, "~> 1.3", only: :test}
    ]
  end
end
