#!/bin/bash
# This script is meant to be run on CI by semantic-release as a verifyReleaseCmd.
# Usage:
#   ["@semantic-release/exec", {
#       "verifyReleaseCmd": ".ci/semantic-release/bump-versions.sh ${nextRelease.version}",
#   }],

DEFAULT_REGISTRY_IMAGE="registry.gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4"

export NEXT_VERSION="$1"
export REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-$DEFAULT_REGISTRY_IMAGE}"
export FULL_IMAGE="$REGISTRY_IMAGE:$NEXT_VERSION"
export CHART_LABEL="nldoc-p4-${NEXT_VERSION}"

yq -P -i "
  .version = strenv(NEXT_VERSION) |
  .appVersion = strenv(NEXT_VERSION)
" helm/Chart.yaml

yq -P -i "
  .image.tag = strenv(NEXT_VERSION)
" helm/values.yaml

for test_folder in test/helm/*; do
  [[ -d "$test_folder" ]] || continue

  yq -P -i '
    with(.metadata.labels ; ."app.kubernetes.io/version" = strenv(NEXT_VERSION)) |
    with(.metadata.labels ; ."helm.sh/chart" = strenv(CHART_LABEL)) |
    with(.spec.template.metadata.labels ; ."app.kubernetes.io/version" = strenv(NEXT_VERSION)) |
    with(.spec.template.metadata.labels ; ."helm.sh/chart" = strenv(CHART_LABEL)) |
    .spec.template.spec.containers[0].image = strenv(FULL_IMAGE)
  ' "$test_folder/expected.yaml"
done
