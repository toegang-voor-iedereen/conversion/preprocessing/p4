#!/bin/bash

set -euo pipefail

jinja2 .gitlab-ci.j2.yml recipe.yml --format=yaml -e jinja2_ext_path.dirname \
    | awk 'BEGIN{RS="";ORS="\n\n"}1' \
    | sed -e :a -e '/^\n*$/{$d;N;};/\n$/ba' > .gitlab-ci.yml

if command -v glab; then glab ci lint; fi
