import Config

config :p4,
  s3_bucket: System.get_env("S3_BUCKET", "local")

config :ex_aws,
  access_key_id: System.get_env("S3_ACCESS_KEY"),
  secret_access_key: System.get_env("S3_SECRET_KEY"),
  region: System.get_env("S3_REGION", "eu-west-1")

config :ex_aws, :s3,
  scheme: System.get_env("S3_SCHEME", "http://"),
  host: System.get_env("S3_HOST", "localhost"),
  port: System.get_env("S3_PORT", "9000")

if System.get_env("APP_ENV", "prod") !== "test" and System.get_env("MIX_ENV", "prod") !== "test" do
  config :amqp,
    connection: [
      host: System.get_env("AMQP_HOST"),
      port: System.get_env("AMQP_PORT"),
      username: System.get_env("AMQP_USERNAME"),
      password: System.get_env("AMQP_PASSWORD"),
      virtual_host: System.get_env("AMQP_VHOST")
    ],
    channel: []
end

config :sentry,
  dsn:
    "https://d9449660e8aab4969dbf4aa0d59affbd@o4504328270970880.ingest.sentry.io/4506423858429952",
  environment_name: System.get_env("SENTRY_ENVIRONMENT", "unknown"),
  enable_source_code_context: true,
  root_source_code_paths: [File.cwd!()]

ca_certificates_path = System.get_env("CA_CERTIFICATES_PATH", nil)

unless is_nil(ca_certificates_path) do
  config :ex_aws, :hackney_opts,
    recv_timeout: 30_000,
    ssl_options: [
      {:versions, [:"tlsv1.2"]},
      {:verify, :verify_peer},
      {:cacertfile, ca_certificates_path},
      {:customize_hostname_check,
       [
         match_fun: :public_key.pkix_verify_hostname_match_fun(:https)
       ]}
    ]
end
