import Config

config :logger, :default_handler,
  formatter: {LoggerJSON.Formatters.Elastic, metadata: {:all_except, [:application]}}
