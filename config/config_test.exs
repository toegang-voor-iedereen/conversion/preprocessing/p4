import Config

config :p4,
  name: System.get_env("APP_NAME", "p4"),
  env: "test",
  version: Mix.Project.config()[:version]
