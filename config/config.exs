import Config

if config_env() == :test do
  import_config "config_test.exs"
else
  import_config "config_default.exs"
end

import_config "config_logger.exs"
