import Config

config :p4,
  name: System.get_env("APP_NAME", "p4"),
  env: System.get_env("APP_ENV", "prod"),
  version: Mix.Project.config()[:version]
